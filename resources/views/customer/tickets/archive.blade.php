@extends('customer.layouts.master')

@section('title', 'Архивные тикеты')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Тикеты
                <small>Архивные тикеты</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('CustomerHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Архивные тикеты</li>
            </ol>

        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                            <h3 class="box-title">Архивные тикеты</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered table-has-content" data-content-url="{{route('CustomerTicketList', ['type' => 'archive'])}}" >
                                <thead>
                                <tr>
                                    <th class="text-center" width="80">#</th>
                                    <th>Заголовок</th>
                                    <th>Категория</th>
                                    <th width="150" class="text-center">Дата</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


            </div>

        </section>
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('js')
<script src="/assets/app/customer/js/modules/tickets.js"></script>
@endpush