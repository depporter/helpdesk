@extends('customer.layouts.master')

@section('title', 'Тикеты в работе')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Тикеты
                <small>Тикеты в работе</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('CustomerHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Тикеты в работе</li>
            </ol>

        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                            <h3 class="box-title">Тикеты в работе</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered table-has-content" data-content-url="{{route('CustomerTicketList', ['type' => 'actual'])}}" >
                                <thead>
                                <tr>
                                    <th class="text-center" width="80">#</th>
                                    <th>Заголовок</th>
                                    <th>Категория</th>
                                    <th class="text-center" width="100">Есть ответ</th>
                                    <th width="150" class="text-center">Дата</th>
                                    <th class="text-center" width="50"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


            </div>

        </section>
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="ticketModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/customer/js/modules/tickets.js"></script>
@endpush