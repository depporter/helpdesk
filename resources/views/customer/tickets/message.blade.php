<div class="item">
    <img src="http://helpdesk.dev/assets/app/customer/images/user-placeholder-160x160.gif" alt="user image" class="offline">

    <p class="message">
        <a href="#" class="name">
            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date("d.m.Y в H:i", strtotime($message->created_at))}}</small>
            Вы
        </a>
        {!! nl2br($message->message) !!}
    </p>
</div>