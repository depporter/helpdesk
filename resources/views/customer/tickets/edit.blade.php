<form action="{{route('CustomerTicketUpdate', ['ticketId' => $ticket->id])}}" method="post" data-app-module="tickets" data-app-method="handleTicketUpdateForm" id="ticketUpdateForm">
    <div class="form-group">
        <label for="title">Краткий заголовок *</label>
        <input name="title" id="title" class="form-control" value="{{$ticket->title}}">
        <span class="help-block"></span>
    </div>

    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>