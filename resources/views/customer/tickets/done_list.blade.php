@foreach($tickets as $ticket)
    <tr>
        <td class="text-center"><a href="{{route('CustomerTicketShow', ['ticketId' => $ticket->id])}}">{{$ticket->id}}</a></td>
        <td><a href="{{route('CustomerTicketShow', ['ticketId' => $ticket->id])}}">{{$ticket->title}}</a></td>
        <td class="text-center">{{str_limit($ticket->category->name, 30)}}</td>

        <td class="text-center">{{$ticket->getRuCreatedAt()}}</td>
        <td class="text-center">
            <a href="{{route('CustomerTicketAction', ['ticketId' => $ticket->id, 'action' => 'archive'])}}" class="btn btn-xs btn-success handle-click" data-app-module="tickets" data-app-method="handleArchiveBtnFromList">Архивировать!</a>
        </td>
    </tr>
@endforeach

@if(!$tickets->count())
    <tr><td colspan="5" class="text-center">Данных не найдено</td></tr>
@endif