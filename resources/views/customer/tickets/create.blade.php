@extends('customer.layouts.master')

@section('title', 'Создать тикет')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Тикеты
                <small>Создать тикет</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('CustomerHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li><a href="{{route('CustomerTicketActual')}}">Тикеты</a></li>
                <li class="active">Создать тикет</li>
            </ol>

        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                            <h3 class="box-title">Создать тикет</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <form action="{{route('CustomerTicketStore')}}" method="post" data-app-module="tickets" data-app-method="handleTicketCreateForm" id="ticketCreateForm">
                                <div class="form-group">
                                    <label for="ticket_category_id">Категория *</label>
                                    <select name="ticket_category_id" id="ticket_category_id" class="form-control">
                                        <option value="">Укажите категорию</option>
                                        @foreach($categories as $category)

                                            @if($category->type == 'limit_monthly')
                                                <option value="{{$category->id}}" @if(isset($categoryTicketsTotal[$category->id]) && $categoryTicketsTotal[$category->id] >= $category->pivot->ticket_limit ) disabled @endif data-title-hint="{{$category->title_hint}}" data-desc-hint="{{$category->desc_hint}}">{{str_limit($category->name, 30)}} Лимит: {{$category->pivot->ticket_limit}} в мес. @if(isset($categoryTicketsTotal[$category->id])) Осталось {{$category->pivot->ticket_limit - $categoryTicketsTotal[$category->id]}} @endif</option>
                                            @endif

                                            @if($category->type == 'limit_countable')
                                                <option value="{{$category->id}}" @if($category->pivot->ticket_limit == 0 ) disabled @endif data-title-hint="{{$category->title_hint}}" data-desc-hint="{{$category->desc_hint}}">{{str_limit($category->name, 30)}} - Осталось {{$category->pivot->ticket_limit}} </option>
                                            @endif

                                            @if($category->type == 'by_price')
                                                <option value="{{$category->id}}"  data-title-hint="{{$category->title_hint}}" data-desc-hint="{{$category->desc_hint}}">{{str_limit($category->name, 30)}} - {{$category->price}} р./тикет</option>
                                            @endif




                                        @endforeach
                                    </select>
                                    <span class="help-block"></span>
                                </div>

                                <div class="form-group">
                                    <label for="title">Краткий заголовок *</label>
                                    <input name="title" id="title" class="form-control" >
                                    <span class="help-block"></span>
                                </div>

                                <div class="form-group">
                                    <label for="message">Описание запроса *</label>
                                    <textarea name="message" id="message" class="form-control" rows="10" ></textarea>
                                    <span class="help-block"></span>
                                </div>

                                <div class="form-group">
                                    <label for="files">Файлы</label>
                                    <input type="file" name="image[]" id="files" multiple class="form-control">
                                    <span class="help-block">Если требуется Вы можете загрузить файлы. Система принимает только: .jpeg, .jpg, .bmp, .png, .zip, .rar, .pdf</span>
                                </div>

                                <button type="submit" class="btn btn-primary">Создать</button>
                            </form>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


            </div>

        </section>
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('js')
<script src="/assets/app/customer/js/modules/tickets.js?v=2"></script>
@endpush