@foreach($tickets as $ticket)
  <tr>
      <td class="text-center"><a href="{{route('CustomerTicketShow', ['ticketId' => $ticket->id])}}">{{$ticket->id}}</a></td>
      <td><a href="{{route('CustomerTicketShow', ['ticketId' => $ticket->id])}}">{{$ticket->title}}</a></td>
      <td class="text-center">@if($ticket->category){{str_limit($ticket->category->name, 30)}}@else -- @endif</td>
      <td class="text-center">{!! $ticket->hasAnswers() !!}</td>
      <td class="text-center">{{$ticket->getRuCreatedAt()}}</td>
      <td class="text-center">
          <a href="#" class="modal-show" data-modal-content-url="{{route('CustomerTicketEdit', ['ticketId' => $ticket->id])}}" data-modal="#ticketModal" data-modal-title="Редактировать"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
          {{--<a href="{{route('CustomerTicketAction', ['ticketId' => $ticket->id, 'action' => 'archive'])}}" class="btn btn-xs btn-success handle-click" data-app-module="tickets" data-app-method="handleArchiveBtn">Архивировать!</a>--}}
      </td>
  </tr>
@endforeach

@if(!$tickets->count())
    <tr><td colspan="6" class="text-center">Данных не найдено</td></tr>
@endif