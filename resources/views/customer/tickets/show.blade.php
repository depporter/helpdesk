@extends('customer.layouts.master')

@section('title', 'Просмотр тикета #' . $ticket->id)


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Тикеты
                <small>Тикет #{{$ticket->id}} - {{$ticket->title}}</small>

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('CustomerHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li><a href="{{route('CustomerTicketActual')}}">Тикеты</a></li>
                <li class="active">Тикет #{{$ticket->id}}</li>
            </ol>

        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                            <h3 class="box-title">Тикет #{{$ticket->id}}</h3>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Переписка по тикету

                                            <span class="pull-right"><a href="{{route('CustomerTicketAction', ['ticketId' => $ticket->id, 'action' => 'archive'])}}" class="btn btn-xs btn-success handle-click" data-app-module="tickets" data-app-method="handleArchiveBtn" data-redirect-after="{{route('CustomerTicketActual')}}">Архивировать!</a></span>
                                        </div>


                                            <div class="box-body chat" id="chat-box">
                                                @foreach($ticket->messages as $message)

                                                    @if($message->author == 'support')
                                                        <div class="item">
                                                            <img src="/assets/app/customer/images/operatorl_placeholder.jpg" alt="user image" class="online">
                                                            <p class="message">
                                                                <a href="#" class="name">
                                                                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date("d.m.Y в H:i", strtotime($message->created_at))}}</small>
                                                                    @if($ticket->support)

                                                                        @if($ticket->support->super_user) <span class="text-danger">Администратор</span> @else Оператор @endif
                                                                    @endif

                                                                </a>
                                                                {!! nl2br($message->message) !!}
                                                            </p>

                                                        </div>
                                                    @endif

                                                    @if($message->author == 'customer')
                                                        <div class="item">
                                                            <img src="/assets/app/customer/images/user-placeholder-160x160.gif" alt="user image" class="offline">

                                                            <p class="message">
                                                                <a href="#" class="name">
                                                                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date("d.m.Y в H:i", strtotime($message->created_at))}}</small>
                                                                    Вы
                                                                </a>
                                                                {!! nl2br($message->message) !!}
                                                            </p>
                                                        </div>
                                                    @endif

                                                @endforeach

                                            </div>
                                            <!-- /.chat -->
                                            <div class="box-footer">
                                                <form action="{{route('CustomerTicketMessageStore', ['ticketId' => $ticket->id])}}" method="post" data-app-module="tickets" data-app-method="handleTicketMessageForm" id="ticketMessageForm">
                                                <div class="input-group">
                                                    <input class="form-control" name="message" placeholder="Ваше сообщение...">
                                                    <div class="input-group-btn">
                                                        <button type="submit" class="btn btn-success">Отправить</button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Файлы загруженные вами
                                            <span class="pull-right">
                                                <a type="button" id="fileUploadBtn" class="btn btn-success btn-xs handle-click" title="К загрузке разрешаются .jpeg, .jpg, .bmp, .png, .zip, .rar, .pdf" data-app-module="tickets" data-app-method="handleUploadBtn">Загрузить</a>
                                            <i class="fa fa-spinner fa-spin" id="spinner" aria-hidden="true" style="display: none"></i>
                                            </span>
                                        </div>
                                            <ul class="list-group customer-files" data-url="{{route('CustomerTicketGetFiles', ['ticketId' => $ticket->id, 'type' => 'customer'])}}">
                                                @foreach($ticket->customerFiles as $file)
                                                <li class="list-group-item"><a href="{{asset('storage/tickets_media/' . $file->orig_file_name)}}" target="_blank">{{str_limit($file->client_file_name, 35)}}</a></li>
                                                @endforeach

                                                @if(!$ticket->customerFiles->count())
                                                    <li class="list-group-item text-center">Еще не загружались</li>
                                                @endif
                                            </ul>

                                        <div class="panel-heading">Файлы от оператора @if($ticket->supportFiles->count()) <span class="pull-right"><a target="_blank" class="btn btn-xs btn-success" href="{{route('CustomerTicketDownload', ['ticketId' => $ticket->id])}}"><i class="fa fa-download" aria-hidden="true"></i> Скачать </a></span> @endif</div>
                                        <ul class="list-group">
                                            @foreach($ticket->supportFiles as $file)
                                                <li class="list-group-item"><a href="{{asset('storage/tickets_media/' . $file->orig_file_name)}}" target="_blank">{{str_limit($file->client_file_name, 35)}}</a></li>
                                            @endforeach

                                            @if($ticket->system_archived)

                                                    <li class="list-group-item text-center">Файлы были архивированны!</li>
                                                    <li class="list-group-item text-center">Напишите в саппорт!</li>
                                                @else

                                                    @if(!$ticket->supportFiles->count())
                                                        <li class="list-group-item text-center">Еще не загружались</li>
                                                    @endif
                                                @endif


                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


            </div>

        </section>
    </div>
    <form action="{{route('CustomerTicketUpload', ['ticketId' => $ticket->id])}}" method="post" id="uploadForm" data-app-module="tickets" data-app-method="handleUploadForm">
        <input id="fileSelector" name="files[]" type="file" multiple style="visibility:hidden" />
    </form>
@endsection

@push('js')
<script src="/assets/app/customer/js/modules/tickets.js?v=2"></script>
@endpush