
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') :: {{config('project.name')}}</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/dist/css/skins/skin-blue.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="/assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/ionicons-2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/assets/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" href="/assets/app/customer/css/common.css">

</head>
<body class="hold-transition login-page">
@yield('content')

<script src="/assets/admin-lte-2.3.7/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/assets/admin-lte-2.3.7/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/iCheck/icheck.min.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/datepicker/locales/ru.js"></script>
<script src="/assets/admin-lte-2.3.7/dist/js/app.js"></script>
<script src="/assets/jQuery-slimScroll-1.3.8/jquery.slimscroll.min.js"></script>
<script src="/assets/sweetalert/dist/sweetalert.min.js"></script>
<script src="/assets/app/customer/js/common.js"></script>
<script src="/assets/app/customer/js/modules/auth.js"></script>

</body>
</html>
