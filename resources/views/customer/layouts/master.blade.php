@inject('admin', 'App\Repos\CustomerRepo')
        <!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') :: HelpDesk</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->

    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/dist/css/skins/skin-blue.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="/assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/ionicons-2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/assets/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" href="/assets/app/customer/css/common.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">HD</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>HelpDesk</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{$avatar}}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{str_limit($fullName, 15)}} ID: {{$ID}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{$avatar}}" class="img-circle" alt="User Image">

                                <p>
                                    {{str_limit($fullName, 15)}}

                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Профиль</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{route('CustomerLogout')}}" class="btn btn-default btn-flat">Выход</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{$avatar}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{str_limit($fullName, 15)}}</p>
                    <!-- Status -->
                    <a href="#"> ID: {{$ID}}</a>
                </div>
            </div>


            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Навигация</li>
                <!-- Optionally, you can add icons to the links -->
                <li @if(Request::is('customers')) class="active" @endif ><a href="{{route('CustomerHome')}}"><i class="fa fa-home" aria-hidden="true"></i> Главная</a></li>
                <li @if(Request::is('payments')) class="active" @endif ><a href="{{route('CustomerPayments')}}"><i class="fa fa-rub" aria-hidden="true"></i> Платежи</a></li>

                    <li class="treeview active">
                        <a href="#"><i class="fa fa-balance-scale" aria-hidden="true"></i> <span>Тикеты</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if(Request::is('customers/tickets/create')) class="active" @endif ><a href="{{route('CustomerTicketCreate')}}"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Создать</a></li>
                            <li @if(Request::is('customers/tickets/actual')) class="active" @endif ><a href="{{route('CustomerTicketActual')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i> В работе</a></li>
                            <li @if(Request::is('customers/tickets/done')) class="active" @endif ><a href="{{route('CustomerTicketDone')}}"><i class="fa fa-flag-checkered" aria-hidden="true"></i> Завершенные</a></li>
                            <li @if(Request::is('customers/tickets/archive')) class="active" @endif ><a href="{{route('CustomerTicketArchive')}}"><i class="fa fa-archive" aria-hidden="true"></i> Архив</a></li>

                        </ul>
                    </li>

                @if(Auth::user()->nbki_credit_rating_type !== 'off' )
                    <li class="header">НБКИ</li>

                    @if(Auth::user()->nbki_credit_rating_type !== 'off')
                        <li  @if(Request::is('customers/nbki/credit-rating')) class="active" @endif><a href="{{route('CustomerNbkiCreditRating')}}"><i class="fa fa-line-chart" aria-hidden="true"></i> Кредитный рейтинг</a></li>
                    @endif

                    @if(Auth::user()->actualization_contacts_type !== 'off')
                        <li  @if(Request::is('customers/nbki/contact')) class="active" @endif><a href="{{route('CustomerNbkiContact')}}"><i class="fa fa-phone" aria-hidden="true"></i> Актуализация контактов</a></li>
                    @endif

                @endif




            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

@yield('content')



<!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            <img height="20" src="/assets/app/customer/images/dev-logo.png">
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{date('Y')}} <a href="#">{{config('project.name')}}</a>.</strong> Все права защищены.
    </footer>


</div>
<!-- ./wrapper -->




<script src="/assets/admin-lte-2.3.7/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/assets/admin-lte-2.3.7/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/iCheck/icheck.min.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/datepicker/locales/ru.js"></script>
<script src="/assets/admin-lte-2.3.7/dist/js/app.js"></script>
<script src="/assets/jQuery-slimScroll-1.3.8/jquery.slimscroll.min.js"></script>
<script src="/assets/sweetalert/dist/sweetalert.min.js"></script>
<script src="/assets/app/customer/js/common.js"></script>

<!-- InputMask -->
<script src="/assets/admin-lte-2.3.7/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/input-mask/jquery.inputmask.extensions.js"></script>
@stack('js')



</body>
</html>
