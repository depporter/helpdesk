@extends('customer.layouts.master')

@section('title', 'Главная страница')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Главная страница
                <small>HelpDesk</small>
            </h1>

        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <h3 class="box-title">Контакты</h3>


                        </div>
                        <div class="box-body">{!! $contacts !!}</div>

                        <div class="box-header">
                            <i class="fa fa-rub" aria-hidden="true"></i><h3 class="box-title">Ваш баланс {{number_format($balance, 0, '.', '.')}}</h3>
                        </div>


                        <div class="box-header">
                            <i class="fa fa-list" aria-hidden="true"></i>
                            <h3 class="box-title">Ваши лимиты</h3>

                        </div>
                        @if($customer->ticketCategories)
                            <ul class="list-group">
                                @foreach($customer->ticketCategories as $category)

                                    @if($category->type == 'limit_monthly')
                                        <li class="list-group-item"><span class="badge">Лимит: {{$category->pivot->ticket_limit}} в мес. @if(isset($ticketsCategories[$category->id])) Осталось {{$category->pivot->ticket_limit - $ticketsCategories[$category->id]}} @endif</span>{{$category->name}} </li>
                                    @endif

                                    @if($category->type == 'limit_countable')
                                        <li class="list-group-item"><span class="badge">Осталось: {{$category->pivot->ticket_limit}}</span>{{$category->name}} </li>
                                    @endif

                                    @if($category->type == 'by_price')
                                            <li class="list-group-item"><span class="badge">{{$category->price}} р./тикет</span>{{$category->name}} </li>
                                    @endif

                                @endforeach

                                @if($customer->nbki_credit_rating_type != 'off')
                                    @if($customer->nbki_credit_rating_type == 'query')
                                            <li class="list-group-item"><span class="badge">{{$customer->nbki_credit_rating_query_price}} р.</span>Кредитный рейтинг. Стоимость запроса  </li>
                                    @endif

                                        @if($customer->nbki_credit_rating_type == 'package')
                                            <li class="list-group-item"><span class="badge">Лимит:{{$customer->nbki_credit_rating_package_amount}}. Осталось: {{$customer->nbki_credit_rating_package_amount - $nbkiThisMonthTotal}}</span>Кредитный рейтинг. Пакет.</li>
                                        @endif
                                @endif

                                    @if($customer->actualization_contacts_type != 'off')
                                        @if($customer->actualization_contacts_type == 'query')
                                            <li class="list-group-item"><span class="badge">{{$customer->actualization_contacts_query_price}} р.</span>Кредитный рейтинг. Стоимость запроса  </li>
                                        @endif

                                        @if($customer->actualization_contacts_type == 'package')
                                            <li class="list-group-item"><span class="badge">Лимит:{{$customer->actualization_contacts_package_amount}}. Осталось: {{$customer->actualization_contacts_package_amount - $nbkiThisMonthTotalContact}}</span>Актуализация контактов. Пакет.</li>
                                        @endif
                                    @endif

                            </ul>
                        @endif

                        <div class="box-header">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <h3 class="box-title">Тикеты с ответами</h3>


                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center" width="50">ID</th>
                                <th>Заголовок</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ticketsWithAnswers as $ticket)
                                <tr>
                                    <td class="text-center">{{$ticket->id}}</td>
                                    <td><a target="_blank" href="{{route('CustomerTicketShow', ['ticketId' => $ticket->id])}}">{{$ticket->title}}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            <h3 class="box-title">Новости</h3>


                        </div>
                        <!-- /.box-header -->

                        <ul class="list-group">
                            @foreach($news as $item)
                                <li class="list-group-item">
                                    <div><a href="#" class="modal-show" data-modal-content-url="{{route('CustomerHomeGetNews', ['newsId' => $item->id])}}" data-modal="#newsModal" data-modal-title="Просмотр новости">{{$item->title}}</a></div>
                                    <small>{{$item->getRuCreatedAt()}}</small>
                                </li>
                            @endforeach
                        </ul>

                        <div class="box-header">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            <h3 class="box-title">Важная информация</h3>

                        </div>
                        <ul class="list-group">
                            @foreach($pages as $item)
                                <li class="list-group-item">
                                    <div><a href="#" class="modal-show" data-modal-content-url="{{route('CustomerHomeGetPage', ['pageId' => $item->id])}}" data-modal="#newsModal" data-modal-title="Просмотр страницы">{{$item->title}}</a></div>
                                    <small>{{$item->getRuCreatedAt()}}</small>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                    <!-- /.box -->
                </div>


            </div>



        </section>
    </div>

    <div class="modal fade" id="newsModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection