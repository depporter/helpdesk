@extends('customer.layouts.auth')
@section('title', 'Авторизация')

@section('content')
    <div class="login-box ">

        <div class="login-box-body">
            <p class="login-box-msg">Авторизуйтесь перед началом работы</p>

            <form action="{{route('CustomerPostLogin')}}" method="post" data-success-url="{{route('CustomerHome')}}" data-app-module="auth" data-app-method="handleLoginForm" id="loginForm">
                <div class="form-group has-feedback">
                    <input type="text" name="email" class="form-control" placeholder="E-mail">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <span class="help-block"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Пароль">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <span class="help-block"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">

                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>


            <br>

        </div>
        <!-- /.login-box-body -->
    </div>
@endsection