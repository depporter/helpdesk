<form action="{{route('CustomerNbkiContactStore')}}" method="post" id="contactForm">

    <div class="text-danger">
        @if($customer->actualization_contacts_type == 'query')
            Цена за запрос {{$customer->actualization_contacts_query_price}}
        @endif

            @if($customer->actualization_contacts_package_amount == 'query')
                Пакет {{$customer->actualization_contacts_package_amount}} запросов. Осталось {{$customer->actualization_contacts_package_amount - $queriesTotal}}
            @endif
    </div>

    <div class="form-group">
        <label for="first_name">Фамилия *</label>
        <input type="text" name="first_name" id="first_name" class="form-control">
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="last_name">Имя *</label>
        <input type="text" name="last_name" id="last_name" class="form-control">
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="middle_name">Отчество</label>
        <input type="text" name="middle_name" id="middle_name" class="form-control">
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="date_birth">Дата рождения</label>
        <input type="text" name="date_birth" id="date_birth" class="form-control datemask">
        <span class="help-block"></span>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="doc_series">Серия паспорта *</label>
                <input type="text" name="doc_series" id="doc_series" class="form-control"
                       placeholder="Первые 4 символа">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="col-md-6">

            <div class="form-group">
                <label for="doc_number">Номер паспорта *</label>
                <input type="text" name="doc_number" id="doc_number" class="form-control"
                       placeholder="Последние 6 символов">
                <span class="help-block"></span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="doc_issue_date">Дата выдачи паспорта</label>
        <input type="text" name="doc_issue_date" id="doc_issue_date" class="form-control datemask">
        <span class="help-block"></span>
    </div>

    {{--<div class="form-group">--}}
        {{--<label for="mode">Режим поиска контактных данных</label>--}}
        {{--<input type="text" name="mode" id="mode" class="form-control">--}}
        {{--<span class="help-block"></span>--}}
    {{--</div>--}}

    <div class="form-group">
        <label>Период актуальности</label>
        <select class="form-control select2 select2-hidden-accessible" name="months"
                style="width: 100%;" tabindex="-1" aria-hidden="true">

            <option value="0">За все время</option>
        @for($i = 1; $i <= 36; $i++)
                <option value="{{$i}}">{{$i}} мес.</option>
            @endfor
        </select>
    </div>

    <div class="form-group">
        <label for="excluded_phones">Исключить номера (через запятую)</label>
        {{--<input type="text" name="phones" id="phones" class="form-control">--}}
        <textarea rows="3" cols="10" class="form-control" id="excluded_phones" name="excluded_phones"></textarea>
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="limit">Количество номеров *</label>
        <input type="text" name="limit" id="limit" class="form-control" placeholder="Максимум 50 номеров">
        <span class="help-block"></span>
    </div>

    <button type="submit" class="btn btn-success">Отправить</button>
</form>