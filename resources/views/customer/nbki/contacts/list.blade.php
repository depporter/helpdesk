@foreach($contacts as $contact)
    <tr>
        <td class="text-center">{{$contact->id}}</td>
        <td class="text-center">{{$contact->request_id}}</td>
        <td class="text-center">{{$contact->created_at->format('d.m.Y')}}</td>
        <td><a href="{{route('CustomerNbkiContactResult', ['id' => $contact])}}" class="contactResult">{{$contact->first_name}} {{$contact->last_name}} {{$contact->middle_name}}</a></td>
        <td>{{$contact->doc_series}} {{$contact->doc_number}}</td>
        {{--<td class="text-center">{{round($contact->cost)}} р.</td>--}}

    </tr>
@endforeach

<tr>
    <td class="text-center" colspan="7">{{$contacts->links()}}</td>
</tr>