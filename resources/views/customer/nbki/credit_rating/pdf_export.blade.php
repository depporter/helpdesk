
<html>
<head>
<title>Экспорт</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style>
        table, td, th {
            border: 1px solid black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            background-color: #F5F5F5;
            padding: 5px;
        }

        td {
            padding: 5px;
            font-size: 12px;
        }
    </style>

</head>

<body>

<h2>{{$result->first_name}} {{$result->last_name}} {{$result->middle_name}}, Серия: {{$result->doc_series}} Номер: {{$result->doc_number}}</h2>


        <h3 >Кредитный рейтинг</h3>

        <table>
            <thead>
            <tr>
                <th >Данные</th>
                <th >Результат</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Количество активных счетов</td>
                <td>{{$response['response']['loanRating']['count']}}</td>
            </tr>

            <tr>
                <td>Количество активных счетов, в которых субъект выступает в качестве поручителя</td>
                <td>{{$response['response']['loanRating']['guarantee']}}</td>
            </tr>

            <tr>
                <td>Суммарный лимит активных кредитов</td>
                <td>{{$response['response']['loanRating']['limit']}}</td>
            </tr>

            <tr>
                <td>Суммарный баланс по активным кредитам</td>
                <td>{{$response['response']['loanRating']['balance']}}</td>
            </tr>

            <tr>
                <td>Максимальный срок закрытия активного кредита</td>
                <td>{{$response['response']['loanRating']['balance']}}</td>
            </tr>

            <tr>
                <td>Максимальная текущая просрочка по активным кредитам</td>
                <td>@if(!is_array($response['response']['loanRating']['maxCloseDt'])) {{$response['response']['loanRating']['maxCloseDt']}} @endif</td>
            </tr>

            <tr>
                <td>Суммарная просроченная задолженность по всем активным счетам</td>
                <td>{{$response['response']['loanRating']['totalOverdue']}}</td>
            </tr>

            <tr>
                <td>Максимальная историческая просрочка по активным кредитам</td>
                <td>@if(!is_array($response['response']['loanRating']['maxDeleay'])) {{$response['response']['loanRating']['maxDeleay']}} @endif</td>
            </tr>

            <tr>
                <td>Наличие негатива в закрытых кредитах</td>
                <td>@if($response['response']['loanRating']['closedNegative'] == 'N') нет @else да @endif</td>
            </tr>
            </tbody>
        </table>

        <h3 >Статистика просрочек</h3>
        <table>
            <thead>
            <tr>
                <th >Просрочка</th>
                <th >По открытым счетам</th>
                <th >По закрытым счетам</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>30 - 59 дней за последние 12 месяцев</td>
                <td>{{$response['response']['loanRating']['countDue30_60InOpenedAccs']}}</td>
                <td>{{$response['response']['loanRating']['countDue30_60InClosedAccs']}}</td>
            </tr>

            <tr>
                <td>60 - 89 дней за последние 12 месяцев</td>
                <td>{{$response['response']['loanRating']['countDue60_90InOpenedAccs']}}</td>
                <td>{{$response['response']['loanRating']['countDue60_90InClosedAccs']}}</td>
            </tr>

            <tr>
                <td>90+ дней за все время</td>
                <td>{{$response['response']['loanRating']['countDue90PlusInOpenedAccs']}}</td>
                <td>{{$response['response']['loanRating']['countDue90PlusInClosedAccs']}}</td>
            </tr>
            </tbody>
        </table>

        <h3 >Верификация паспорта</h3>
        <table>
            <thead>
            <tr>
                <th>Тип проверки</th>
                <th  >Результат</th>

            </tr>
            </thead>
            <tbody>

            <tr>
                <td>Паспорт из запроса найден</td>
                <td >
                    @if($response['response']['docCheck']['found'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['found'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['found']) Нет информации @endif
                </td>
            </tr>

            <tr>
                <td>Есть более свежий паспорт</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['hasNewer'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['hasNewer'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['hasNewer']) Нет информации @endif
                </td>
            </tr>

            <tr>
                <td>Признак утери паспорта</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['lost'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['lost'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['lost']) Нет информации @endif
                </td>
            </tr>

            <tr>
                <td>Признак недействительности паспорта</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['invalid'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['invalid'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['invalid']) Нет информации @endif
                </td>
            </tr>

            <tr>
                <td>Розыск</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['wanted'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['wanted'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['wanted']) Нет информации @endif
                </td>
            </tr>

            </tbody>
        </table>

        <h3 class="panel-title">Запросы в бюро</h3>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Запросы</th>
                <th  class="text-center">Количество</th>

            </tr>
            </thead>
            <tbody>

            <tr>
                <td>За последние 7 дней</td>
                <td class="text-center">{{$response['response']['inquiries']['week']}}</td>
            </tr>

            <tr>
                <td>За последние 14 дней</td>
                <td class="text-center">{{$response['response']['inquiries']['twoWeeks']}}</td>
            </tr>

            <tr>
                <td>За последний месяц</td>
                <td class="text-center">{{$response['response']['inquiries']['month']}}</td>
            </tr>

            <tr>
                <td>Всего</td>
                <td class="text-center">{{$response['response']['inquiries']['overall']}}</td>
            </tr>


            </tbody>
        </table>

        <h3 class="panel-title">Запросы по кредитному рейтингу</h3>

        <table >
            <thead>
            <tr>
                <th  >Дата</th>
                <th >Тип</th>
                <th  >Сумма</th>
                <th  >Срок</th>
            </tr>
            </thead>
            <tbody>

            @if(isset($response['response']['checks']['check']['date']))
                <tr>
                    <td >{{date('d.m.Y', strtotime($response['response']['checks']['check']['date']))}}</td>
                    <td >{{$response['response']['checks']['check']['loanType']}}</td>
                    <td >{{$response['response']['checks']['check']['loanAmount']}}</td>
                    <td >@if(!is_array($response['response']['checks']['check']['loanDuration'])) {{$response['response']['checks']['check']['loanDuration']}} @endif</td>
                </tr>
                @else
                @foreach($response['response']['checks']['check'] as $check)
                    <tr>
                        <td >{{date('d.m.Y', strtotime($check['date']))}}</td>
                        <td >{{$check['loanType']}}</td>
                        <td >{{$check['loanAmount']}}</td>
                        <td >@if(!is_array($check['loanDuration'])) {{$check['loanDuration']}} @endif</td>
                    </tr>
                @endforeach
            @endif

            </tbody>
        </table>
</body>
</html>




