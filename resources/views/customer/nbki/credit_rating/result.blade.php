

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Паспорт</div>
            <div class="panel-body">
                <div>Паспорт найден

                        @if($response['response']['docCheck']['found'] == 'Y') <span class="badge bg-green pull-right">Да</span> @endif
                        @if($response['response']['docCheck']['found'] == 'N') <span class="badge bg-red pull-right">Нет</span> @endif
                        @if(!$response['response']['docCheck']['found']) -- @endif

                </div>

                <div>
                    Паспорт не действителен

                @if($response['response']['docCheck']['invalid'] == 'Y') <span class="badge bg-red pull-right">Да</span> @endif
                        @if($response['response']['docCheck']['invalid'] == 'N') <span class="badge bg-green pull-right">Нет</span> @endif
                        @if(!$response['response']['docCheck']['invalid']) -- @endif

                </div>

                <div>
                   В розыске

                 @if($response['response']['docCheck']['wanted'] == 'Y') <span class="badge bg-red pull-right">Да</span> @endif
                        @if($response['response']['docCheck']['wanted'] == 'N') <span class="badge bg-green pull-right">Нет</span> @endif
                        @if(!$response['response']['docCheck']['wanted']) Нет информации @endif

                </div>
            </div>
        </div>


    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Кредиты</div>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Просрочка</th>
                        <th width="200">Открытые</th>
                        <th width="200">Закрытые</th>
                    </tr>
                    </thead>
                    @if(isset($response['response']['loanRating']))
                    <tbody>

                    <tr>
                        <td>30 - 59 дн</td>
                        <td class="text-center">{{$response['response']['loanRating']['countDue30_60InOpenedAccs']}}</td>
                        <td class="text-center">{{$response['response']['loanRating']['countDue30_60InClosedAccs']}}</td>
                    </tr>

                    <tr>
                        <td>60 - 89 дн</td>
                        <td class="text-center">{{$response['response']['loanRating']['countDue60_90InOpenedAccs']}}</td>
                        <td class="text-center">{{$response['response']['loanRating']['countDue60_90InClosedAccs']}}</td>
                    </tr>

                    <tr>
                        <td>90+ дн</td>
                        <td class="text-center">{{$response['response']['loanRating']['countDue90PlusInOpenedAccs']}}</td>
                        <td class="text-center">{{$response['response']['loanRating']['countDue90PlusInClosedAccs']}}</td>
                    </tr>
                    </tbody>
                        @endif
                </table>

        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Обращения в банки</div>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Запросы</th>
                        <th width="100" class="text-center">Количество</th>

                    </tr>
                    </thead>
                    @if(isset($response['response']['inquiries']))
                    <tbody>

                    <tr>
                        <td>За последние 7 дней</td>
                        <td class="text-center">{{$response['response']['inquiries']['week']}}</td>
                    </tr>

                    <tr>
                        <td>За последние 14 дней</td>
                        <td class="text-center">{{$response['response']['inquiries']['twoWeeks']}}</td>
                    </tr>

                    <tr>
                        <td>За последний месяц</td>
                        <td class="text-center">{{$response['response']['inquiries']['month']}}</td>
                    </tr>

                    <tr>
                        <td>Всего</td>
                        <td class="text-center">{{$response['response']['inquiries']['overall']}}</td>
                    </tr>


                    </tbody>
                    @endif
                </table>

        </div>
    </div>
</div>

@if(
    isset($response['response']['loanRating']) &&
    isset($response['response']['inquiries']) && ($response['response']['loanRating']['count'] > 6 || $response['response']['loanRating']['totalOverdue'] > 0 || $response['response']['loanRating']['count'] > 6 || $response['response']['inquiries']['month'] > 7 ))
    <div class="panel panel-danger">
        <div class="panel-heading">Стоп факторы</div>
        <div class="panel-body">
        <div class="row">

            @if(isset($response['response']['loanRating']) && $response['response']['loanRating']['totalOverdue'] > 0)
                <div class="col-md-3">
                    Действующая просрочка: {{$response['response']['loanRating']['totalOverdue']}} р.
                </div>
            @endif

            @if(isset($response['response']['loanRating']) && $response['response']['loanRating']['count'] > 6)
                <div class="col-md-3">
                    Активные счета: <b>более 6</b>
                </div>
            @endif

            @if(isset($response['response']['inquiries']) && $response['response']['inquiries']['month'] > 7)
                <div class="col-md-3">
                    Запросов за последний месяц: <b>более 7</b>
                </div>
            @endif

            @if(isset($response['response']['loanRating']) && $response['response']['loanRating']['count'] > 6)
                <div class="col-md-3">
                    Активные счета:
                    <b>более 6</b>
                </div>
            @endif

        </div>
        </div>

    </div>

@endif

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Счета</div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Данные</th>
                    <th width="100">&nbsp;</th>
                </tr>
                </thead>
                @if(isset($response['response']['loanRating']))
                <tbody>
                <tr>
                    <td>Активные счета (заемщик)</td>
                    <td>{{$response['response']['loanRating']['count']}}</td>
                </tr>

                <tr>
                    <td>Активные счета (поручитель)</td>
                    <td>{{$response['response']['loanRating']['guarantee']}}</td>
                </tr>

                <tr>
                    <td>Сумма действующих кредитов</td>
                    <td>{{$response['response']['loanRating']['limit']}}</td>
                </tr>

                <tr>
                    <td>Выплачено по кредитам</td>
                    <td>{{$response['response']['loanRating']['balance']}}</td>
                </tr>

                <tr>
                    <td>Дата погашения последнего кредита</td>
                    <td>@if(!is_array($response['response']['loanRating']['maxCloseDt'])) {{$response['response']['loanRating']['maxCloseDt']}} @else -- @endif</td>
                </tr>
                </tbody>
                    @endif
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Просрочки по активным кредитам</div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Данные</th>
                    <th width="100">&nbsp;</th>
                </tr>
                </thead>
                @if(isset($response['response']['loanRating']))
                <tbody>
                <tr>
                    <td>Действующая просрочка</td>
                    <td>@if(!is_array($response['response']['loanRating']['delay'])){{$dict[$response['response']['loanRating']['delay']]}} @else -- @endif</td>
                </tr>

                <tr>
                    <td>Просрочка по всем активным счетам</td>
                    <td>{{$response['response']['loanRating']['totalOverdue']}}</td>
                </tr>

                <tr>
                    <td>Максимальный срок просрочки по активным кредитам</td>
                    <td>@if(!is_array($response['response']['loanRating']['maxDeleay'])){{$dict[$response['response']['loanRating']['maxDeleay']]}} @else -- @endif</td>
                </tr>

                <tr>
                    <td>Наличие негативной информации в закрытых счетах</td>
                    <td>@if($response['response']['loanRating']['closedNegative'] == 'Y') Да @else Нет @endif</td>
                </tr>


                </tbody>
                    @endif
            </table>
        </div>
    </div>
</div>











