@foreach($ratings as $rating)
    <tr>
        <td class="text-center">{{$rating->id}}</td>
        <td class="text-center">{{$rating->request_id}}</td>
        <td class="text-center">{{$rating->created_at->format('d.m.Y')}}</td>
        <td><a href="{{route('CustomerNbkiCreditRatingResult', ['id' => $rating])}}" class="creditRatingResult">{{$rating->first_name}} {{$rating->last_name}} {{$rating->middle_name}}</a></td>
        <td>{{$rating->doc_series}} {{$rating->doc_number}}</td>
        <td class="text-center">{{round($rating->cost)}} р.</td>

    </tr>
@endforeach

<tr>
    <td class="text-center" colspan="7">{{$ratings->links()}}</td>
</tr>