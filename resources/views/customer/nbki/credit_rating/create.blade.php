<form action="{{route('CustomerNbkiCreditRatingStore')}}" method="post" id="creditRatingForm">

    <div class="text-danger">
        @if($customer->nbki_credit_rating_type == 'query')
            Цена за запрос {{$customer->nbki_credit_rating_query_price}}
        @endif

            @if($customer->nbki_credit_rating_package_amount == 'query')
                Пакет {{$customer->nbki_credit_rating_package_amount}} запросов. Осталось {{$customer->nbki_credit_rating_package_amount - $queriesTotal}}
            @endif
    </div>

    <div class="form-group">
        <label for="first_name">Фамилия *</label>
        <input type="text" name="first_name" id="first_name" class="form-control">
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="last_name">Имя *</label>
        <input type="text" name="last_name" id="last_name" class="form-control">
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="middle_name">Отчество</label>
        <input type="text" name="middle_name" id="middle_name" class="form-control">
        <span class="help-block"></span>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="doc_series">Серия паспорта *</label>
                <input type="text" name="doc_series" id="doc_series" class="form-control"
                       placeholder="Первые 4 символа">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="col-md-6">

            <div class="form-group">
                <label for="doc_number">Номер паспорта *</label>
                <input type="text" name="doc_number" id="doc_number" class="form-control"
                       placeholder="Последние 6 символов">
                <span class="help-block"></span>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Отправить</button>
</form>