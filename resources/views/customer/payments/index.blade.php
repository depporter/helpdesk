@extends('customer.layouts.master')

@section('title', 'История платежей')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                История платежей

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('CustomerHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">История платежей</li>
            </ol>

        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <h3 class="box-title">Пополнения</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th class="text-center" width="80">#</th>
                                    <th class="text-center">Сумма</th>
                                    <th>Назначение</th>
                                    <th width="150" class="text-center">Дата</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($topUps as $topUp)
                                    <tr>
                                        <td class="text-center">{{$topUp->id}}</td>
                                        <td class="text-center">{{number_format($topUp->amount, 0, '.', '.')}}</td>
                                        <td>{{$topUp->desc}}</td>
                                        <td class="text-center">{{$topUp->created_at->format('d.m.Y H:i')}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <h3 class="box-title">Траты</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th class="text-center" width="80">#</th>
                                    <th class="text-center">Сумма</th>
                                    <th>Назначение</th>
                                    <th width="150" class="text-center">Дата</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($expenses as $expense)
                                    <tr>
                                        <td class="text-center">{{$expense->id}}</td>
                                        <td class="text-center">{{number_format($expense->amount, 0, '.', '.')}}</td>
                                        <td>{{$expense->comment}}</td>
                                        <td class="text-center">{{$expense->created_at->format('d.m.Y H:i')}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="ticketModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/customer/js/modules/tickets.js"></script>
@endpush