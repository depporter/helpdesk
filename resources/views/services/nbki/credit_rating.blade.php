<?php echo '<?xml version="1.0" encoding="windows-1251" ?>' ?>
<credit_rating>
    <auth>
        <login>{{$requestData['login']}}</login>
        <password>{{$requestData['password']}}</password>
    </auth>

    <person>
        <lastname>{{$requestData['firstName']}}</lastname>
        <firstname>{{$requestData['lastName']}}</firstname>
    </person>

    <document>
        <number>{{$requestData['number']}}</number>
        <series>{{$requestData['series']}}</series>
    </document>

    <istest>{{$requestData['isTest']}}</istest>
</credit_rating>