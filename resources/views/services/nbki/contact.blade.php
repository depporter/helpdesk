<?php echo '<?xml version="1.0" encoding="windows-1251" ?>' ?>
<request>
    <uid>2782796</uid>
    <auth>
        <login>{{$requestData['login']}}</login>
        <password>{{$requestData['password']}}</password>
    </auth>

    <person>
        <lastname>{{$requestData['firstName']}}</lastname>
        <firstname>{{$requestData['lastName']}}</firstname>
        <middlename>{{$requestData['middleName']}}</middlename>
        <datebirth>{{date("d.m.Y", strtotime($requestData['dateBirth']))}}</datebirth>

        <phones>
            @if(count($requestData['phones']) > 1)
                @foreach ($requestData['phones'] as $phone)
                    <phone>{{$phone}}</phone>
                @endforeach
            @endif
        </phones>
    </person>

    <document>
        <number>{{$requestData['number']}}</number>
        <series>{{$requestData['series']}}</series>
        <issuedate>{{date("d.m.Y", strtotime($requestData['issueDate']))}}</issuedate>
    </document>

    <params>
        <nbsk01>
            <months>{{$requestData['months']}}</months>
            <limit>{{$requestData['limit']}}</limit>
            <mode>0</mode>
        </nbsk01>
    </params>

    <types>
        <type>NBSK01</type>
    </types>

    <istest>0</istest>
</request>