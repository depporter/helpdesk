<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Заявка на регистрацию в информационной системе</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="/frontend/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="/frontend/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/frontend/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/frontend/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/frontend/img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <link href="/frontend/css/responsive.css" rel="stylesheet">
    <link href="/frontend/css/menu.css" rel="stylesheet">
    <link href="/frontend/css/animate.min.css" rel="stylesheet">
    <link href="/frontend/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    <link href="/frontend/css/skins/square/grey.css" rel="stylesheet">
    <link href="/frontend/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="/frontend/css/custom.css" rel="stylesheet">
    <link href="/frontend/css/app.css" rel="stylesheet">

    <script src="/frontend/js/modernizr.js"></script>
    <!-- Modernizr -->

</head>

<body>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div><!-- /Preload -->

<main>
    <div id="form_container">
        <div class="row">

            <div class="col-md-5">
                <div id="left_form">

                    <figure><img width="65%" src="/frontend/img/logo-is.png" alt=""></figure>

                    <h2 style="font-size: 25px;">Кто владеет информацией, тот владеет миром.</h2>
                    <p>Уважаемый посетитель. На данном сайте Вы можете отправить заявку на регистрацию в информационной системе.</p>

                </div>
            </div>
            <div class="col-md-7">

                <div id="wizard_container" >

                    <!-- /top-wizard -->
                    <form action="{{route('frontend.home.register')}}"  method="post" id="regForm">
                        {{ csrf_field() }}
                        <div id="middle-wizard">

                            <div class="step">


                                <p>Укажите интересующие услуги</p>

                                <div class=" services">
                                    <div style="margin-top: 5px">
                                        <a href="#" data-service="road_camera" class="btn btn-info btn-block service" ><span class="marker"></span> Дорожные камеры</a>
                                    </div>

                                    <div style="margin-top: 5px">
                                        <a href="#" data-service="rosreestr" class="btn btn-info btn-block service" ><span class="marker"></span> Росреестр</a>
                                    </div>

                                    <div style="margin-top: 5px">
                                        <a href="#" data-service="check_sb" class="btn btn-info btn-block service" ><span class="marker"></span> Проверка СБ</a>
                                    </div>

                                    <div style="margin-top: 5px">
                                        <a href="#" data-service="credit_history"  class="btn btn-info btn-block service" ><span class="marker"></span> Кредитная История</a>
                                    </div>


                                </div>


                                <br>
                                <p>Укажите ваши данные</p>

                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control required" placeholder="Имя или название компании">
                                    <span class="help-block"></span>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="email" id="email" class="form-control required" placeholder="Почта">
                                    <span class="help-block"></span>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="phone" id="phone" class="form-control required" placeholder="Телефон">
                                    <span class="help-block"></span>
                                </div>

                                <div id="services_inputs">

                                </div>
                                <button type="submit" class="btn btn-success">Регистрация</button>
                                <a href="/customers/auth/login" class="btn btn-info">Личный кабинет</a>

                            </div>
                        </div>


                    </form>
                </div>

            </div>
        </div>
    </div>
</main>

<footer id="home" class="clearfix">
    <p>© 2017</p>
</footer>


<div class="cd-overlay-nav">
    <span></span>
</div>


<div class="cd-overlay-content">
    <span></span>
</div>



<!-- Modal info -->
<div class="modal fade" id="more-info" tabindex="-1" role="dialog" data-backdrop="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="more-infoLabel">Реквизиты для оплаты</h4>
            </div>
            <div class="modal-body">
                <p>Мы приняли ваши данные. Пожалуйста осуществите перевод по номеру карты:<br>
                    Карта: <strong>5469 1600 2657 3852</strong><br>
                    Имя получателя: <strong>Данил Валерьевич.</strong>
                </p>


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- SCRIPTS -->
<!-- Jquery-->
<script src="/frontend/js/jquery-2.2.4.min.js"></script>
<!-- Common script -->
<!-- <script src="/frontend/js/common_scripts_min.js"></script> -->
<!-- Wizard script -->
<!-- <script src="/frontend/js/registration_wizard_func.js"></script> -->
<!-- Menu script -->
<script src="/frontend/js/velocity.min.js"></script>
<script src="/frontend/js/main.js"></script>
<!-- Theme script -->
<script src="/frontend/js/functions.js"></script>
<script src="/frontend/js/bootstrap.min.js"></script>
<script src="/frontend/vendors/sweetalert2/dist/sweetalert2.min.js"></script>
<!-- Google map -->



<script src="/frontend/js/app.js"></script>

</body>
</html>