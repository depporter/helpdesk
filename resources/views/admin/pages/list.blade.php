@foreach($pages as $item)
    <tr>
        <td class="text-center">{{$item->id}}</td>
        <td>{{$item->title}}</td>
        <td class="text-center">{{$item->getRuCreatedAt()}}</td>
        <td class="text-center">{!! $item->isPublish() !!}</td>
        <td class="text-center"><a href="#" class="modal-show" data-modal-content-url="{{route('AdminPagesEdit', ['pageId' => $item->id])}}" data-modal="#pagesModal" data-modal-title="Редактировать страницу"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
    </tr>
@endforeach