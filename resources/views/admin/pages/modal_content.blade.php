<form action="{{$formAction}}" method="post" data-app-module="pages" data-app-method="handlePagesForm" id="pagesForm">

    <div class="form-group">
        <label for="title">Заголовок *</label>
        <input type="text" class="form-control" id="title" name="title" @if(isset($item)) value="{{$item->title}}" @endif>
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="content">Содержание *</label>
        <textarea name="content" class="form-control" id="content">@if(isset($item)){{$item->content}}@endif</textarea>
        <span class="help-block"></span>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="publish" @if(isset($item) && $item->publish) checked @endif > Опубликована
        </label>
    </div>

    <button type="submit" class="btn btn-primary">@if(!isset($item)) Создать @else Сохранить @endif</button>
</form>


