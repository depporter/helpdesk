@inject('admin', 'App\Repos\AdminRepo')
        <!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') :: HelpDesk</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->

    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/dist/css/skins/skin-blue.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="/assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/ionicons-2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/assets/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" href="/assets/admin-lte-2.3.7/plugins/datepicker/datepicker3.css">

    <link rel="stylesheet" href="/assets/app/admin/css/common.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">HD</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>HelpDesk</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{$avatar}}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{str_limit($fullName, 15)}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{$avatar}}" class="img-circle" alt="User Image">

                                <p>
                                    {{str_limit($fullName, 15)}}

                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Профиль</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{route('AdminLogout')}}" class="btn btn-default btn-flat">Выход</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{$avatar}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{str_limit($fullName, 15)}}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>


            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Навигация</li>
                <!-- Optionally, you can add icons to the links -->

                @if(Auth::guard('admin')->user()->super_user)
                <li @if(Request::is('admins/manage/*')) class="treeview active" @else class="treeview" @endif>
                    <a href="#"><i class="fa fa-balance-scale" aria-hidden="true"></i> <span>Управление</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li @if(Request::is('admins/manage/tickets*')) class="active" @endif ><a href="{{route('AdminManageTickets')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i> Тикеты</a></li>
                        <li @if(Request::is('admins/manage/contacts*')) class="active" @endif ><a href="{{route('AdminContacts')}}"><i class="fa fa-phone" aria-hidden="true"></i> Контакты</a></li>
                        <li @if(Request::is('admins/manage/archives*')) class="active" @endif ><a href="{{route('AdminArchives')}}"><i class="fa fa-archive" aria-hidden="true"></i> Архивы</a></li>
                    </ul>
                </li>
                @endif

                @if($admin->can('manage_news') || $admin->can('static_pages'))
                    <li @if(Request::is('admins/content/*')) class="treeview active" @else class="treeview" @endif>
                        <a href="#"><i class="fa fa-book" aria-hidden="true"></i> <span>Контент</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if(Request::is('admins/content/news*')) class="active" @endif ><a href="{{route('AdminNews')}}"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Новости</a></li>
                            <li @if(Request::is('admins/content/pages*')) class="active" @endif ><a href="{{route('AdminPages')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> Страницы</a></li>
                        </ul>
                    </li>
                @endif


                @if($admin->can('manage_customers') || $admin->can('manage_admins'))
                    <li @if(Request::is('admins/users/*')) class="treeview active" @else class="treeview" @endif>
                        <a href="#"><i class="fa fa-users" aria-hidden="true"></i> <span>Пользователи</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            @if($admin->can('manage_admins'))
                            <li @if(Request::is('admins/users/admin*')) class="active" @endif ><a href="{{route('AdminUsersAdmin')}}"><i class="fa fa-user-secret" aria-hidden="true"></i> Пользователи</a></li>
                            @endif

                            @if($admin->can('manage_customers'))
                            <li @if(Request::is('admins/users/customer*')) class="active" @endif ><a href="{{route('AdminUsersCustomer')}}"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Клиенты</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if($admin->can('catalogs_tickets_categories'))
                    <li @if(Request::is('admins/catalogs*')) class="treeview active" @else class="treeview" @endif>
                        <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i> <span>Справочники</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if(Request::is('admins/catalogs/ticket-categories*')) class="active" @endif ><a href="{{route('AdminCatalogsTicketCategory')}}"><i class="fa fa-tags" aria-hidden="true"></i> Типы тикетов</a></li>
                        </ul>
                    </li>
                @endif

                @if($admin->can('module_tickets'))
                    <li  class="treeview active" >
                        <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Тикеты</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>

                        <ul class="treeview-menu">
                            <li @if(Request::is('admins/tickets/nobody*')) class="active" @endif ><a href="{{route('AdminTicketNobody')}}"><i class="fa fa-fire" aria-hidden="true"></i> Новые</a></li>
                            <li @if(Request::is('admins/tickets/my*')) class="active" @endif ><a href="{{route('AdminTicketMy')}}"><i class="fa fa-laptop" aria-hidden="true"></i> В работе</a></li>
                            <li @if(Request::is('admins/tickets/nobody*')) class="active" @endif ><a href="{{route('AdminTicketArchive')}}"><i class="fa fa-archive" aria-hidden="true"></i> Архивные</a></li>
                        </ul>
                    </li>
                @endif

                @if($admin->can('nbki_credit_rating'))
                    <li class="header">НБКИ</li>

                    @if($admin->can('nbki_credit_rating'))
                        <li  @if(Request::is('admins/nbki/credit-rating')) class="active" @endif><a href="{{route('AdminNbkiCreditRating')}}"><i class="fa fa-line-chart" aria-hidden="true"></i> Кредитный рейтинг</a></li>
                    @endif

                    @if($admin->can('nbki_credit_rating'))
                        <li  @if(Request::is('admins/nbki/contact')) class="active" @endif><a href="{{route('AdminNbkiContact')}}"><i class="fa fa-phone" aria-hidden="true"></i> Актуализация контактов</a></li>
                    @endif
                @endif


            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

@yield('content')



<!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            <img height="20" src="/assets/app/admin/images/dev-logo.png">
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{date('Y')}} <a href="#">{{config('project.name')}}</a>.</strong> V 1.1
    </footer>


</div>
<!-- ./wrapper -->




<script src="/assets/admin-lte-2.3.7/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/assets/admin-lte-2.3.7/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/iCheck/icheck.min.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/assets/admin-lte-2.3.7/plugins/datepicker/locales/ru.js"></script>
<script src="/assets/admin-lte-2.3.7/dist/js/app.js"></script>
<script src="/assets/jQuery-slimScroll-1.3.8/jquery.slimscroll.min.js"></script>
<script src="/assets/sweetalert/dist/sweetalert.min.js"></script>
<script src="/assets/app/admin/js/common.js?v=4"></script>
@stack('js')

</body>
</html>
