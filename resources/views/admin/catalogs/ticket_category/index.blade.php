@extends('admin.layouts.master')

@section('title', 'Категории тикетов')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Справочники
                <small>Категории тикетов</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Справочники</li>
                <li class="active">Категории тикетов</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Категории тикетов</h3>

                            <div class="toolbox pull-right">
                                <a href="#" class="btn btn-success btn-xs modal-show" data-modal-content-url="{{route('AdminCatalogsTicketCategoryCreate')}}" data-modal="#ticketCategoryModal" data-modal-title="Создать категории тикетов" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> Создать</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">

                            <table class="table table-bordered table-has-content" id="ticketCategoryListTable" data-content-url="{{route('AdminCatalogsTicketCategoryList')}}">
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th width="300">Название</th>
                                    <th>Подсказка заголовка</th>
                                    <th>Подсказка описание</th>
                                    <th class="text-center">Тип</th>
                                    <th class="text-center">Цена</th>
                                    <th class="text-center" width="100"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="ticketCategoryModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/catalogTicketCategory.js?v=2"></script>
@endpush