<form action="{{$formAction}}" method="post" data-app-module="catalogTicketCategory" data-app-method="handleTicketCategoryForm" id="adminTicketCategoryForm">

    <div class="form-group">
        <label for="ticket_categories">Названия категорий с новой строки *</label>
        <textarea class="form-control" name="ticket_categories" id="ticket_categories" rows="8"></textarea>
        <span class="help-block"></span>
    </div>
    <button type="submit" class="btn btn-primary">Создать</button>

</form>