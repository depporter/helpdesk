@foreach($categories as $category)
<tr>
    <td class="text-center">{{$category->id}}</td>
    <td>{{$category->name}}</td>
    <td>{{$category->title_hint}}</td>
    <td>{{$category->desc_hint}}</td>
    <td class="text-center">{{$category->getType()}}</td>
    <td class="text-center">{{$category->getPrice()}}</td>
    <td class="text-center">
        <a href="#" class="modal-show" data-modal-content-url="{{route('AdminCatalogsTicketCategoryEdit', ['categoryId' => $category->id])}}" data-modal="#ticketCategoryModal" data-modal-title="Редактирование категории"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
        &nbsp; &nbsp; &nbsp;
        <a href="{{route('AdminCatalogsTicketCategoryDelete', ['categoryId' => $category->id])}}" class="delete-item"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
    </td>
</tr>
@endforeach