<form action="{{$formAction}}" method="post" data-app-module="catalogTicketCategory" data-app-method="handleTicketCategoryForm" id="adminTicketCategoryForm">

    <div class="form-group">
        <label for="ticket_categories">Категория *</label>
        <input type="text" class="form-control" name="name" @if(isset($category)) value="{{$category->name}}" @endif>
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="title_hint">Подсказка для заголовка</label>
        <input type="text" class="form-control" name="title_hint" @if(isset($category)) value="{{$category->title_hint}}" @endif>
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="desc_hint">Подсказка для описания</label>
        <input type="text" class="form-control" name="desc_hint" @if(isset($category)) value="{{$category->desc_hint}}" @endif>
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="type">Тип</label>
        <select class="form-control type" name="type" id="type">
        @foreach(config('project.ticketCategoryTypes') as $key => $value)
        <option value="{{$key}}" @if(isset($category) && $category->type == $key) selected @endif>{{$value}}</option>
        @endforeach
        </select>
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="price">Цена за тикет</label>
        <input type="text" class="form-control price" name="price" @if(isset($category)) value="{{ceil($category->price)}}" @else  value="0" @endif id="price" onclick="this.select()" >
        <span class="help-block"></span>
    </div>



    <button type="submit" class="btn btn-primary">Сохранить</button>

</form>