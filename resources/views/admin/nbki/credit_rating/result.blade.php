<div class="panel panel-default">
    <div class="panel-heading">
        <span class="pull-right text-success"><small>Информация из официальных источников и является достоверной</small></span>
        <h3 class="panel-title">Кредитный рейтинг</h3>

    </div>
    <div class="panel-body no-padding">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Данные</th>
                <th width="200">Результат</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Количество активных счетов</td>
                <td>{{$response['response']['loanRating']['count']}}</td>
            </tr>

            <tr>
                <td>Количество активных счетов, в которых субъект выступает в качестве поручителя</td>
                <td>{{$response['response']['loanRating']['guarantee']}}</td>
            </tr>

            <tr>
                <td>Суммарный лимит активных кредитов</td>
                <td>{{$response['response']['loanRating']['limit']}}</td>
            </tr>

            <tr>
                <td>Суммарный баланс по активным кредитам</td>
                <td>{{$response['response']['loanRating']['balance']}}</td>
            </tr>

            <tr>
                <td>Максимальный срок закрытия активного кредита</td>
                <td>{{$response['response']['loanRating']['balance']}}</td>
            </tr>

            <tr>
                <td>Максимальная текущая просрочка по активным кредитам</td>
                <td>@if(!is_array($response['response']['loanRating']['maxCloseDt'])) {{$response['response']['loanRating']['maxCloseDt']}} @endif</td>
            </tr>

            <tr>
                <td>Суммарная просроченная задолженность по всем активным счетам</td>
                <td>{{$response['response']['loanRating']['totalOverdue']}}</td>
            </tr>

            <tr>
                <td>Максимальная историческая просрочка по активным кредитам</td>
                <td>@if(!is_array($response['response']['loanRating']['maxDeleay'])) {{$response['response']['loanRating']['maxDeleay']}} @endif</td>
            </tr>

            <tr>
                <td>Наличие негатива в закрытых кредитах</td>
                <td>@if($response['response']['loanRating']['closedNegative'] == 'N') нет @else да @endif</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="pull-right text-success"><small>Информация из официальных источников и является достоверной</small></span>
        <h3 class="panel-title">Статистика просрочек</h3>

    </div>
    <div class="panel-body no-padding">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Просрочка</th>
                <th width="200">По открытым счетам</th>
                <th width="200">По закрытым счетам</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>30 - 59 дней за последние 12 месяцев</td>
                <td>{{$response['response']['loanRating']['countDue30_60InOpenedAccs']}}</td>
                <td>{{$response['response']['loanRating']['countDue30_60InClosedAccs']}}</td>
            </tr>

            <tr>
                <td>60 - 89 дней за последние 12 месяцев</td>
                <td>{{$response['response']['loanRating']['countDue60_90InOpenedAccs']}}</td>
                <td>{{$response['response']['loanRating']['countDue60_90InClosedAccs']}}</td>
            </tr>

            <tr>
                <td>90+ дней за все время</td>
                <td>{{$response['response']['loanRating']['countDue90PlusInOpenedAccs']}}</td>
                <td>{{$response['response']['loanRating']['countDue90PlusInClosedAccs']}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="pull-right text-success"><small>Информация из официальных источников и является достоверной</small></span>
        <h3 class="panel-title">Верификация паспорта</h3>

    </div>
    <div class="panel-body no-padding">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Тип проверки</th>
                <th width="100" class="text-center">Результат</th>

            </tr>
            </thead>
            <tbody>

            <tr>
                <td>Паспорт из запроса найден</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['found'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['found'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['found']) Нет информации @endif
                </td>
            </tr>

            <tr>
                <td>Есть более свежий паспорт</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['hasNewer'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['hasNewer'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['hasNewer']) Нет информации @endif
                </td>
            </tr>

            <tr>
                <td>Признак утери паспорта</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['lost'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['lost'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['lost']) Нет информации @endif
                </td>
            </tr>

            <tr>
                <td>Признак недействительности паспорта</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['invalid'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['invalid'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['invalid']) Нет информации @endif
                </td>
            </tr>

            <tr>
                <td>Розыск</td>
                <td class="text-center">
                    @if($response['response']['docCheck']['wanted'] == 'Y') Да @endif
                    @if($response['response']['docCheck']['wanted'] == 'N') Нет @endif
                    @if(!$response['response']['docCheck']['wanted']) Нет информации @endif
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="pull-right text-success"><small>Информация из официальных источников и является достоверной</small></span>
        <h3 class="panel-title">Запросы в бюро</h3>

    </div>
    <div class="panel-body no-padding">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Запросы</th>
                <th width="100" class="text-center">Количество</th>

            </tr>
            </thead>
            <tbody>

            <tr>
                <td>За последние 7 дней</td>
                <td class="text-center">{{$response['response']['inquiries']['week']}}</td>
            </tr>

            <tr>
                <td>За последние 14 дней</td>
                <td class="text-center">{{$response['response']['inquiries']['twoWeeks']}}</td>
            </tr>

            <tr>
                <td>За последний месяц</td>
                <td class="text-center">{{$response['response']['inquiries']['month']}}</td>
            </tr>

            <tr>
                <td>Всего</td>
                <td class="text-center">{{$response['response']['inquiries']['overall']}}</td>
            </tr>


            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="pull-right text-success"><small>Информация из официальных источников и является достоверной</small></span>
        <h3 class="panel-title">Запросы по кредитному рейтингу</h3>

    </div>
    <div class="panel-body no-padding">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center" width="100">Дата</th>
                <th class="text-center">Тип</th>
                <th class="text-center" width="200">Сумма</th>
                <th class="text-center" width="200">Срок</th>
            </tr>
            </thead>
            <tbody>
                @foreach($response['response']['checks']['check'] as $check)
                    <tr>
                        <td class="text-center">{{date('d.m.Y', strtotime($check['date']))}}</td>
                        <td class="text-center">{{$check['loanType']}}</td>
                        <td class="text-center">{{$check['loanAmount']}}</td>
                        <td class="text-center">@if(!is_array($check['loanDuration'])) {{$check['loanDuration']}} @endif</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

