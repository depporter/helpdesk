@foreach($results as $result)
<tr>
    <td class="text-center">{{$result->id}}</td>
    <td class="text-center">{{$result->request_id}}</td>
    <td class="text-center">{{$result->customer->full_name}}</td>
    <td class="text-center">{{$result->created_at->format('d.m.Y')}}</td>
    <td><a href="{{route('AdminNbkiContactResult', ['id' => $result->id])}}" class="contactsResult">{{$result->first_name}} {{$result->last_name}} {{$result->middle_name}}</a></td>
    <td>{{$result->doc_series}} {{$result->doc_number}}</td>
    {{--<td class="text-center">{{round($result->cost)}} р.</td>--}}
    {{--<td class="text-center"><a href="{{route('AdminNbkiContactResult', ['id' => $result->id])}}"><i class="fa fa-download" aria-hidden="true"></i></a></td>--}}
</tr>
@endforeach

<tr>
    <td class="text-center" colspan="8">{{$results->links()}}</td>
</tr>