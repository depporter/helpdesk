@extends('admin.layouts.master')

@section('title', 'Актуализация контактов')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                НБКИ
                <small>Актуализация контактов</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">НБКИ</li>
                <li class="active">Актуализация контактов</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Актуализация контактов</h3>
                            <div class="pull-right">
                                <a href="#" class="btn btn-info btn-xs show-filter">Фильтр</a>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="filter pad" style="display: none; border-bottom: 1px solid #b6b6b6;">

                                <form action="{{route('AdminNbkiContactList')}}" method="get" id="contactsFilterForm">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="filter_customer_id">Пользователь</label>
                                                <select class="form-control" name="filter_customer_id" id="filter_customer_id">
                                                    <option value="all">Все</option>
                                                    @foreach($customers as $customer)
                                                        <option value="{{$customer->id}}">{{$customer->full_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="filter_first_name">Фамилия</label>
                                                <input type="text" class="form-control" id="filter_first_name" name="filter_first_name">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="form-group">
                                                <label for="filter_doc_series">Серия паспорта</label>
                                                <input type="text" class="form-control" id="filter_doc_series" name="filter_doc_series">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="filter_last_name">Имя</label>
                                                <input type="text" class="form-control" id="filter_last_name" name="filter_last_name">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="form-group">
                                                <label for="filter_doc_number">Номер паспорта</label>
                                                <input type="text" class="form-control" id="filter_doc_number" name="filter_doc_number">
                                                <span class="help-block"></span>
                                            </div>

                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-success btn-xs">Фильтр</button>
                                    <a href="{{route('AdminNbkiContact')}}" class="btn btn-info btn-xs">Сброс</a>
                                </form>

                            </div>
                            <table class="table table-bordered table-has-content" data-content-url="{{route('AdminNbkiContactList')}}" id="contactTable">
                                <thead>
                                <tr>
                                    <th class="text-center" width="80">#</th>
                                    <th class="text-center" width="100">Report ID</th>
                                    <th class="text-center" width="150">Пользователь</th>
                                    <th class="text-center" width="80">Дата</th>
                                    <th>Субъект</th>
                                    <th>Документ</th>
                                    {{--<th class="text-center" width="80">Стоимость</th>--}}
                                    {{--<th class="text-center" width="50"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></th>--}}
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" data-backdrop="static" id="contactsResultModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Результаты запроса</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/contacts.js"></script>
@endpush