@extends('admin.layouts.master')

@section('title', 'Управление архивами тикетов')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Управление архивами тикетов

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Управление архивами тикетов</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Управление архивами тикетов</h3>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <ul class="list-group">
                                @foreach($files as $file)
                                    @if(basename($file) != '.gitignore')
                                        <li class="list-group-item"><a href="{{route('AdminArchivesDownload', ['archiveName' => basename($file)])}}">{{basename($file)}}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>



@endsection

@push('js')
<script src="/assets/app/admin/js/modules/manageTickets.js?v=2"></script>
@endpush