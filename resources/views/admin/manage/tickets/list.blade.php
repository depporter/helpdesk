@foreach($results as $result)
    <tr>
        <td class="text-center">{{$result->id}}</td>
        <td><a href="#" class="modal-show" data-modal-content-url="{{route('AdminManageTicketsShow', ['ticketId' => $result->id])}}" data-modal="#ticketModal" data-modal-title="Просмотр тикета">{{$result->title}}</a></td>
        <td>@if($result->category){{$result->category->name}}@endif</td>
        <td>@if($result->customer) {{$result->customer->full_name}} @endif</td>
        <td>@if($result->support) {{$result->support->full_name}} @endif</td>
        <td class="text-center">{!! $result->getTicketStatus() !!}</td>
        <td class="text-center">{!! $result->getRuCreatedAt('d.m.y в H:i') !!}</td>
        <td class="text-center">{!! $result->getRuAssignedAt('d.m.y в H:i') !!}</td>
        <td class="text-center">{!! $result->getRuSupportArchivedAt('d.m.y в H:i') !!}</td>
    </tr>
@endforeach

@if(!$results->count())

<tr>
    <td colspan="8" class="text-center">Данных не найдено</td>
</tr>
@endif