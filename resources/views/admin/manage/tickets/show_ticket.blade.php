<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Переписка по тикету</div>


            <div class="box-body chat" id="chat-box">
                @foreach($ticket->messages as $message)

                    @if($message->author == 'support')
                        <div class="item">
                            <img src="/assets/app/admin/images/user-placeholder-160x160.gif" alt="user image" class="online">
                            <p class="message">
                                <a href="#" class="name">
                                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date("d.m.Y в H:i", strtotime($message->created_at))}}</small>
                                    Оператор: @if($ticket->support){{$ticket->support->full_name}}@else -- @endif
                                </a>
                                {!! nl2br($message->message) !!}
                            </p>

                        </div>
                    @endif

                    @if($message->author == 'customer')
                        <div class="item">
                            <img src="/assets/app/admin/images/user-placeholder-160x160.gif" alt="user image" class="offline">

                            <p class="message">
                                <a href="#" class="name">
                                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date("d.m.Y в H:i", strtotime($message->created_at))}}</small>
                                    Клиент: {{$ticket->customer->full_name}}
                                </a>
                                {!! nl2br($message->message) !!}
                            </p>
                        </div>
                    @endif

                @endforeach

            </div>
            <!-- /.chat -->
            <div class="box-footer">
                <form action="{{route('AdminTicketMessageStore', ['ticketId' => $ticket->id])}}" method="post" data-app-module="manageTickets" data-app-method="handleTicketMessageForm" id="ticketMessageForm">
                    <div class="input-group">
                        <input class="form-control" name="message" placeholder="Ваше сообщение...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-success">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Файлы саппорта </div>
            <ul class="list-group support-files" data-url="{{route('AdminTicketFiles', ['ticketId' => $ticket->id, 'type' => 'support'])}}">
                @foreach($ticket->supportFiles as $file)
                    <li class="list-group-item"><a href="{{asset('storage/tickets_media/' . $file->orig_file_name)}}" target="_blank">{{str_limit($file->client_file_name, 35)}}</a>
                        <span class="pull-right"><a href="{{route('AdminTicketFilesDelete', ['ticketId' => $ticket, 'fileId' => $file->id])}}" class="handle-click" data-app-module="manageTickets" data-app-method="handleTicketDeleteBtn"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
                    </li>
                @endforeach

                @if(!$ticket->supportFiles->count())
                    <li class="list-group-item text-center">Еще не загружались</li>
                @endif
            </ul>
            <div class="panel-heading">
                Файлы клиента

            </div>
            <ul class="list-group " >
                @foreach($ticket->customerFiles as $file)
                    <li class="list-group-item"><a href="{{asset('storage/tickets_media/' . $file->orig_file_name)}}" target="_blank">{{str_limit($file->client_file_name, 35)}}</a></li>
                @endforeach

                @if(!$ticket->customerFiles->count())
                    <li class="list-group-item text-center">Еще не загружались</li>
                @endif
            </ul>


        </div>
    </div>
</div>

