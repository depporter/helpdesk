@extends('admin.layouts.master')

@section('title', 'Управление тикетами')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Управление тикетами

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Управление тикетами</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Управление тикетами</h3>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Не назначенные тикеты</div>

                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th width="50" class="text-center">#</th>
                                                <th>Заголовок</th>
                                                <th>Категория</th>
                                                <th>Клиент</th>
                                                <th width="150" class="text-center">Создан</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($nobodyTickets as $ticket)
                                                <tr>
                                                    <td class="text-center">{{$ticket->id}}</td>
                                                    <td><a href="#" class="modal-show" data-modal-content-url="{{route('AdminManageTicketsShow', ['ticketId' => $ticket->id])}}" data-modal="#ticketModal" data-modal-title="Просмотр тикета">{{$ticket->title}}</a></td>
                                                    <td>@if($ticket->category){{$ticket->category->name}}@endif</td>
                                                    <td>@if($ticket->customer){{$ticket->customer->full_name}}@endif</td>
                                                    <td class="text-center">{{$ticket->getRuCreatedAt('d.m.y в H:i')}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="row">


                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Тикеты в работе</div>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th width="50" class="text-center">#</th>
                                                <th>Заголовок</th>
                                                <th>Категория</th>
                                                <th>Клиент</th>
                                                <th>Ответственный</th>
                                                <th width="150" class="text-center">Создан</th>
                                                <th width="150" class="text-center">Назначен</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($assignedTickets as $assignedTicket)
                                                <tr>
                                                    <td class="text-center">{{$assignedTicket->id}}</td>
                                                    <td><a href="#" class="modal-show" data-modal-content-url="{{route('AdminManageTicketsShow', ['ticketId' => $assignedTicket->id])}}" data-modal="#ticketModal" data-modal-title="Просмотр тикета">{{$assignedTicket->title}}</a></td>
                                                    <td>@if($assignedTicket->category){{$assignedTicket->category->name}}@endif</td>
                                                    <td>@if($assignedTicket->customer){{$assignedTicket->customer->full_name}}@endif</td>
                                                    <td>@if($assignedTicket->support){{$assignedTicket->support->full_name}}@endif</td>
                                                    <td class="text-center">{{$assignedTicket->getRuCreatedAt('d.m.y в H:i')}}</td>
                                                    <td class="text-center">{{$assignedTicket->getRuAssignedAt('d.m.y в H:i')}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Поиск тикетов</div>
                                        <div class="panel-body">
                                            <form action="{{route('AdminManageTicketsFilter')}}" method="get" data-app-module="manageTickets" data-app-method="handleFilterForm" id="manageTicketsFilterForm">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="filter_support_id">Ответственный</label>
                                                            <select name="filter_support_id" id="filter_support_id" class="form-control">
                                                                <option value="">Все</option>
                                                                @foreach($admins as $admin)
                                                                    <option value="{{$admin->id}}">{{$admin->full_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="help-block"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="filter_customer_id">Клиент</label>
                                                            <select name="filter_customer_id" id="filter_customer_id" class="form-control">
                                                                <option value="">Все</option>
                                                                @foreach($customers as $customer)
                                                                    <option value="{{$customer->id}}">{{$customer->full_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="help-block"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="filter_category_id">Категория</label>
                                                            <select name="filter_category_id" id="filter_category_id" class="form-control">
                                                                <option value="">Все</option>
                                                                @foreach($ticketCategories as $category)
                                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="help-block"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="filter_is_archived">Завершен</label>
                                                            <select name="filter_is_archived" id="filter_is_archived" class="form-control">
                                                                <option value="">Все</option>
                                                                <option value="1">Да</option>
                                                                <option value="0">Нет</option>

                                                            </select>
                                                            <span class="help-block"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-success btn-xs">Фильтр</button>
                                            </form>
                                        </div>

                                        <table class="table table-bordered" id="ticketList">
                                            <thead>
                                            <tr>
                                                <th width="50" class="text-center">#</th>
                                                <th>Заголовок</th>
                                                <th>Категория</th>
                                                <th>Клиент</th>
                                                <th>Ответственный</th>
                                                <th class="text-center" width="100">Статус</th>
                                                <th class="text-center" width="120">Создан</th>
                                                <th class="text-center" width="120">Назначен</th>
                                                <th class="text-center" width="120">Завершен</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="9" class="text-center">Укажите данные в фильтре и нажмите "Фильтр"</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>


    <div class="modal fade" id="ticketModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/manageTickets.js?v=2"></script>
@endpush