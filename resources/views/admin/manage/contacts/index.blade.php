@extends('admin.layouts.master')

@section('title', 'Контакты')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Контакты

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Контакты</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Контакты</h3>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                        <form action="{{route('AdminContactsUpdate')}}" method="post" data-app-module="contacts" data-app-method="handleContactsForm" id="contactsForm">

                            <div class="form-group">
                                <label for="contacts">Контакты</label>
                                <textarea name="contacts" class="form-control editor" id="contacts" rows="8">{{$contacts}}</textarea>
                                <span class="help-block"></span>
                            </div>

                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>



@endsection

@push('js')

<script src="/assets/app/admin/js/modules/contacts.js?v=2"></script>
<script src="/assets/app/admin/js/ckeditor/ckeditor.js"></script>
@endpush