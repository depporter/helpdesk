@foreach($groups as $group)
    <tr>
        <td class="text-center">{{$group->id}}</td>
        <td ><a href="{{route('AdminUsersCustomerGroupCustomers', ['groupId' => $group->id])}}">{{$group->name}}</a></td>
        <td>{{$group->customers_count}}</td>
        <td class="text-center">
            @if($group->name != 'Default')
            <a href="#" class="modal-show" data-modal-content-url="{{route('AdminUsersCustomerGroupEdit', ['groupId' => $group->id])}}" data-modal="#customerModal" data-modal-title="Редактировать группу"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
            &nbsp;&nbsp;
            @endif
        </td>
    </tr>
@endforeach

