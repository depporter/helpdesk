<form action="{{$formAction}}" method="post" data-app-module="usersCustomers" data-app-method="handleCustomerGroupForm" id="adminCustomerGroupForm">

    <div class="form-group">
        <label for="name">Название группы *</label>
        <input type="text" class="form-control" id="name" name="name" @if(isset($group)) value="{{$group->name}}" @endif >
        <span class="help-block"></span>
    </div>



    <button type="submit" class="btn btn-primary">@if(!isset($group)) Создать @else Сохранить @endif</button>
</form>


