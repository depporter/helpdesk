@extends('admin.layouts.master')

@section('title', 'Группы клиентов')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Клиенты
                <small>Список групп клиентов</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Группы клиентов</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Заявки на регистрацию</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">

                            <table class="table table-bordered table-has-content" id="regListTable" data-content-url="{{route('AdminUsersCustomerRegList')}}">
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th>Имя</th>
                                    <th>Почта</th>
                                    <th>Телефон</th>
                                    <th>Услуги</th>
                                    <th class="text-center" width="100"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>


        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Список групп клиентов</h3>

                            <div class="toolbox pull-right">

                                <a href="#" class="btn btn-success btn-xs modal-show" data-modal-content-url="{{route('AdminUsersCustomerCreate')}}" data-modal="#customerModal" data-modal-title="Создать клиента" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> Создать клиента</a>
                                <a href="#" class="btn btn-success btn-xs modal-show" data-modal-content-url="{{route('AdminUsersCustomerGroupCreate')}}" data-modal="#customerModal" data-modal-title="Создать группу" ><i class="fa fa-users" aria-hidden="true"></i> Создать группу</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">

                            <table class="table table-bordered table-has-content" id="customersListTable" data-content-url="{{route('AdminUsersCustomerList')}}">
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th>Группа</th>
                                    <th>Пользователей</th>
                                    <th class="text-center" width="100"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="customerModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/usersCustomers.js"></script>
@endpush