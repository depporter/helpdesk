@foreach($regs as $reg)
    <tr>
        <td class="text-center">{{$reg->id}}</td>
        <td>{{$reg->name}}</td>
        <td>{{$reg->email}}</td>
        <td>{{$reg->phone}}</td>
        <td>{{$reg->formatServices()}}</td>
        <td class="text-center" width="60">
            <a href="#" class="btn btn-xs btn-success modal-show" data-modal-content-url="{{route('AdminUsersCustomerCreate', ['regId' => $reg->id])}}" data-modal="#customerModal" data-modal-title="Создать клиента"><i class="fa fa-check" aria-hidden="true"></i></a>
            <a href="{{route('AdminUsersCustomerRegDelete', ['id' => $reg->id])}}" class="btn btn-xs btn-danger delete-item"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </td>
    </tr>
@endforeach

@if(!$regs->count())
    <tr>
        <td colspan="6" class="text-center">Нет данных</td>
    </tr>
@endif