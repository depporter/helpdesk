
<form action="{{route('AdminUsersCustomerTransactionsStore', ['customerId' => $customer->id])}}" method="post" data-app-module="usersCustomers" data-app-method="handleCustomerTransactionForm" id="adminCustomerTransactionForm">
    <div class="form-group">
        <label for="total">Сумма *</label>
        <input type="text" class="form-control" id="total" name="total" placeholder="Пополнить баланс">
        <span class="help-block"></span>
    </div>

    <button type="submit" class="btn btn-primary btn-sm">Накинуть к балансу</button>
</form>

<hr>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="text-center" width="50">#</th>
        <th width="200">Создал</th>
        <th class="text-center" width="100">Сумма</th>
        <th>Коммент</th>
        <th class="text-center">Тип</th>
        <th class="text-center" width="200">Дата</th>
    </tr>
    </thead>

    <tbody>
    @foreach($transactions as $transaction)
    <tr>
        <td class="text-center">{{$transaction->id}}</td>
        <td>{{str_limit($transaction->creator->full_name, 15)}}</td>
        <td class="text-center">{{$transaction->formatTotal()}}</td>
        <td>{{$transaction->comment}}</td>
        <td class="text-center">{{$transaction->getType()}}</td>
        <td>{{$transaction->getRuCreatedAt()}}</td>
    </tr>
    @endforeach
    </tbody>
</table>