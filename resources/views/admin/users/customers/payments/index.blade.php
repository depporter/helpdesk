@extends('admin.layouts.master')

@section('title', 'История пополнения баланса')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Клиенты
                <small>История пополнения баланса</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li><a href="{{route('AdminUsersCustomer')}}"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Клиенты</a></li>
                <li class="active">История пополнения баланса</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">История пополнения баланса</h3>

                            <div class="toolbox pull-right">
                                <a href="#" class="btn btn-success btn-xs modal-show" data-modal-content-url="{{route('AdminUsersCustomerPaymentsCreate', ['customerId' => $customer->id])}}" data-modal="#customerPaymentModal" data-modal-title="Пополнить баланс" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> Пополнить баланс</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">

                            <table class="table table-bordered table-has-content" id="customersListTable" data-content-url="{{route('AdminUsersCustomerPaymentsList', ['customerId' => $customer->id])}}">
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th>Кто</th>
                                    <th width="80" class="text-center">Сумма</th>
                                    <th width="80" class="text-center">Назначение</th>
                                    <th width="150" class="text-center">Когда</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">История трат</h3>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">

                            <table class="table table-bordered table-has-content" id="customersExpenseTable" data-content-url="{{route('AdminUsersCustomerPaymentsExpenses', ['customerId' => $customer->id])}}">
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th width="80" class="text-center">Сумма</th>
                                    <th width="80" class="text-center">Назначение</th>
                                    <th width="150" class="text-center">Когда</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="customerPaymentModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/usersCustomers.js"></script>
@endpush