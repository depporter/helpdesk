@foreach($payments as $payment)
    <tr>
        <td class="text-center">{{$payment->id}}</td>
        <td>{{$payment->admin->full_name}}</td>
        <td class="text-center">{{number_format($payment->amount, 0, '.', '.')}}</td>
        <td>{{$payment->desc}}</td>
        <td class="text-center">{{$payment->created_at->format("d.m.Y H:i")}}</td>
    </tr>
@endforeach
<tr>
    <td colspan="2" class="text-right"><strong>Итого</strong></td>
    <td class="text-center"><strong>{{number_format($payments->sum('amount'), 0, '.', '.')}}</strong></td>
</tr>