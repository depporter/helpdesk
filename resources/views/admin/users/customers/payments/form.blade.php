<form action="{{$formAction}}" method="post" data-app-module="usersCustomers" data-app-method="handleCustomerPaymentForm" id="adminCustomerPaymentForm">

    <div class="form-group">
        <label for="amount">Сумма платежа *</label>
        <input type="text" class="form-control" id="amount" name="amount">
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="desc">Комментарий к платежу</label>
        <textarea class="form-control" name="desc" id="desc"></textarea>
        <span class="help-block"></span>
    </div>

    <button type="submit" class="btn btn-primary">Добавить</button>
</form>


