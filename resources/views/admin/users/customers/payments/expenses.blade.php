@foreach($expenses as $payment)
    <tr>
        <td class="text-center">{{$payment->id}}</td>

        <td class="text-center">{{number_format($payment->amount, 0, '.', '.')}}</td>
        <td>{{$payment->comment}}</td>
        <td class="text-center">{{$payment->created_at->format("d.m.Y H:i")}}</td>
    </tr>
@endforeach
<tr>
    <td colspan="1" class="text-right"><strong>Итого</strong></td>
    <td class="text-center"><strong>{{number_format($expenses->sum('amount'), 0, '.', '.')}}</strong></td>
</tr>