@foreach($customers as $customer)
    <tr>
        <td class="text-center">{{$customer->id}}</td>
        <td >{{$customer->full_name}}
            <br>{{$customer->company}}
            <br>{{$customer->email}}
            <br>{{$customer->getRuCreatedAt()}}
        </td>

        <td>{!! $customer->getLimitsList() !!}</td>

        <td class="text-center"><a href="{{route('AdminUsersCustomerPayments', ['customerId' => $customer->id])}}">{{number_format($customer->balance, 0, '.', '.')}}</a></td>
        <td class="text-center">{{$customer->allow_work_on_credit}}</td>
        <td class="text-center">{!! $customer->IsActive() !!}</td>
        <td class="text-center">
            <a href="#" class="modal-show" data-modal-content-url="{{route('AdminUsersCustomerEdit', ['customerId' => $customer->id])}}" data-modal="#customerModal" data-modal-title="Редактировать клиента"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
            <a href="{{route('AdminUsersCustomerDelete', ['customerId' => $customer->id])}}" class="delete-item" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </td>
    </tr>
@endforeach

@if(!$customers->count())
    <tr>
        <td colspan="7" class="text-center">Нет данных для отображения</td>
    </tr>
@endif

