@extends('admin.layouts.master')

@section('title', 'Группы клиентов')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Клиенты
                <small>Список клиентов</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li><a href="{{route('AdminUsersCustomer')}}">{{$group->name}}</a></li>
                <li class="active">Список  клиентов</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Список клиентов</h3> <br><span class="badge bg-red">Месячный лимит</span> <span class="badge bg-green">Лимит по кол-ву</span> <span class="badge bg-blue">Оплата за тикет</span>

                            <div class="toolbox pull-right">

                                <a href="#" class="btn btn-info btn-xs show-filter" ><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</a>
                                <a href="#" class="btn btn-success btn-xs modal-show" data-modal-content-url="{{route('AdminUsersCustomerCreate')}}" data-modal="#customerModal" data-modal-title="Создать клиента" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> Создать клиента</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="filter pad" style="display: none; border-bottom: 1px solid #b6b6b6;">
                                <div class="row">
                                    <form action="{{route('AdminUsersCustomerGroupCustomersList', ['groupId' => $group->id])}}" method="get" data-app-module="usersCustomers" data-app-method="handleCustomerFilterForm" id="adminCustomerFilterForm" >
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="filter_full_name">ФИО</label>
                                                <input type="text" class="form-control" id="filter_full_name" name="filter_full_name">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="form-group">
                                                <label for="filter_email">E-mail</label>
                                                <input type="text" class="form-control" id="filter_email" name="filter_email">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="form-group">
                                                <label for="filter_company">Компания</label>
                                                <input type="text" class="form-control" id="filter_company" name="filter_company">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="form-group">
                                                <label for="filter_phone">Телефон</label>
                                                <input type="text" class="form-control" id="filter_phone" name="filter_phone">
                                                <span class="help-block"></span>
                                            </div>


                                            <button type="submit" class="btn btn-success btn-xs">Фильтр</button>
                                            <a href="{{route('AdminUsersCustomerGroupCustomers', ['groupId' => $group->id])}}" class="btn btn-info btn-xs">Сброс</a>
                                        </div>

                                    </form>
                                </div>
                            </div>

                            <table class="table table-bordered table-has-content" id="customersListTable" data-content-url="{{route('AdminUsersCustomerGroupCustomersList', ['groupId' => $group->id])}}">
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th width="200">Клиент</th>
                                    <th>Лимиты</th>

                                    <th class="text-center" width="70">Баланс</th>
                                    <th class="text-center" width="70">Кредит</th>
                                    <th class="text-center" width="50"><i class="fa fa-power-off" aria-hidden="true"></i></th>
                                    <th class="text-center" width="100"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="customerModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/usersCustomers.js"></script>
@endpush