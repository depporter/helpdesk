<form action="{{$formAction}}" method="post" data-app-module="usersCustomers" data-app-method="handleCustomerForm"
      id="adminCustomerForm">

    @if(isset($services))<p class="text-danger">Интересуется: {{$services}} </p> @endif

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="full_name">ФИО *</label>
                <input type="text" class="form-control" id="full_name" name="full_name"
                       @if(isset($user)) value="{{$user->full_name}}" @endif>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="email">E-mail *</label>
                <input type="text" class="form-control" id="email" name="email"
                       @if(isset($user)) value="{{$user->email}}" @endif>
                <span class="help-block"></span>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="company">Компания</label>
                <input type="text" class="form-control" id="company" name="company"
                       @if(isset($user)) value="{{$user->company}}" @endif>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="phone">Телефон</label>
                <input type="text" class="form-control" id="phone" name="phone"
                       @if(isset($user)) value="{{$user->phone}}" @endif>
                <span class="help-block"></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="password">Пароль</label>
                <span class="pull-right"><a href="#" class="show-password"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                <input autocomplete="new-password" type="password" class="form-control" id="password" name="password"
                       @if(isset($user) && $user->created_at) placeholder="Новый пароль, если требуется сменить" @endif>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="allow_work_on_credit">Может работать в кредит</label>
                <select name="allow_work_on_credit" class="form-control">
                    <option value="0" @if(isset($user) && $user->getOriginal('allow_work_on_credit') == 0) selected @endif>Нет</option>
                    <option value="1" @if(isset($user) && $user->getOriginal('allow_work_on_credit') == 1) selected @endif>Да</option>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="credit_up_to">Кредит до (без нуля)</label>
                <input type="text" class="form-control" id="credit_up_to" name="credit_up_to" @if(isset($user)) value="{{$user->credit_up_to}}" @endif>
                <span class="help-block"></span>
            </div>
        </div>
    </div>


    @if(isset($user) && $user->created_at)
        <div class="checkbox">
            <label>
                <input type="checkbox" name="active" @if($user->active) checked @endif > Активен
            </label>
        </div>
    @endif


    @if(!isset($user) && $user->created_at)

        <div class="checkbox">
            <label>
                <input type="checkbox" name="send_register_email" checked> Отправить письмо c доступами
            </label>
        </div>
    @endif

    <hr>
    <div class="row">
        <div class="col-md-3">
            <h4>Работа с НБКИ</h4>
            <label for="nbki_credit_rating_type">Кредитный рейтинг</label>
            <select name="nbki_credit_rating_type" id="nbki_credit_rating_type" class="form-control">
                <option value="off" @if(isset($user) && $user->nbki_credit_rating_type == 'off') selected="" @endif>
                    Выключен
                </option>
                <option value="package"
                        @if(isset($user) && $user->nbki_credit_rating_type == 'package') selected="" @endif>Пакет
                </option>
                <option value="query" @if(isset($user) && $user->nbki_credit_rating_type == 'query') selected="" @endif>
                    Запрос
                </option>
            </select>

            <div class="form-group">
                <label for="nbki_credit_rating_query_price">Цена за 1 запрос</label>
                <input type="text" class="form-control" id="nbki_credit_rating_query_price"
                       name="nbki_credit_rating_query_price"
                       @if(isset($user)) value="{{round($user->nbki_credit_rating_query_price)}}"
                       @endif onclick="this.select()">
                <span class="help-block"></span>
            </div>

            <div class="form-group">
                <label for="nbki_credit_rating_package_amount">Запросов в пакете</label>
                <input type="text" class="form-control" id="nbki_credit_rating_package_amount"
                       name="nbki_credit_rating_package_amount"
                       @if(isset($user)) value="{{round($user->nbki_credit_rating_package_amount)}}"
                       @endif onclick="this.select()">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="col-md-3">
            <h4>Актуализация контактов</h4>
            <label for="actualization_contacts_type">Актуализация контактов</label>
            <select name="actualization_contacts_type" id="actualization_contacts_type" class="form-control">
                <option value="off" @if(isset($user) && $user->actualization_contacts_type == 'off') selected="" @endif>
                    Выключен
                </option>
                <option value="package"
                        @if(isset($user) && $user->actualization_contacts_type == 'package') selected="" @endif>Пакет
                </option>
                <option value="query" @if(isset($user) && $user->actualization_contacts_type == 'query') selected="" @endif>
                    Запрос
                </option>
            </select>

            <div class="form-group">
                <label for="actualization_contacts_query_price">Цена за 1 запрос</label>
                <input type="text" class="form-control" id="actualization_contacts_query_price"
                       name="actualization_contacts_query_price"
                       @if(isset($user)) value="{{round($user->actualization_contacts_query_price)}}"
                       @endif onclick="this.select()">
                <span class="help-block"></span>
            </div>

            <div class="form-group">
                <label for="actualization_contacts_package_amount">Запросов в пакете</label>
                <input type="text" class="form-control" id="actualization_contacts_package_amount"
                       name="actualization_contacts_package_amount"
                       @if(isset($user)) value="{{round($user->actualization_contacts_package_amount)}}"
                       @endif onclick="this.select()">
                <span class="help-block"></span>
            </div>
        </div>
    </div>

    <hr>
    <h4>Группы</h4>

    @foreach($groups as $group)
        <div class="checkbox">
            <label>
                <input name="group[]" value="{{$group->id}}" type="checkbox"
                       @if(isset($customerGroupsIds) && is_array($customerGroupsIds) && in_array($group->id, $customerGroupsIds)) checked @endif> {{$group->name}}
            </label>
        </div>
    @endforeach

    <hr>
    <h4>Категории тикетов и лимиты к ним</h4>
    <small class="text-danger">Категория тикетов доступна клиенту если указан положительный лимит</small>
    <br>
    <small class="text-danger">Для категорий типа: "Оплата за тикет" значение лимита установите в 1</small>

    @foreach($ticketCategories as $category)
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="limit_{{$category->id}}">({{config('project.ticketCategoryTypes.' . $category->type)}}
                        ) @if($category->type == 'by_price')- {{$category->price}}р.@endif - {{$category->name}} - лимит
                        тикетов</label>
                    <input type="text" class="form-control" id="limit_{{$category->id}}" name="limit[{{$category->id}}]"
                           onclick="this.select()"
                           @if(isset($ticketCategoriesIds) && in_array($category->id, $ticketCategoriesIds)) value="{{$ticketCategoryLimit[$category->id]['limit']}}"
                           @else value="0" @endif>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>

    @endforeach
    <div>
        <br>
        <button type="submit" class="btn btn-primary">@if(isset($user) && $user->created_at) Сохранить @else
                Создать @endif</button>
    </div>

</form>


