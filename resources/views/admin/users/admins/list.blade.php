@foreach($users as $user)
<tr>
    <td class="text-center">{{$user->id}}</td>
    <td>{{$user->full_name}}</td>
    <td>{{$user->email}}</td>
    <td class="text-center">{!! $user->rolesDevideByComma() !!}</td>
    <td class="text-center">{{$user->tickets_total}}</td>
    <td class="text-center">{!! $user->isSuperUser() !!}</td>
    <td class="text-center">{!! $user->isActive() !!}</td>
    <td class="text-center">
        <a href="#" class="modal-show" data-modal-content-url="{{route('AdminUsersAdminEdit', ['adminId' => $user->id])}}" data-modal="#userModal" data-modal-title="Редактировать пользователя"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
        &nbsp;&nbsp;&nbsp;
        <a href="{{route('AdminUsersAdminDelete', ['adminId' => $user->id])}}" class="delete-item"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
    </td>
</tr>
@endforeach