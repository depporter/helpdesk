@extends('admin.layouts.master')

@section('title', 'Пользователи')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Пользователи
                <small>Список пользователей</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Пользователи</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Список пользователей</h3>

                            <div class="toolbox pull-right">
                                <a href="#" class="btn btn-info btn-xs show-filter" ><i class="fa fa-filter" aria-hidden="true"></i> Фильтр</a>
                                <a href="#" class="btn btn-success btn-xs modal-show" data-modal-content-url="{{route('AdminUsersAdminCreate')}}" data-modal="#userModal" data-modal-title="Создать пользователя" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> Создать</a>
                                <a href="{{route('AdminUsersAdminRoles')}}" class="btn btn-success btn-xs"><i class="fa fa-object-group" aria-hidden="true"></i> Роли</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="filter pad" style="display: none; border-bottom: 1px solid #b6b6b6;">

                                    <form action="{{route('AdminUsersAdminList')}}" method="get" data-app-module="usersAdmins" data-app-method="adminUserFilterForm" id="adminUserFilterForm" >
                                        <div class="row">
                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Поиск пользователя</legend>
                                                <div class="form-group">
                                                    <label for="filter_full_name">ФИО</label>
                                                    <input type="text" class="form-control" id="filter_full_name" name="filter_full_name">
                                                    <span class="help-block"></span>
                                                </div>

                                                <div class="form-group">
                                                    <label for="filter_email">E-mail</label>
                                                    <input type="text" class="form-control" id="filter_email" name="filter_email">
                                                    <span class="help-block"></span>
                                                </div>
                                            </fieldset>




                                        </div>

                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Перод для тикетов</legend>

                                                <div class="form-group">
                                                    <label for="filter_date_start">Дата начала</label>
                                                    <input type="text" class="form-control dp" id="filter_date_start" name="filter_date_start">
                                                    <span class="help-block"></span>
                                                </div>

                                                <div class="form-group">
                                                    <label for="filter_date_end">Дата окончания</label>
                                                    <input type="text" class="form-control dp" id="filter_date_end" name="filter_date_end">
                                                    <span class="help-block"></span>
                                                </div>
                                            </fieldset>

                                        </div>

                                        </div>



                                        <button type="submit" class="btn btn-success btn-xs">Фильтр</button>
                                        <a href="{{route('AdminUsersAdmin')}}" class="btn btn-info btn-xs">Сброс</a>

                                    </form>

                            </div>
                            <table class="table table-bordered table-has-content" id="usersListTable" data-content-url="{{route('AdminUsersAdminList')}}">
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th width="200">ФИО</th>
                                    <th width="200">E-mail</th>
                                    <th class="text-center">Группы</th>
                                    <th class="text-center" width="50">Тикетов</th>
                                    <th class="text-center" width="50">Супер</th>
                                    <th class="text-center" width="50"><i class="fa fa-power-off" aria-hidden="true"></i></th>
                                    <th class="text-center" width="100"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="userModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/usersAdmins.js"></script>
@endpush