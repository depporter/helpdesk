@foreach($roles as $role)
    <tr>
        <td class="text-center">{{$role->id}}</td>
        <td>{{$role->name}}</td>
        <td>{!! $role->permsToList() !!}</td>
        <td class="text-center"><a class="modal-show" data-modal-content-url="{{route('AdminUsersAdminRolesEdit', ['roleId' => $role->id])}}" data-modal="#roleModal" data-modal-title="Редактировать роль" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
    </tr>
@endforeach

@if(!$roles->count())
    <tr>
        <td colspan="5" class="text-center">Записей не найдено</td>
    </tr>
@endif