<form action="{{$formAction}}" method="post" data-app-module="usersAdmins" data-app-method="handleRoleForm" id="adminRoleForm" >
    <div class="form-group">
        <label for="name">Название роли</label>
        <input type="text" class="form-control" id="name" name="name" @if(isset($role)) value="{{$role->name}}" @endif>
        <span class="help-block"></span>
    </div>

    @foreach($permsGroups as $group)
        <p><strong>{{$group->name}}</strong></p>
        @foreach($group->permissions as $permission)
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="perms[]" value="{{$permission->id}}" @if(isset($permsIds) && in_array($permission->id, $permsIds)) checked @endif> {{$permission->name}}
                </label>
                <br> <small>{{$permission->desc}}</small>
            </div>

        @endforeach

    @endforeach

    <button type="submit" class="btn btn-primary">@if(!isset($role)) Создать @else Сохранить @endif</button>
</form>

