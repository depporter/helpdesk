<form action="{{$formAction}}" method="post" data-app-module="usersAdmins" data-app-method="handleAdminForm" id="usersAdminForm">

    <div class="form-group">
        <label for="full_name">ФИО *</label>
        <input type="text" class="form-control" id="full_name" name="full_name" @if(isset($user)) value="{{$user->full_name}}" @endif>
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="email">E-mail *</label>
        <input type="text" class="form-control" id="email" name="email" @if(isset($user)) value="{{$user->email}}" @endif>
        <span class="help-block"></span>
    </div>

    <div class="form-group">
        <label for="password">Пароль @if(!isset($user))*@endif</label>
        <span class="pull-right"><a href="#" class="show-password"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
        <input autocomplete="new-password"  type="password" class="form-control" id="password" name="password" @if(isset($user)) placeholder="Новый пароль, если требуется сменить" @endif>
        <span class="help-block"></span>
    </div>



    @if(isset($user) && !$user->super_user)
        <div class="checkbox">
            <label>
                <input type="checkbox" name="active" @if($user->active) checked @endif > Активен
            </label>
        </div>
    @endif

    <hr>
    <h4>Роли</h4>


    @foreach($roles as $role)
        <div class="checkbox">
            <label>
                <input type="checkbox" name="roles[]" value="{{$role->id}}" @if(isset($adminRolesIds) && in_array($role->id, $adminRolesIds)) checked @endif> {{$role->name}}
            </label>
        </div>
    @endforeach

    <hr>
    <h4>Категории тикетов</h4>
    <small>Это применимо, если пользователю разрешено работать с модулем тикетов</small>
    @foreach($ticketCategories as $ticketCategory)
        <div class="checkbox">
            <label>
                <input type="checkbox" name="ticket_categories[]" value="{{$ticketCategory->id}}" @if(isset($adminTicketCategoriesIds) && in_array($ticketCategory->id, $adminTicketCategoriesIds)) checked @endif > {{$ticketCategory->name}} <br>
                <small>{{$ticketCategory->getType()}} @if($ticketCategory->price > 0) {{$ticketCategory->price}} @endif</small>
            </label>
        </div>
    @endforeach

<br>
    <button type="submit" class="btn btn-primary">@if(!isset($user)) Создать @else Сохранить @endif</button>
</form>


