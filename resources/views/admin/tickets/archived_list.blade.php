@foreach($tickets as $ticket)
    <tr>
        <td class="text-center"><a href="{{route('AdminTicketShow', ['ticketId' => $ticket->id])}}">{{$ticket->id}}</a></td>
        <td><a href="{{route('AdminTicketShow', ['ticketId' => $ticket->id])}}">{{$ticket->title}}</a></td>
        <td class="text-center">{{str_limit($ticket->category->name, 30)}}</td>
        <td class="text-center">{{$ticket->getRuCreatedAt()}}</td>

    </tr>
@endforeach

@if(!$tickets->count())
    <tr><td colspan="4" class="text-center">Данных не найдено</td></tr>
@endif

<tr>
    <td class="text-center" colspan="4">{{$tickets->links()}}</td>
</tr>