@foreach($tickets as $ticket)
<tr>
    <td class="text-center">{{$ticket->id}}</td>
    <td>{{$ticket->title}}</td>
    <td>{{$ticket->category->name}}</td>
    <td>{{$ticket->getRuCreatedAt()}}</td>
    <td><a target="_blank" href="{{route('AdminTicketTake', ['ticketId' => $ticket->id])}}" class="btn btn-info btn-xs">Взять в обработку</a></td>
</tr>
@endforeach