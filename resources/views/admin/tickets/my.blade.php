@extends('admin.layouts.master')

@section('title', 'Мои тикеты')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Тикеты
                <small>Мои тикеты</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Тикеты</li>
                <li class="active">Мои тикеты</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Мои тикеты</h3>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">

                            <table class="table table-bordered table-has-content" data-content-url="{{route('AdminTicketList', ['type' => 'my'])}}" >
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th width="300">Заголовок</th>
                                    <th>Категория</th>
                                    <th width="100">Есть ответ</th>
                                    <th class="text-center" width="150">Дата</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


@endsection

@push('js')
<script src="/assets/app/admin/js/modules/tickets.js"></script>
@endpush