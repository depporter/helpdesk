@extends('admin.layouts.master')

@section('title', 'Просмотр тикета #' . $ticket->id)


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Тикеты
                <small>Тикет #{{$ticket->id}}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li><a href="{{route('AdminTicketNobody')}}">Тикеты</a></li>
                <li class="active">Тикет #{{$ticket->id}}</li>
            </ol>

        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                            <h3 class="box-title">Тикет #{{$ticket->id}} {{$ticket->title}}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Переписка по тикету <span class="pull-right"><a href="{{route('AdminTicketAction', ['ticketId' => $ticket->id, 'action' => 'archive'])}}" data-redirect-after="{{route('AdminTicketNobody')}}" class="btn btn-xs btn-success handle-click" data-app-module="tickets" data-app-method="handleArchiveBtn">Завершить тикет</a></span></div>


                                        <div class="box-body chat" id="chat-box">
                                            @foreach($ticket->messages as $message)

                                                @if($message->author == 'support')
                                                    <div class="item">
                                                        <img src="/assets/app/admin/images/user-placeholder-160x160.gif" alt="user image" class="online">
                                                        <p class="message">
                                                            <a href="#" class="name">
                                                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date("d.m.Y в H:i", strtotime($message->created_at))}}</small>
                                                                @if($ticket->support){{$ticket->support->full_name}}@else -- @endif
                                                            </a>
                                                            {!! nl2br($message->message) !!}
                                                        </p>

                                                    </div>
                                                @endif

                                                @if($message->author == 'customer')
                                                    <div class="item">
                                                        <img src="/assets/app/admin/images/user-placeholder-160x160.gif" alt="user image" class="offline">

                                                        <p class="message">
                                                            <a href="#" class="name">
                                                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{date("d.m.Y в H:i", strtotime($message->created_at))}}</small>
                                                                @if($ticket->customer){{$ticket->customer->full_name}}@else -- @endif
                                                            </a>
                                                            {!! nl2br($message->message) !!}
                                                        </p>
                                                    </div>
                                                @endif

                                            @endforeach

                                        </div>
                                        <!-- /.chat -->
                                        <div class="box-footer">
                                            <form action="{{route('AdminTicketMessageStore', ['ticketId' => $ticket->id])}}" method="post" data-app-module="tickets" data-app-method="handleTicketMessageForm" id="ticketMessageForm">
                                                <div class="input-group">
                                                    <input class="form-control" name="message" placeholder="Ваше сообщение...">
                                                    <div class="input-group-btn">
                                                        <button type="submit" class="btn btn-success">Отправить</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Файлы загруженные Вами <span class="pull-right">
                                                <a type="button" id="fileUploadBtn" class="btn btn-success btn-xs handle-click" title="К загрузке разрешаются .jpeg, .jpg, .bmp, .png, .zip, .rar, .pdf" data-app-module="tickets" data-app-method="handleUploadBtn">Загрузить</a>
                                            <i class="fa fa-spinner fa-spin" id="spinner" aria-hidden="true" style="display: none"></i>
                                            </span></div>
                                        <ul class="list-group support-files" data-url="{{route('AdminTicketFiles', ['ticketId' => $ticket->id, 'type' => 'support'])}}">
                                            @foreach($ticket->supportFiles as $file)
                                                <li class="list-group-item">
                                                    <a href="{{asset('storage/tickets_media/' . $file->orig_file_name)}}" target="_blank">{{str_limit($file->client_file_name, 35)}}</a>
                                                    <span class="pull-right"><a href="{{route('AdminTicketFilesDelete', ['ticketId' => $ticket, 'fileId' => $file->id])}}" class="handle-click" data-app-module="tickets" data-app-method="handleTicketDeleteBtn"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
                                                </li>
                                            @endforeach

                                            @if(!$ticket->supportFiles->count())
                                                <li class="list-group-item text-center">Еще не загружались</li>
                                            @endif
                                        </ul>
                                        <div class="panel-heading">
                                            Файлы загруженные клиентом

                                        </div>
                                        <ul class="list-group " >
                                            @foreach($ticket->customerFiles as $file)
                                                <li class="list-group-item"><a href="{{asset('storage/tickets_media/' . $file->orig_file_name)}}" target="_blank">{{str_limit($file->client_file_name, 35)}}</a></li>
                                            @endforeach

                                            @if(!$ticket->customerFiles->count())
                                                <li class="list-group-item text-center">Еще не загружались</li>
                                            @endif
                                        </ul>


                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


            </div>

        </section>
    </div>
    <form action="{{route('AdminTicketUpload', ['ticketId' => $ticket->id])}}" method="post" id="uploadForm" data-app-module="tickets" data-app-method="handleUploadForm">
        <input id="fileSelector" name="files[]" type="file" multiple style="visibility:hidden" />
    </form>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/tickets.js"></script>
@endpush