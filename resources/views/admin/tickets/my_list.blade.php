@foreach($tickets as $ticket)
<tr>
    <td class="text-center">{{$ticket->id}}</td>
    <td><a href="{{route('AdminTicketShow', ['ticketId' => $ticket->id])}}">{{$ticket->title}}</a></td>
    <td>{{$ticket->category->name}}</td>
    <td class="text-center">{!! $ticket->hasAnswers('operator') !!}</td>
    <td>{{$ticket->getRuCreatedAt()}}</td>

</tr>
@endforeach