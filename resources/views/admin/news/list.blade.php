@foreach($news as $item)
<tr>
    <td class="text-center">{{$item->id}}</td>
    <td>{{$item->title}}</td>
    <td class="text-center">{{$item->getRuCreatedAt()}}</td>
    <td class="text-center">{!! $item->isPublish() !!}</td>
    <td class="text-center"><a href="#" class="modal-show" data-modal-content-url="{{route('AdminNewsEdit', ['newsId' => $item->id])}}" data-modal="#newsModal" data-modal-title="Редактировать новость"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
</tr>
@endforeach