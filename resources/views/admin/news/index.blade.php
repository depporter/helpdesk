@extends('admin.layouts.master')

@section('title', 'Новости')


@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Новости
                <small>Список новостей</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('AdminHome')}}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
                <li class="active">Новости</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <h3 class="box-title">Список новостей</h3>

                            <div class="toolbox pull-right">

                                <a href="#" class="btn btn-success btn-xs modal-show" data-modal-content-url="{{route('AdminNewsCreate')}}" data-modal="#newsModal" data-modal-title="Создать новость" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> Создать</a>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">

                            <table class="table table-bordered table-has-content" id="newsListTable" data-content-url="{{route('AdminNewsList')}}">
                                <thead>
                                <tr>
                                    <th class="text-center" width="50">#</th>
                                    <th width="200">Новость</th>
                                    <th width="80" class="text-center">Дата</th>
                                    <th class="text-center" width="50"><i class="fa fa-power-off" aria-hidden="true"></i></th>
                                    <th class="text-center" width="50"><i class="fa fa-bars" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="newsModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
<script src="/assets/app/admin/js/modules/news.js"></script>
<script src="/assets/app/admin/js/ckeditor/ckeditor.js"></script>
@endpush