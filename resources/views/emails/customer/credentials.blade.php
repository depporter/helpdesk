<div style="border: 1px solid black; padding: 10px;">
    <p>Здравствуйте {{$customer->full_name}}!</p>
    <p>Добро пожаловать в HelpDesk.</p>
    <p>
        Для авторизации используйте следующие данные:<br>
        Адрес сайта: <a href="{{route('CustomerLogin')}}">{{route('CustomerLogin')}}</a><br>
        Логин: {{$customer->email}}<br>
        Пароль: {{$password}}<br>
    </p>
</div>
