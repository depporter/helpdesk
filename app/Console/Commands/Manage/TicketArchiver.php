<?php

namespace App\Console\Commands\Manage;

use Illuminate\Console\Command;
use Zipper;

// repos
use App\Repos\TicketRepo;
use App\Repos\TicketAttachmentRepo;

class TicketArchiver extends Command
{

    protected $ticketRepo;
    protected $ticketAttachmentRepo;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manage:archive_tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Архивирует аттачменты тикетов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TicketRepo $ticketRepo, TicketAttachmentRepo $ticketAttachmentRepo)
    {
        parent::__construct();

        $this->ticketRepo = $ticketRepo;
        $this->ticketAttachmentRepo = $ticketAttachmentRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tickets = $this->ticketRepo->getForArchiving();

        //dd($tickets->count());

        foreach ($tickets as $ticket)
        {


            $filesArray = $ticket->supportFiles->pluck('orig_file_name')->toArray();

            if (count($filesArray))
            {
                foreach ($filesArray as $k => $value)
                {
                    if (file_exists(storage_path() . '/app/public/tickets_media/' . $value))
                    {
                        $filesArray[$k] = storage_path() . '/app/public/tickets_media/' . $value;
                    } else {

                        unset($filesArray[$k]);
                    }


                }

                $zipFile = storage_path() . '/app/public/tickets_archives/' . $ticket->id . '_' . uniqid() . '.zip';

                //dd($filesArray);

                Zipper::make($zipFile)->add($filesArray)->close();

                foreach ($filesArray as $file)
                {
                    if (file_exists($file))
                    {
                        unlink($file);
                    }
                }

                $ticket->system_archived = 1;
                $ticket->save();

                // удаляем приатаченные файлы
                $this->ticketAttachmentRepo->destroyByWhere(['ticket_id' => $ticket->id]);
            }


        }




    }


}
