<?php

namespace App\Console\Commands\Manage;

use Illuminate\Console\Command;

use App\Models\Admin;

class Admins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manage:add-super-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create super user';

    private $admin;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Admin $admin)
    {
        parent::__construct();

        $this->admin = $admin;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->ask('Email пользователя');

        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $this->error('Указанный вами e-mail имеет не верный формат');
            exit;
        }

        if ($this->admin->where('email', $email)->count())
        {
            $this->error('Пользователь с таким e-mail уже существует');
            exit;
        }


        $name = $this->ask('ФИО пользователя');

        $password = $this->secret('Пароль пользователя');

        $this->admin->create([
            'email' => $email,
            'password' => $password,
            'full_name' => $name,
            'super_user' => 1
        ]);


        $this->info('Пользователь успешно создан');
    }
}
