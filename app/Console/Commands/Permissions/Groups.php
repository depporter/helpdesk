<?php

namespace App\Console\Commands\Permissions;

use Illuminate\Console\Command;
use App\Models\PermissionGroup;


class Groups extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpdesk:permissions-group-add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создание группы для набора прав';

    private $model;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PermissionGroup $permissionGroup)
    {
        parent::__construct();
        $this->model = $permissionGroup;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groups = $this->model->orderBy('name')->get();

        if ($groups->count())
        {
            $this->info('Список уже существующих групп:');

            foreach ($groups as $group)
            {
                $this->info('- ' . $group->name);
            }
        }


        $name = $this->ask('Имя группы кириллицей');

        $this->model->create([
            'name' => $name
        ]);


        $this->info('Группа создана');

        $this->call('helpdesk:permissions-dump');

    }
}
