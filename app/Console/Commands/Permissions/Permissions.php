<?php

namespace App\Console\Commands\Permissions;

use Illuminate\Console\Command;

use App\Models\PermissionGroup;
use App\Models\Permission;

class Permissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpdesk:permissions-add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создание пермишена для группы';

    private $permissionGroup;
    private $permission;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Permission $permission, PermissionGroup $permissionGroup)
    {
        parent::__construct();
        $this->permission = $permission;
        $this->permissionGroup = $permissionGroup;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groups = $this->permissionGroup->orderBy('name')->get();

        if ($groups->count())
        {
            $this->info('Для какой группы создается пермишен:');

            foreach ($groups as $group)
            {
                $this->comment('- ID: ' . $group->id . ' ' . $group->name  );
            }
        }

        $groupId = (int) $this->ask('Укажите ID группы');



        if (!$this->checkGroupExists($groupId))
        {
            $this->error('Не найдено группы с ID ' . $groupId);
            return;
        }

        $alias = $this->ask('Алиас пермишна латиницей без проблелов');

        if (!$this->checkPermissionExists($alias))
        {
            $this->error('Пермишен с таким алиасом уже существует');
            return;
        }

        $name = $this->ask('Название пермишена кириллицей');
        $desc = $this->ask('Описание пермишена кириллицей');


        $this->permission->create([
            'group_id' => $groupId,
            'name' => $name,
            'alias' => $alias,
            'desc' => $desc
        ]);

        $this->info('Пермишен успешно создан');

        $this->call('helpdesk:permissions-dump');
    }

    private function checkPermissionExists($alias)
    {
        return !(boolean) $this->permission->where('alias', $alias)->count();
    }

    private function checkGroupExists($groupId)
    {
        return (boolean) $this->permissionGroup->where('id', $groupId)->count();
    }
}

/*
 * Справочники
 * goods_bonus_grid
 * Бонусная сетка
 * Управление бонусной сеткой
 */