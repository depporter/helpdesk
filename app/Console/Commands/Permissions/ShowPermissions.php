<?php

namespace App\Console\Commands\Permissions;

use Illuminate\Console\Command;

use App\Models\PermissionGroup;
use Illuminate\Database\Eloquent\Collection;

class ShowPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpdesk:permissions-show';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отобржение всех ранее созданных пермишенов';

    private $permissionGroups;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PermissionGroup $permissionGroup)
    {
        parent::__construct();
        $this->permissionGroups = $permissionGroup;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $groups = $this->getGroupsWithPermissions();
        $this->info('Пермишены');
        foreach ($groups as $group)
        {
            $this->comment('- ' . $group->name);

            foreach ($group->permissions as $permission)
            {
                $this->info('-- ' . $permission->name . ' (' .  $permission->alias . ')');
            }
        }
    }

    private function getGroupsWithPermissions()
    {
        $res = $this->permissionGroups->with('permissions');


        return $res->get();
    }
}
