<?php

namespace App\Console\Commands\Permissions;

use Illuminate\Console\Command;

use App\Models\PermissionGroup;
use App\Models\Permission;
use Storage;

class Importer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpdesk:permissions-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импорт настроект пермишенов';

    private $permissionGroup;
    private $permission;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Permission $permission, PermissionGroup $permissionGroup)
    {
        parent::__construct();
        $this->permissionGroup = $permissionGroup;
        $this->permission = $permission;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Импорт дампа настроек...');
        if (Storage::exists('permissions_config'))
        {
            $groups = json_decode(Storage::get('permissions_config'));

            foreach ($groups as $group)
            {
                if (!$this->permissionGroup->where('id', $group->id)->count())
                {
                    $this->permissionGroup->create([
                        'name' => $group->name
                    ]);
                }

                foreach ($group->permissions as $permission)
                {
                    if (!$this->permission->where('alias', $permission->alias)->count())
                    {
                        $this->permission->create([
                            'group_id' => $group->id,
                            'name' => $permission->name,
                            'alias' => $permission->alias,
                            'desc' => $permission->desc
                        ]);
                    }
                }
            }

        }
    }
}
