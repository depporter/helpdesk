<?php

namespace App\Console\Commands\Permissions;

use Illuminate\Console\Command;

use App\Models\PermissionGroup;

use Storage;

class Dumper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpdesk:permissions-dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Дамп пермишенов';

    private $model;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PermissionGroup $permissionGroup)
    {
        parent::__construct();
        $this->model = $permissionGroup;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Содзание дампа настроек пермишенов...');
        $groups = $this->model->with('permissions')->get();

        Storage::put('permissions_config', json_encode($groups));
    }
}
