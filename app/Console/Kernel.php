<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Manage\Admins::class,
        Commands\Manage\TicketArchiver::class,
        Commands\Permissions\Groups::class,
        Commands\Permissions\Permissions::class,
        Commands\Permissions\ShowPermissions::class,
        Commands\Permissions\Dumper::class,
        Commands\Permissions\Importer::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('manage:archive_tickets')->dailyAt('08:00')->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

