<?php

namespace App\Services\Nbki;

use App\Models\NbkiContact;
use Ixudra\Curl\Facades\Curl;

use App\Models\NbkiCreditRating;

class NbkiService {

    private $url = 'http://api.exbico.ru/se/crm_V4_1';
    private $contactUrl = 'http://capi.exbico.ru:8004/se/nbch_contact_v5';

    private $login;
    private $password;


    private $isTest = 0;

    public function __construct()
    {
        $this->login = config('services.nbki.login');
        $this->password = config('services.nbki.password');
        $this->isTest = (app('env') == 'local') ? 1 : 0;
    }

    /**
     * @param int $isTest
     * @return $this
     */
    public function setIsTest(int $isTest)
    {
        $this->isTest = $isTest;
        return $this;
    }

    public function creditRating(NbkiCreditRating $nbkiCreditRating)
    {
        $response = $this->makeRequest($nbkiCreditRating);

        //$obj = simplexml_load_string($response);

        //print_r($obj);

        return json_decode(json_encode((array) simplexml_load_string($response)),1);
    }

    private function makeRequest(NbkiCreditRating $nbkiCreditRating)
    {
        $xml = $this->buildXml($nbkiCreditRating);


        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $this->url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
        $result = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_exec($ch) === false || $responseCode != 200)
        {
            $errorNumber = curl_errno ( $ch );

            throw new \Exception('Curl exec fail. Error code: ' . $errorNumber . ' Http response code: ' . $responseCode);
        }

        curl_close($ch);

        return $result;
    }

    private function parseXml($data)
    {
        $obj = simplexml_load_string($data);

        return $obj;
    }

    private function buildXml(NbkiCreditRating $nbkiCreditRating)
    {
        $requestData = [
            'login' => $this->login,
            'password' => $this->password,
            'firstName' => $nbkiCreditRating->first_name,
            'lastName' => $nbkiCreditRating->last_name,
            'series' => $nbkiCreditRating->doc_series,
            'number' => $nbkiCreditRating->doc_number,
            'isTest' => $this->isTest
        ];



        return view('services.nbki.credit_rating',['requestData' => $requestData])->render();
    }


    public function contact(NbkiContact $nbkiContact)
    {
        $response = $this->makeRequestContact($nbkiContact);

        //$obj = simplexml_load_string($response);

        //print_r($obj);

        return json_decode(json_encode((array) simplexml_load_string($response)),1);
    }

    private function makeRequestContact(NbkiContact $nbkiContact)
    {
        $xml = $this->buildXmlContact($nbkiContact);


//        dd($xml);

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $this->contactUrl );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
        $result = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_exec($ch) === false || $responseCode != 200)
        {

            //dd($result);
            $errorNumber = curl_errno ( $ch );

            throw new \Exception('Curl exec fail. Error code: ' . $errorNumber . ' Http response code: ' . $responseCode);
        }

        curl_close($ch);

        return $result;

    }

    private function buildXmlContact(NbkiContact $nbkiContact)
    {
        $requestData = [
            'login' => $this->login,
            'password' => $this->password,
            'firstName' => $nbkiContact->first_name,
            'lastName' => $nbkiContact->last_name,
            'middleName' => $nbkiContact->middle_name,
            'dateBirth' => $nbkiContact->date_birth,
            'series' => $nbkiContact->doc_series,
            'number' => $nbkiContact->doc_number,
            'issueDate' => $nbkiContact->doc_issue_date,
            'phones' => json_decode($nbkiContact->excluded_phones, true),
            'months' => $nbkiContact->months,
            'limit' => $nbkiContact->limit,
            'isTest' => $this->isTest
        ];

        return view('services.nbki.contact',['requestData' => $requestData])->render();
    }


}