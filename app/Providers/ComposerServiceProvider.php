<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {

        // Using class based composers...
        View::composer(
            'customer.*', \App\Http\ViewComposers\Customer\CommonComposer::class
        );

        View::composer(
            'admin.*', \App\Http\ViewComposers\Admin\CommonComposer::class
        );





    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}