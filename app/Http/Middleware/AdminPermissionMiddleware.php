<?php

namespace App\Http\Middleware;

use App\Repos\AdminRepo;
use Closure;
use Auth;

class AdminPermissionMiddleware
{

    protected $admRepo;

    public function __construct(AdminRepo $adminRepo)
    {
        $this->admRepo = $adminRepo;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {

        if (!$this->admRepo->can($permission))
        {
            abort(403, 'Access denied');
        }

        return $next($request);

    }
}
