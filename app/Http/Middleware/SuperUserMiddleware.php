<?php

namespace App\Http\Middleware;

use App\Repos\AdminRepo;
use Closure;
use Auth;

class SuperUserMiddleware
{

    protected $admRepo;

    public function __construct(AdminRepo $adminRepo)
    {
        $this->admRepo = $adminRepo;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$this->admRepo->isSuperUser())
        {
            abort(403, 'Access denied');
        }

        return $next($request);

    }
}
