<?php

namespace App\Http\ViewComposers\Admin;


use Illuminate\View\View;
use App\Repos\AdminRepo;

class CommonComposer
{

    protected $admin;


    public function __construct(AdminRepo $adminRepo)
    {
        // Dependencies automatically resolved by service container...
        $this->admin = $adminRepo;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $view->with('fullName', $this->admin->getFullName());
        $view->with('avatar', $this->admin->getAvatar());


    }
}