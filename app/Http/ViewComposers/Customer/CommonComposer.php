<?php

namespace App\Http\ViewComposers\Customer;


use Illuminate\View\View;
use App\Repos\CustomerRepo;

class CommonComposer
{

    protected $customer;


    public function __construct(CustomerRepo $customer)
    {
        // Dependencies automatically resolved by service container...
        $this->customer = $customer;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $view->with('ID', $this->customer->getId());
        $view->with('fullName', $this->customer->getFullName());
        $view->with('avatar', $this->customer->getAvatar());
        $view->with('balance', $this->customer->getBalance());


    }
}