<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repos\TicketCategoryRepo;

class TestController extends Controller
{
    private $ticketCategory;

    public function __construct(TicketCategoryRepo $ticketCategory)
    {
        $this->ticketCategory = $ticketCategory;
    }

    public function index()
    {
        $category = $this->ticketCategory->getById(1, ['users']);

        dd($category->toArray());
    }
}
