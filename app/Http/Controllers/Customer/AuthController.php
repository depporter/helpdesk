<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Customer\AuthRequest;

use Auth;

class AuthController extends Controller
{
    public function getLogin(): View
    {

        return view('customer.auth.login');
    }

    public function postLogin(AuthRequest $request)
    {
        if (!Auth::guard('customer')->attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'active' => 1]))
        {
            return response('auth fail', 403);
        }
    }

    public function getLogout()
    {
        Auth::guard('customer')->logout();
        return redirect()->route('CustomerLogin');
    }
}
