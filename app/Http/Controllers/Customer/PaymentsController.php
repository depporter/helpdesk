<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repos\CustomerTopUpRepo;
use App\Repos\ExpenseRepo;
use App\Repos\CustomerRepo;

class PaymentsController extends Controller
{
    protected $cutomerTopUpRepo;
    protected $expenseRepo;
    protected $customerRepo;

    public function __construct(CustomerTopUpRepo $customerTopUpRepo,
                                ExpenseRepo $expenseRepo,
                                CustomerRepo $customerRepo)
    {
        $this->cutomerTopUpRepo = $customerTopUpRepo;
        $this->expenseRepo = $expenseRepo;
        $this->customerRepo = $customerRepo;
    }

    public function index()
    {
        $customerId = $this->customerRepo->getId();
        $topUps = $this->cutomerTopUpRepo->getCustomerTransactions($customerId);
        $expenses = $this->expenseRepo->getCustomerTransactions($customerId);

        return view('customer.payments.index', [
            'topUps' => $topUps,
            'expenses' => $expenses
        ]);
    }
}
