<?php

namespace App\Http\Controllers\Customer\Nbki;

use App\Http\Requests\Customer\NbkiContactRequest;
use App\Repos\CustomerRepo;
use App\Repos\NbkiContactRepo;
use App\Services\Nbki\NbkiService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\ResponseTrait;

use Illuminate\Support\Facades\Log;

class ContactController extends Controller
{
    use ResponseTrait;

    protected $nbkiService;
    protected $nbkiContactRepo;
    protected $customerRepo;

    public function __construct(NbkiService $nbkiService,
                                NbkiContactRepo $nbkiContactRepo,
                                CustomerRepo $customerRepo)
    {
        $this->nbkiService = $nbkiService;
        $this->nbkiContactRepo = $nbkiContactRepo;
        $this->customerRepo = $customerRepo;
    }

    public function index()
    {
        return view('customer.nbki.contacts.index');
    }

    public function getList(Request $request)
    {

        $customerId = $this->customerRepo->getId();

        $this->nbkiContactRepo->setFilterCustomer($customerId);

        if ($request->has('filter_first_name'))
        {
            $this->nbkiContactRepo->setFilterFirstName($request->input('filter_first_name'));
        }

        if ($request->has('filter_last_name'))
        {
            $this->nbkiContactRepo->setFilterLastName($request->input('filter_last_name'));
        }

        if ($request->has('filter_doc_series'))
        {
            $this->nbkiContactRepo->setFilterDocSeries($request->input('filter_doc_series'));
        }

        if ($request->has('filter_doc_number'))
        {
            $this->nbkiContactRepo->setFilterDocNumber($request->input('filter_doc_number'));
        }

        $contacts = $this->nbkiContactRepo->filter();

        return view('customer.nbki.contacts.list', [
            'contacts' => $contacts
        ]);
    }

    public function create()
    {
        $customer = $this->customerRepo->getAuthUser();
        $queriesTotal = $this->nbkiContactRepo->queriesTotalCreatedThisMonth($customer->id);
//        dd($queriesTotal);

        return view('customer.nbki.contacts.create', [
            'customer' => $customer,
            'queriesTotal' => $queriesTotal
        ]);
    }

    public function store(NbkiContactRequest $request)
    {
        $customer = $this->customerRepo->getAuthUser();

        if ($customer->actualization_contacts_type == 'off')
        {
            return response()->json([
                'error' => true,
                'message' => 'Использование услуги отключено'
            ], 409);
        }

        if ($customer->actualization_contacts_type == 'query' && $customer->balance < $customer->actualization_contacts_query_price)
        {
            return response()->json([
                'error' => true,
                'message' => 'У вас не достаточно средств для выполнения запроса'
            ], 409);
        }

        if ($customer->actualization_contacts_type == 'package')
        {
            $createdThisMonth = $this->nbkiContactRepo->queriesTotalCreatedThisMonth($customer->id);

            Log::info('Contact. Customer : '.$customer->id . ' Created this month ' . $createdThisMonth);

            if ($createdThisMonth >= $customer->actualization_contacts_package_amount)
            {
                return response()->json([
                    'error' => true,
                    'message' => 'Пакет исчерпан'
                ], 409);
            }
        }

        $price = ($customer->actualization_contacts_type == 'query') ? $customer->actualization_contacts_query_price : 0;

        //dd(explode(',', str_replace(' ', '', $request->input('excluded_phones'))));
        $request = $this->nbkiContactRepo->create([
            'type' => $customer->actualization_contacts_type,
            'customer_id' => $customer->id,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'middle_name' => $request->input('middle_name'),
            'date_birth' => date("Y-m-d", strtotime($request->input('date_birth'))),
            'excluded_phones' => json_encode(explode(',', str_replace(' ', '', $request->input('excluded_phones')))),
//            'mode' => $request->input('mode'),
            'months' => $request->input('months'),
            'limit' => $request->input('limit'),
            'doc_series' => $request->input('doc_series'),
            'doc_number' => $request->input('doc_number'),
            'doc_issue_date' => date("Y-m-d", strtotime($request->input('doc_issue_date'))),
            'doc_type' => 21,
            'cost' => $price
        ]);

        $response = $this->nbkiService->contact($request);

        $request->request_id = $response['header']['reportiid'];
        $request->response_status = $response['status'];
        $request->response = json_encode($response);
        $request->save();

        if ($customer->actualization_contacts_type == 'query')
        {
            $customer->balance -= $customer->actualization_contacts_query_price;
            $customer->save();
        }

        return response()->json(['result_url' => route('CustomerNbkiContactResult', ['id' => $request->id]) ]);
    }

    public function result($id)
    {
        $customer = $this->customerRepo->getAuthUser();
        $result = $this->nbkiContactRepo->getCustomerResult($customer->id, $id);

        $response = json_decode($result->response, true);

        return view('customer.nbki.contacts.result', [
            'result' => $result,
            'response' => $response,
        ]);
    }
}
