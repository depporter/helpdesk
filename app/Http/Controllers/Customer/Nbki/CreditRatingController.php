<?php

namespace App\Http\Controllers\Customer\Nbki;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// services
use App\Services\Nbki\NbkiService;

// repos
use App\Repos\NbkiCreditRatingRepo;
use App\Repos\CustomerRepo;

// requests
use App\Http\Requests\Customer\NbkiCreditRatingRequest;
use Illuminate\Http\ResponseTrait;

use PDF;

use Illuminate\Support\Facades\Log;


class CreditRatingController extends Controller
{
    use ResponseTrait;
    protected $nbkiService;
    protected $nbkiCreditRatingRepo;
    protected $customerRepo;

    public function __construct(NbkiService $nbkiService,
                                NbkiCreditRatingRepo $nbkiCreditRatingRepo,
                                CustomerRepo $customerRepo)
    {
        $this->nbkiService = $nbkiService;
        $this->nbkiCreditRatingRepo = $nbkiCreditRatingRepo;
        $this->customerRepo = $customerRepo;
    }

    public function index()
    {
        return view('customer.nbki.credit_rating.index');
    }

    public function getList(Request $request)
    {

        $customerId = $this->customerRepo->getId();

        $this->nbkiCreditRatingRepo->setFilterCustomer($customerId);

        if ($request->has('filter_first_name'))
        {
            $this->nbkiCreditRatingRepo->setFilterFirstName($request->input('filter_first_name'));
        }

        if ($request->has('filter_last_name'))
        {
            $this->nbkiCreditRatingRepo->setFilterLastName($request->input('filter_last_name'));
        }

        if ($request->has('filter_doc_series'))
        {
            $this->nbkiCreditRatingRepo->setFilterDocSeries($request->input('filter_doc_series'));
        }

        if ($request->has('filter_doc_number'))
        {
            $this->nbkiCreditRatingRepo->setFilterDocNumber($request->input('filter_doc_number'));
        }

        $ratings = $this->nbkiCreditRatingRepo->filter();
        return view('customer.nbki.credit_rating.list', [
            'ratings' => $ratings
        ]);
    }

    public function create()
    {
        $customer = $this->customerRepo->getAuthUser();
        $queriesTotal = $this->nbkiCreditRatingRepo->queriesTotalCreatedThisMonth($customer->id);
        return view('customer.nbki.credit_rating.create', [
            'customer' => $customer,
            'queriesTotal' => $queriesTotal
        ]);
    }

    public function store(NbkiCreditRatingRequest $request)
    {
        $customer = $this->customerRepo->getAuthUser();


        if ($customer->nbki_credit_rating_type == 'off')
        {
            return response()->json([
                'error' => true,
                'message' => 'Использование услуги отключено'
            ], 409);
        }

        if ($customer->nbki_credit_rating_type == 'query' && $customer->balance < $customer->nbki_credit_rating_query_price)
        {
            return response()->json([
                'error' => true,
                'message' => 'У вас не достаточно средств для выполнения запроса'
            ], 409);
        }

        if ($customer->nbki_credit_rating_type == 'package')
        {
            $createdThisMonth = $this->nbkiCreditRatingRepo->queriesTotalCreatedThisMonth($customer->id);

            Log::info('Credit rating. Customer : '.$customer->id . ' Created this month ' . $createdThisMonth);

            if ($createdThisMonth >= $customer->nbki_credit_rating_package_amount)
            {
                return response()->json([
                    'error' => true,
                    'message' => 'Пакет исчерпан'
                ], 409);
            }
        }


        $price = ($customer->nbki_credit_rating_type == 'query') ? $customer->nbki_credit_rating_query_price : 0;

        $request = $this->nbkiCreditRatingRepo->create([
            'type' => $customer->nbki_credit_rating_type,
            'customer_id' => $customer->id,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'middle_name' => $request->input('middle_name'),
            'doc_series' => $request->input('doc_series'),
            'doc_number' => $request->input('doc_number'),
            'doc_type' => 21,
            'cost' => $price
        ]);



        $response = $this->nbkiService->creditRating($request);

        $request->request_id = $response['header']['reportiid'];
        $request->response_status = $response['status'];
        $request->response = json_encode($response);
        $request->save();

        if ($customer->nbki_credit_rating_type == 'query')
        {
            $customer->balance -= $customer->nbki_credit_rating_query_price;
            $customer->save();
        }



        return response()->json(['result_url' => route('CustomerNbkiCreditRatingResult', ['id' => $request->id]) ]);
    }

    public function result($id)
    {
        $customer = $this->customerRepo->getAuthUser();
        $result = $this->nbkiCreditRatingRepo->getCustomerResult($customer->id, $id);

        $response = json_decode($result->response, true);

        $dict = [
            '0' => 'Новый, оценка невозможна',
            'X' => 'Нет информации',
            '1' => 'Оплата без просрочек',
            'A' => 'Просрочка от 1 до 29 дней',
            '2' => 'Просрочка от 30 до 59 дней',
            '3' => 'Просрочка от 60 до 89 дней',
            '4' => 'Просрочка от 90 до 119 дней',
            '5' => 'Просрочка более 120 дней',
            '7' => 'Регулярные консолидированные платежи',
            '8' => 'Погашение по кредиту с использованием залога',
            '9' => 'Безнадёжный долг/ передано на взыскание/ пропущенный платеж',

        ];

        return view('customer.nbki.credit_rating.result', [
            'result' => $result,
            'response' => $response,
            'dict' => $dict
        ]);
    }

    public function pdf($id)
    {
        $customer = $this->customerRepo->getAuthUser();
        $result = $this->nbkiCreditRatingRepo->getCustomerResult($customer->id, $id);

        $response = json_decode($result->response, true);

       //dd(count($response['response']['checks']['check']));
        $pdf = PDF::loadView('customer.nbki.credit_rating.pdf_export', [
            'result' => $result,
            'response' => $response
        ]);


        return $pdf->download($result->first_name . ' ' . $result->last_name . ' - ' . $result->doc_series . $result->doc_number .  '.pdf');
    }
}
