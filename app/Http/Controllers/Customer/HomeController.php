<?php

namespace App\Http\Controllers\Customer;

use App\Repos\NbkiContactRepo;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

// repos
use App\Repos\NewsRepo;
use App\Repos\PageRepo;
use App\Repos\TicketRepo;
use App\Repos\CustomerRepo;
use App\Repos\NbkiCreditRatingRepo;

class HomeController extends Controller
{
    protected $newsRepo;
    protected $pageRepo;
    protected $ticketRepo;
    protected $customerRepo;
    protected $nbkiCreditRatingRepo;
    protected $nbkiContactRepo;

    public function __construct(NewsRepo $newsRepo,
                                PageRepo $pageRepo,
                                TicketRepo $ticketRepo,
                                CustomerRepo $customerRepo,
                                NbkiCreditRatingRepo $nbkiCreditRatingRepo,
                                NbkiContactRepo $nbkiContactRepo)
    {
        $this->newsRepo = $newsRepo;
        $this->pageRepo = $pageRepo;
        $this->ticketRepo = $ticketRepo;
        $this->customerRepo = $customerRepo;
        $this->nbkiCreditRatingRepo = $nbkiCreditRatingRepo;
        $this->nbkiContactRepo = $nbkiContactRepo;
    }

    public function index(): View
    {
        $this->newsRepo->setFilterPublish(1);
        $news = $this->newsRepo->filter();
        $contacts = Storage::get('contacts');


        $ticketsWithAnswers = $this->ticketRepo->getCustomersWithAnswers($this->customerRepo->getId());

        $this->pageRepo->setFilterPublish(1);
        $pages = $this->pageRepo->filter();

        $ticketsCategories  = $this->ticketRepo->ticketCategoryTotalCreatedThisMonth($this->customerRepo->getId());

        //dd($ticketsCategories);

        $customer = $this->customerRepo->getById($this->customerRepo->getId(), ['ticketCategories']);

        $nbkiThisMonthTotal = $this->nbkiCreditRatingRepo->queriesTotalCreatedThisMonth($customer->id);

        $nbkiThisMonthTotalContact = $this->nbkiContactRepo->queriesTotalCreatedThisMonth($customer->id);

        return view('customer.home.index', [
            'news' => $news,
            'pages' => $pages,
            'contacts' => $contacts,
            'ticketsWithAnswers' => $ticketsWithAnswers,
            'customer' => $customer,
            'ticketsCategories' => $ticketsCategories,
            'nbkiThisMonthTotal' => $nbkiThisMonthTotal,
            'nbkiThisMonthTotalContact' => $nbkiThisMonthTotalContact
        ]);
    }

    public function getNews($newsId)
    {
        $news = $this->newsRepo->getFirstByIdAndWhere($newsId, ['publish' => 1]);
        return view('customer.home.news_item', ['news' => $news]);
    }

    public function getPage($pageId)
    {
        $page = $this->pageRepo->getFirstByIdAndWhere($pageId, ['publish' => 1]);
        return view('customer.home.page_item', ['page' => $page]);
    }
}
