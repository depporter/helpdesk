<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\ResponseTrait;
use Mail;
use Zipper;
use Debugbar;
use Illuminate\Support\Facades\Log;

// repos
use App\Repos\TicketRepo;
use App\Repos\TicketCategoryRepo;
use App\Repos\CustomerRepo;
use App\Repos\AdminRepo;
use App\Repos\TicketAttachmentRepo;
use App\Repos\ExpenseRepo;

// requests
use App\Http\Requests\Customer\TicketsRequest;

// mails
use App\Mail\Admin\TicketMessageFromCustomer;
use App\Mail\ForSuperUserOnTicketCreated;
use App\Mail\Admin\TicketCreated;

class TicketController extends Controller
{
    use ResponseTrait;

    protected $ticketRepo;
    protected $ticketCategoryRepo;
    protected $customerRepo;
    protected $adminRepo;
    protected $ticketAttachmentRepo;
    protected $expenseRepo;



    public function __construct(TicketRepo $ticketRepo,
                                TicketCategoryRepo $ticketCategoryRepo,
                                CustomerRepo $customerRepo,
                                AdminRepo $adminRepo,
                                TicketAttachmentRepo $ticketAttachmentRepo,
                                ExpenseRepo $expenseRepo)
    {
        $this->ticketRepo = $ticketRepo;
        $this->ticketCategoryRepo = $ticketCategoryRepo;
        $this->customerRepo = $customerRepo;
        $this->adminRepo = $adminRepo;
        $this->ticketAttachmentRepo = $ticketAttachmentRepo;
        $this->expenseRepo = $expenseRepo;
    }

    public function create(): View
    {

        $customer = $this->customerRepo->getById($this->customerRepo->getId(), ['ticketCategories']);


        $categoryTicketsTotal = $this->ticketRepo->ticketCategoryTotalCreatedThisMonth($customer->id);

        $categories = $customer->ticketCategories;

       // dd($categories->toArray());

        return view('customer.tickets.create', [
            'categories' => $categories,
            'categoryTicketsTotal' => $categoryTicketsTotal
        ]);
    }

    public function actual(): View
    {
        return view('customer.tickets.actual');
    }

    public function archive(): View
    {
        return view('customer.tickets.archive');
    }

    public function done(): View
    {
        return view('customer.tickets.done');
    }

    public function getList($type)
    {
        switch ($type)
        {
            case 'actual':
                $tickets = $this->ticketRepo->getCustomerActual($this->customerRepo->getId());
                return view('customer.tickets.actual_list', ['tickets' => $tickets]);
                break;

            case 'archive':
                $tickets = $this->ticketRepo->getCustomerArchive($this->customerRepo->getId());
                return view('customer.tickets.archive_list', ['tickets' => $tickets]);
                break;

            case 'done':
                $tickets = $this->ticketRepo->getCustomerDone($this->customerRepo->getId());
                return view('customer.tickets.done_list', ['tickets' => $tickets]);
                break;
        }

    }

    public function store(TicketsRequest $request)
    {
        // в топку если нет кастомера
        $customer = $this->customerRepo->getById($this->customerRepo->getId(), ['ticketCategories']);

        if (!$customer)
        {
            abort(404, 'User not found');
        }


        $ticketCategoryId = (int) $request->input('ticket_category_id');


        $category = $this->ticketCategoryRepo->getById($ticketCategoryId, ['users']);

        if ($category->type == 'by_price')
        {
            if ($customer->balance < $category->price && $customer->allow_work_on_credit && $customer->credit_up_to < (int) date("j"))
            {
                return response()->json('Кредит исчерпан, пополните баланс', 402);
            }

            if ($customer->balance < $category->price && !$customer->allow_work_on_credit)
            {
                return response()->json('Баланс исчерпан', 402);
            }

        }

        $categoryLimit = $this->customerRepo->getTicketCategoryLimit($customer, $ticketCategoryId);

       // Debugbar::info($categoryLimit->toArray());

        //dd('ok');

        if ($category->type == 'limit_countable' && $categoryLimit->pivot->ticket_limit == 0)
        {
            return response()->json('Лимит исчерпан', 402);

        }


        if ($category->type == 'limit_monthly' && !$this->ticketRepo->canCustomerCreate($customer->id, $ticketCategoryId,  $categoryLimit->pivot->ticket_limit))
        {
            return response()->json('Лимит исчерпан', 402);
        }

        $ticket = $this->ticketRepo->create([
            'creator' => 'customer',
            'customer_id' => $customer->id,
            'ticket_category_id' => $request->input('ticket_category_id'),
            'title' => $request->input('title'),
            'user_unread_messages' => 1
        ]);

        if ($category->type == 'by_price')
        {
            $customer->balance -= $category->price;
            $customer->save();

            $this->expenseRepo->create([
                'customer_id' => $customer->id,
                'amount' => $category->price,
                'comment' => 'Списание средств за тикет #' . $ticket->id . ' в категории ' . $category->name
            ]);
        }


        $ticket->messages()->create([
            'user_id' => $customer->id,
            'author' => 'customer',
            'message' => $request->input('message')
        ]);

        if ($request->hasFile('image'))
        {
            foreach ($request->file('image') as $file)
            {
                $this->ticketAttachmentRepo->attach($ticket->id, $customer->id, 'customer', $file);
            }
        }

        if ($category->type == 'limit_countable')
        {
            $changedLimit = $categoryLimit->pivot->ticket_limit - 1;
            $customer->ticketCategories()->updateExistingPivot($categoryLimit->id, ['ticket_limit' => $changedLimit]);
        }


        //dd($category->users->toArray());
        foreach ($category->users as $admin)
        {
            Log::info('Send email to: ' . $admin->email);
            Mail::to($admin->email)->queue(new TicketCreated($admin, $ticket));
        }

        return response()->json(['url' => route('CustomerTicketShow', ['ticketId' => $ticket->id])]);

    }



    public function show(int $ticketId): View
    {
        $ticket = $this->ticketRepo->getById($ticketId, ['messages', 'customerFiles', 'supportFiles', 'support']);


        if ($ticket->customer_id != $this->customerRepo->getId())
        {
            abort(403, 'Нельзя смотреть чужие тикеты');
        }

        $ticket->customer_unread_messages = 0;
        $ticket->save();

        return view('customer.tickets.show', [
            'ticket' => $ticket
        ]);
    }

    public function messageStore(TicketsRequest $request, $ticketId): View
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['customer_id' => $this->customerRepo->getId()]);

        if (!$ticket)
        {
            abort(404, 'Cant get ticket');
        }

        $message = $ticket->messages()->create([
            'user_id' => $this->customerRepo->getId(),
            'author' => 'customer',
            'message' => $request->input('message')
        ]);

        $ticket->user_unread_messages +=1;
        $ticket->customer_archived = 0;
        $ticket->support_archived = 0;
        $ticket->save();

        // отправка мыла
        $admin = $this->adminRepo->getById($ticket->support_id);

        if ($admin)
        {
            Mail::to($admin->email)->queue(new TicketMessageFromCustomer($admin, $ticket));
        }



        return view('customer.tickets.message', ['message' => $message]);

    }

    public function upload(TicketsRequest $request, $ticketId)
    {
        foreach ($request->file('files') as $file)
        {
            $this->ticketAttachmentRepo->attach($ticketId, $this->customerRepo->getId(), 'customer', $file);
        }

    }

    public function getFiles($ticketId, $type)
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['customer_id' => $this->customerRepo->getId()], ['customerFiles', 'supportFiles']);

        if (!$ticket)
        {
            return;
        }

        $files = [];
        switch ($type)
        {
            case 'customer':
                $files =  $ticket->customerFiles;
                break;

            case 'support':
                $files =  $ticket->supportFiles;
                break;
        }

        return view('customer.tickets.files', ['files' => $files]);

    }

    public function action($ticketId, $action)
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['customer_id' => $this->customerRepo->getId()]);
        switch ($action)
        {
            case 'archive':
                $ticket->customer_archived = 1;
                $ticket->save();
                break;
        }
    }

    public function edit($ticketId)
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['customer_id' => $this->customerRepo->getId()]);

        return view('customer.tickets.edit', ['ticket' => $ticket]);
    }

    public function update(TicketsRequest $request, $ticketId)
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['customer_id' => $this->customerRepo->getId()]);

        $ticket->title = $request->input('title');
        $ticket->save();
    }

    public function download($ticketId)
    {
        $files = $this->ticketAttachmentRepo->getSupportFiles($ticketId);

        if ($files->count())
        {
            $filesArray = $files->pluck('orig_file_name')->toArray();

            foreach ($filesArray as $k => $value)
            {
                $filesArray[$k] = storage_path() . '/app/public/tickets_media/' . $value;
            }


            $zipFile = storage_path() . '/app/public/tickets_media/' . $ticketId . '_' . uniqid() . '.zip';


            Zipper::make($zipFile)->add($filesArray)->close();

            if (file_exists($zipFile))
            {

                return response()->download($zipFile)->deleteFileAfterSend(true);
            }
        }
    }


}
