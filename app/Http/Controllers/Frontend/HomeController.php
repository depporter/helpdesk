<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Frontend\Register;
use App\Models\RegistrationRequest;
use App\Repos\RegRequestRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class HomeController extends Controller
{


    protected $regRequestRepo;

    public function __construct(RegRequestRepo $regRequestRepo)
    {
        $this->regRequestRepo = $regRequestRepo;
    }

    public function index(Request $request)
    {
        $rosReestrOnly = false;
        if ($request->segment(1) == 'rr')
        {
            $rosReestrOnly = true;
        }

        if ($rosReestrOnly)
        {
            return view('frontend.home.rosreestr');
        }

        return view('frontend.home.index');
    }

    public function register(Register $request)
    {
        $this->regRequestRepo->create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'services' => json_encode($request->input('service')),
        ]);

        return response()->json('ok');
    }
}
