<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

// repos
use App\Repos\AdminRepo;

// requests
use App\Http\Requests\Admin\AuthRequest;

class AuthController extends Controller
{
    protected $adminRepo;

    public function __construct(AdminRepo $adminRepo)
    {
        $this->adminRepo = $adminRepo;

    }

    public function getLogin()
    {
        return view('admin.auth.login');
    }

    public function postLogin(AuthRequest $request)
    {

        if (!Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'active' => 1]))
        {
            return response('auth fail', 403);
        }
    }

    public function getLogout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('AdminLogin');
    }
}
