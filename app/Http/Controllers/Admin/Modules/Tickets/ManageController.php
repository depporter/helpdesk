<?php

namespace App\Http\Controllers\Admin\Modules\Tickets;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// repos
use App\Repos\TicketRepo;
use App\Repos\CustomerRepo;
use App\Repos\AdminRepo;
use App\Repos\TicketCategoryRepo;

class ManageController extends Controller
{
    protected $ticketRepo;
    protected $adminRepo;
    protected $customerRepo;
    protected $ticketCategoryRepo;

    public function __construct(TicketRepo $ticketRepo,
                                AdminRepo $adminRepo,
                                CustomerRepo $customerRepo,
                                TicketCategoryRepo $ticketCategoryRepo)
    {
        $this->ticketRepo = $ticketRepo;
        $this->adminRepo = $adminRepo;
        $this->customerRepo = $customerRepo;
        $this->ticketCategoryRepo = $ticketCategoryRepo;
    }

    public function index()
    {
        $nobodyTickets = $this->ticketRepo->getNobody();

        $assignedTickets = $this->ticketRepo->getAssigned();
        $admins = $this->adminRepo->all();
        $customers = $this->customerRepo->all();
        $ticketCategories = $this->ticketCategoryRepo->all();

       // dd($assignedTickets->toArray());

        return view('admin.manage.tickets.index', [
            'nobodyTickets' => $nobodyTickets,
            'assignedTickets' => $assignedTickets,
            'admins' => $admins,
            'customers' => $customers,
            'ticketCategories' => $ticketCategories
        ]);
    }

    public function getFilterList(Request $request)
    {
        if ($request->has('filter_support_id'))
        {
            $this->ticketRepo->setSupportId($request->input('filter_support_id'));
        }

        if ($request->has('filter_customer_id'))
        {
            $this->ticketRepo->setCustomerId($request->input('filter_customer_id'));
        }

        if ($request->has('filter_category_id'))
        {
            $this->ticketRepo->setCategoryId($request->input('filter_category_id'));
        }

        if ($request->has('filter_is_archived'))
        {
            $this->ticketRepo->setIsArchived($request->input('filter_is_archived'));
        }

        $results = $this->ticketRepo->filter();

        return view('admin.manage.tickets.list', [
            'results' => $results
        ]);
    }

    public function showTicket($ticketId)
    {
        $ticket = $this->ticketRepo->getById($ticketId, ['category', 'customer', 'support', 'customerFiles', 'supportFiles']);
        return view('admin.manage.tickets.show_ticket', ['ticket' => $ticket]);
    }

    public function archives()
    {
        $files = Storage::files('./public/tickets_archives');
        return view('admin.manage.tickets.archives', [
            'files' => $files
        ]);
    }

    public function archivesDownload($fileName)
    {

        $file = storage_path() . '/app/public/tickets_archives/' . $fileName;
        if(file_exists($file))
        {
            return response()->download($file)->deleteFileAfterSend(true);
        } else {
            die('file not exists');
        }

    }
}

