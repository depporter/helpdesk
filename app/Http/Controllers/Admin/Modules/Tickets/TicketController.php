<?php

namespace App\Http\Controllers\Admin\Modules\Tickets;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Illuminate\Support\Facades\Auth;

// repos
use App\Repos\AdminRepo;
use App\Repos\CustomerRepo;
use App\Repos\TicketRepo;
use App\Repos\TicketCategoryRepo;
use App\Repos\TicketAttachmentRepo;
use App\Models\TicketStat;

// requests
use App\Http\Requests\Admin\TicketsRequest;

// mails
use App\Mail\Admin\TicketMessageFromSupport;


class TicketController extends Controller
{
    protected $adminRepo;
    protected $customerRepo;
    protected $ticketRepo;
    protected $ticketCategoryRepo;
    protected $ticketAttachmentRepo;
    protected $ticketStat;

    public function __construct(AdminRepo $adminRepo,
                                CustomerRepo $customerRepo,
                                TicketRepo $ticketRepo,
                                TicketCategoryRepo $ticketCategoryRepo,
                                TicketAttachmentRepo $ticketAttachmentRepo,
                                TicketStat $ticketStat)
    {
        $this->adminRepo = $adminRepo;
        $this->customerRepo = $customerRepo;
        $this->ticketRepo = $ticketRepo;
        $this->ticketCategoryRepo = $ticketCategoryRepo;
        $this->ticketAttachmentRepo = $ticketAttachmentRepo;
        $this->ticketStat = $ticketStat;
    }

    public function nobody(): View
    {
        return view('admin.tickets.nobody');
    }

    public function getList($type): View
    {
        switch ($type)
        {
            case 'nobody':
                $allowedCategories = $this->adminRepo->getAllowedTicketCategoriesIds();

                $tickets = $this->ticketRepo->getOperatorNobody($allowedCategories);


                return view('admin.tickets.nobody_list', ['tickets' => $tickets]);
                break;

            case 'my':
                $tickets = $this->ticketRepo->getOperatorAssigned($this->adminRepo->getAuthUserId());
                return view('admin.tickets.my_list', ['tickets' => $tickets]);
                break;

            case 'archive':
                $tickets = $this->ticketRepo->getOperatorArchived($this->adminRepo->getAuthUserId());
                return view('admin.tickets.archived_list', ['tickets' => $tickets]);
                break;
        }
    }

    public function my(): View
    {
        return view('admin.tickets.my');
    }

    public function archive()
    {
        return view('admin.tickets.archive');
    }

    public function take(int $ticketId)
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['support_id' => null]);
        $allowedCategories = $this->adminRepo->getAllowedTicketCategoriesIds();

        if ($ticket && in_array($ticket->ticket_category_id, $allowedCategories))
        {
            $ticket->support_id = $this->adminRepo->getAuthUserId();
            $ticket->assigned_at = date('Y-m-d H:i:s');
            $ticket->save();

            return redirect(route('AdminTicketShow', ['ticketId' => $ticketId]));
        }
    }

    public function show(int $ticketId)
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['support_id' => $this->adminRepo->getAuthUserId()], ['messages', 'customerFiles', 'supportFiles', 'category', 'customer']);

        if (!$ticket)
        {
            abort(404, 'Ticket not found');
        }

        $ticket->user_unread_messages = 0;
        $ticket->save();

        return view('admin.tickets.show', ['ticket' => $ticket]);
    }

    public function messageStore(TicketsRequest $request, int $ticketId)
    {
        $ticket = $this->ticketRepo->getById($ticketId);
        $user = $this->adminRepo->getAuthUser();

        if (!$user->super_user && $ticket->support_id != $user->id)
        {
            abort(404, 'Ticket not found');
        }


        $message = $ticket->messages()->create([
            'user_id' => $this->adminRepo->getAuthUserId(),
            'author' => 'support',
            'message' => $request->input('message')
        ]);

        $ticket->customer_unread_messages +=1;
        $ticket->customer_archived = 0;
        $ticket->support_archived = 0;
        $ticket->save();

        // отправка мыла
        $customer = $this->customerRepo->getById($ticket->customer_id);

        Mail::to($customer->email)->queue(new TicketMessageFromSupport($customer, $ticket));


        return view('admin.tickets.message', ['message' => $message]);
    }

    public function upload(TicketsRequest $request, $ticketId)
    {
        foreach ($request->file('files') as $file)
        {
            $this->ticketAttachmentRepo->attach($ticketId, $this->adminRepo->getAuthUserId(), 'support', $file);
        }

    }

    public function getFiles($ticketId, $type)
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['support_id' => $this->adminRepo->getAuthUserId()], ['customerFiles', 'supportFiles']);

        if (!$ticket)
        {
            return;
        }

        $files = [];
        switch ($type)
        {
            case 'customer':
                $files =  $ticket->customerFiles;
                break;

            case 'support':
                $files =  $ticket->supportFiles;
                break;
        }

        return view('admin.tickets.files', ['files' => $files]);

    }

    public function deleteFile($ticketId, $fileId)
    {
        $this->ticketAttachmentRepo->delete($ticketId, $fileId);
    }

    public function action($ticketId, $action)
    {
        $ticket = $this->ticketRepo->getFirstByIdAndWhere($ticketId, ['support_id' => $this->adminRepo->getAuthUserId()]);
        switch ($action)
        {
            case 'archive':
                $ticket->support_archived = 1;
                $ticket->support_archived_at = date('Y-m-d H:i:s');
                $ticket->save();

                $this->ticketStat->firstOrCreate([
                    'user_id' => Auth::guard('admin')->id(),
                    'ticket_id' => $ticket->id
                ]);
                break;
        }
    }
}
