<?php

namespace App\Http\Controllers\Admin\Catalogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;

// repos
use App\Repos\TicketCategoryRepo;
use App\Repos\AdminRepo;
use App\Repos\CustomerRepo;

// requests
use App\Http\Requests\Admin\Catalogs\TicketCategoryRequest;

// helpers
use App\Support\Helpers\StringsHelper;

class TicketCategoryController extends Controller
{
    protected $ticketCategoryRepo;
    protected $adminRepo;
    protected $customerRepo;
    protected $stringsHelper;

    public function __construct(TicketCategoryRepo $ticketCategoryRepo,
                                AdminRepo $adminRepo,
                                CustomerRepo $customerRepo,
                                StringsHelper $stringsHelper)
    {
        $this->ticketCategoryRepo = $ticketCategoryRepo;
        $this->adminRepo = $adminRepo;
        $this->customerRepo = $customerRepo;
        $this->stringsHelper = $stringsHelper;
    }

    public function index(): View
    {
        return view('admin.catalogs.ticket_category.index');
    }

    public function getList(): View
    {
        $categories = $this->ticketCategoryRepo->filter();

        return view('admin.catalogs.ticket_category.list', [
            'categories' => $categories
        ]);
    }

    public function create()
    {
        $formAction = route('AdminCatalogsTicketCategoryStore');
        return view('admin.catalogs.ticket_category.edit', [
            'formAction' => $formAction
        ]);
    }

    public function store(TicketCategoryRequest $request)
    {
        $this->ticketCategoryRepo->create([
            'name' => $request->input('name'),
            'title_hint' => $request->input('title_hint'),
            'desc_hint' => $request->input('desc_hint'),
            'type' => $request->input('type'),
            'price' => ($request->input('type') != 'by_price') ? 0 : $request->input('price')
        ]);
    }

    public function edit(int $categoryId): View
    {
        $category = $this->ticketCategoryRepo->getById($categoryId, ['users']);
        $categoryUsersIds = $category->users->pluck('id')->toArray();
        $formAction = route('AdminCatalogsTicketCategoryUpdate', ['categoryId' => $categoryId]);

        return view('admin.catalogs.ticket_category.edit', [
            'category' => $category,
            'formAction' => $formAction,

            'categoryUsersIds' => $categoryUsersIds
        ]);
    }

    public function update(TicketCategoryRequest $request, int $categoryId)
    {
        $category = $this->ticketCategoryRepo->getById($categoryId);
        $category->update([
            'name' => $request->input('name'),
            'title_hint' => $request->input('title_hint'),
            'desc_hint' => $request->input('desc_hint'),
            'type' => $request->input('type'),
            'price' => ($request->input('type') != 'by_price') ? 0 : $request->input('price')
        ]);


    }

    public function delete(int $categoryId)
    {
        $this->ticketCategoryRepo->destroy($categoryId);
    }
}
