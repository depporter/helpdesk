<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Mail;

use Auth;

// repos
use App\Repos\AdminRepo;
use App\Repos\CustomerRepo;
use App\Repos\CustomerGroupRepo;
use App\Repos\TicketCategoryRepo;
use App\Repos\TicketRepo;
use App\Repos\CustomerTopUpRepo;
use App\Repos\ExpenseRepo;
use App\Repos\RegRequestRepo;

use App\Models\Customer;


// requests
use App\Http\Requests\Admin\Users\CustomerRequest;

// mails
use App\Mail\Admin\CustomerCredentials;

class CustomerController extends Controller
{
    protected $adminRepo;
    protected $customerRepo;
    protected $customerGroupRepo;
    protected $ticketCategoryRepo;
    protected $ticketRepo;
    protected $customerTopUpRepo;
    protected $expenseRepo;
    protected $regRequestRepo;


    public function __construct(AdminRepo $adminRepo,
                                CustomerRepo $customerRepo,
                                CustomerGroupRepo $customerGroupRepo,
                                TicketCategoryRepo $ticketCategoryRepo,
                                TicketRepo $ticketRepo,
                                CustomerTopUpRepo $customerTopUpRepo,
                                ExpenseRepo $expenseRepo, RegRequestRepo $regRequestRepo)
    {
        $this->adminRepo = $adminRepo;
        $this->customerRepo = $customerRepo;
        $this->customerGroupRepo = $customerGroupRepo;
        $this->ticketCategoryRepo = $ticketCategoryRepo;
        $this->ticketRepo = $ticketRepo;
        $this->customerTopUpRepo = $customerTopUpRepo;
        $this->expenseRepo = $expenseRepo;
        $this->regRequestRepo = $regRequestRepo;
    }

    public function index(): View
    {
        return view('admin.users.customers.index');
    }

    public function getList(Request $request)
    {
        if ($request->has('filter_full_name'))
        {
            $this->customerRepo->setFilterName($request->input('filter_full_name'));
        }

        if ($request->has('filter_email'))
        {
            $this->customerRepo->setFilterEmail($request->input('filter_email'));
        }

        if ($request->has('filter_company'))
        {
            $this->customerRepo->setFilterCompany($request->input('filter_company'));
        }

        if ($request->has('filter_phone'))
        {
            $this->customerRepo->setFilterPhone($request->input('filter_phone'));
        }

        $customers = $this->customerRepo->filter();

        foreach ($customers as $customer)
        {
            $ticketCategories = $this->ticketRepo->ticketCategoryTotalCreatedThisMonth($customer->id);
            foreach ($customer->ticketCategories as $ticketCategory)
            {

                $ticketCategory->pivot->thisMonthCreated = (isset($ticketCategories[$ticketCategory->id])) ? $ticketCategories[$ticketCategory->id] : 0 ;
            }
        }

        //dd($customers->toArray());

        $groups = $this->customerGroupRepo->filter();


        return view('admin.users.customers.list', [
            'groups' => $groups,
        ]);
    }

    public function create(Request $request): View
    {
        $formAction = route('AdminUsersCustomerStore');
        $ticketCategories = $this->ticketCategoryRepo->filter();
        $groups = $this->customerGroupRepo->all();

        $user = new Customer();

        $services = null;

        if ($request->has('regId'))
        {
            $reg = $this->regRequestRepo->getById($request->input('regId'));
            $user->email = $reg->email;
            $user->full_name = $reg->name;
            $user->phone = $reg->phone;

            $services = $reg->formatServices();
        }

        return view('admin.users.customers.modal_content', [
            'formAction' => $formAction,
            'ticketCategories' => $ticketCategories,
            'groups' => $groups,
            'user' => $user,
            'services' => $services
        ]);
    }

    public function store(CustomerRequest $request)
    {




        $request->merge([
            'active' => 1,
            'credit_up_to' => ($request->has('credit_up_to')) ? $request->input('credit_up_to') : 0
        ]);

        $customer = $this->customerRepo->create($request->all());

        if ($request->has('group'))
        {
            foreach ($request->input('group') as $groupId)
            {
                $customer->groups()->attach($groupId);
            }
        } else {
            $customer->groups()->attach($this->customerGroupRepo->getDefaultGroupId());
        }

        if ($request->has('limit'))
        {
            $attachData = [];
            foreach ($request->input('limit') as $categoryId => $limit)
            {
                if ($limit)
                {
                    $attachData[$categoryId] = ['ticket_limit' => $limit];
                }
            }

            if (count($attachData))
            {
                $customer->ticketCategories()->sync($attachData);
            }
        }

        /**
         * Если при создании пользователя указали "Отправить письмо c доступами"
         * то дернем событие :)
         */
        if ($request->has('send_register_email'))
        {
            $password = $request->input('password');
            Mail::to($customer->email)->send(new CustomerCredentials($customer, $password));
        }
    }

    public function edit(int $customerId)
    {
        $user = $this->customerRepo->getById($customerId, ['ticketCategories', 'groups']);
        $formAction = route('AdminUsersCustomerUpdate', ['customerId' => $user->id]);
        $ticketCategories = $this->ticketCategoryRepo->filter();
        $ticketCategoriesIds = $user->ticketCategories->pluck('id')->toArray();
        $ticketCategoryLimit = [];

        $groups = $this->customerGroupRepo->all();
        $customerGroupsIds = $user->groups->pluck('id')->toArray();

        foreach ($user->ticketCategories as $category)
        {
            $ticketCategoryLimit[$category->id] = [
                'limit' => $category->pivot->ticket_limit,
                'countable' => $category->pivot->countable
            ];
        }

        //dd($ticketCategoryLimit);

        return view('admin.users.customers.modal_content', [
            'user' => $user,
            'formAction' => $formAction,
            'ticketCategoriesIds' => $ticketCategoriesIds,
            'ticketCategories' => $ticketCategories,
            'ticketCategoryLimit' => $ticketCategoryLimit,
            'groups' => $groups,
            'customerGroupsIds' => $customerGroupsIds
        ]);
    }

    public function update(CustomerRequest $request, $customerId)
    {
        //print_r($request->all());

        //dd('ok');
        $customer = $this->customerRepo->getById($customerId);
        $active = ($request->has('active')) ? 1 : 0;

        $request->merge([
            'active' => $active,
            'allow_nbki_credit_rating' => ($request->has('allow_nbki_credit_rating')) ? 1 : 0,
            'allow_nbki_contact_actualization' => ($request->has('allow_nbki_contact_actualization')) ? 1 : 0
        ]);

        $updateRequestFields = [
            'full_name',
            'email',
            'active',
            'company',
            'phone',
            'nbki_credit_rating_package_amount',
            'nbki_credit_rating_query_price',
            'nbki_credit_rating_type',
            'actualization_contacts_package_amount',
            'actualization_contacts_query_price',
            'actualization_contacts_type',
            'allow_work_on_credit',
            'credit_up_to'
        ];

        // обнуляем значение праса

        $customer->update($request->only($updateRequestFields));

        if ($request->has('password'))
        {
            $customer->update($request->only(['password']));
        }

        if ($request->has('limit'))
        {
            $attachData = [];

            foreach ($request->input('limit') as $categoryId => $limit)
            {
                if ($limit)
                {
                    $attachData[$categoryId] = [
                        'ticket_limit' => $limit,
                    ];
                }
            }

            if (count($attachData))
            {
                $customer->ticketCategories()->sync($attachData);
            } else {
                $customer->ticketCategories()->sync([]);
            }
        }

        $groups = ($request->has('group')) ? $request->input('group') : [$this->customerGroupRepo->getDefaultGroupId()];

        $customer->groups()->sync($groups);
    }

    public function delete($customerId)
    {
        $this->customerRepo->destroy($customerId);
    }

    public function groupCreate()
    {
        $formAction = route('AdminUsersCustomerGroupStore');

        return view('admin.users.customers.group_form', [
            'formAction' => $formAction,
        ]);
    }

    public function groupStore(CustomerRequest $request)
    {
        $this->customerGroupRepo->create(['name' => $request->input('name')]);
    }

    public function groupEdit($groupId)
    {
        $formAction = route('AdminUsersCustomerGroupUpdate', ['groupId' => $groupId]);
        $group = $this->customerGroupRepo->getById($groupId);

        return view('admin.users.customers.group_form', [
            'formAction' => $formAction,
            'group' => $group
        ]);
    }

    public function groupUpdate(CustomerRequest $request, $groupId)
    {
        $group = $this->customerGroupRepo->getById($groupId);
        $group->name = $request->input('name');
        $group->save();
    }

    public function groupCustomers($groupId)
    {
        $group = $this->customerGroupRepo->getById($groupId);
        return view('admin.users.customers.group', [
            'group' => $group
        ]);
    }

    public function groupCustomersList(Request $request, $groupId)
    {

        if ($request->has('filter_full_name'))
        {
            $this->customerGroupRepo->setFilterName($request->input('filter_full_name'));
        }

        if ($request->has('filter_email'))
        {
            $this->customerGroupRepo->setFilterEmail($request->input('filter_email'));
        }

        if ($request->has('filter_company'))
        {
            $this->customerGroupRepo->setFilterCompany($request->input('filter_company'));
        }

        if ($request->has('filter_phone'))
        {
            $this->customerGroupRepo->setFilterPhone($request->input('filter_phone'));
        }

        $customers = $this->customerGroupRepo->getCustomers($groupId);

        foreach ($customers as $customer)
        {
            $ticketCategories = $this->ticketRepo->ticketCategoryTotalCreatedThisMonth($customer->id);
            foreach ($customer->ticketCategories as $ticketCategory)
            {

                $ticketCategory->pivot->thisMonthCreated = (isset($ticketCategories[$ticketCategory->id])) ? $ticketCategories[$ticketCategory->id] : 0 ;
            }
        }

        return view('admin.users.customers.group_customers', [
            'customers' => $customers
        ]);
    }

    public function payments($customerId)
    {
        $customer = $this->customerRepo->getById($customerId);

        return view('admin.users.customers.payments.index', [
            'customer' => $customer
        ]);
    }

    public function paymentsList($customerId)
    {

        $payments = $this->customerTopUpRepo->getCustomerTransactions($customerId);

        return view('admin.users.customers.payments.list', [
            'payments' => $payments
        ]);
    }

    public function paymentsCreate($customerId)
    {
        $customer = $this->customerRepo->getById($customerId);
        $formAction = route('AdminUsersCustomerPaymentsStore', ['customerId' => $customer->id]);

        return view('admin.users.customers.payments.form', [
            'customer' => $customer,
            'formAction' => $formAction
        ]);
    }

    public function paymentsStore(CustomerRequest $request, $customerId)
    {
        $this->customerTopUpRepo->create([
            'admin_id' => $this->adminRepo->getAuthUserId(),
            'customer_id' => $customerId,
            'amount' => $request->input('amount'),
            'desc' => $request->input('desc'),
        ]);

        $customer = $this->customerRepo->getById($customerId);
        $customer->balance += $request->input('amount');
        $customer->save();
    }

    public function expenses($customerId)
    {
        $expenses = $this->expenseRepo->getCustomerTransactions($customerId);

        return view('admin.users.customers.payments.expenses', [
            'expenses' => $expenses
        ]);
    }

    public function regList()
    {
        $regs = $this->regRequestRepo->all();

        return view('admin.users.customers.reg_list', [
            'regs' => $regs
        ]);
    }

    public function deleteReg($id)
    {
        $this->regRequestRepo->destroy($id);
    }


}
