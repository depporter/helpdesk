<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;

// repos
use App\Repos\AdminRepo;
use App\Repos\RoleRepo;
use App\Repos\PermissionGroupRepo;
use App\Repos\TicketCategoryRepo;
use App\Models\TicketStat;

// requests
use App\Http\Requests\Admin\Users\AdminsRequest;

class AdminController extends Controller
{
    protected $adminRepo;
    protected $roleRepo;
    protected $permissionGroupRepo;
    protected $ticketCategoryRepo;
    protected $ticketStats;

    public function __construct(AdminRepo $adminRepo,
                                RoleRepo $roleRepo,
                                PermissionGroupRepo $permissionGroupRepo,
                                TicketCategoryRepo $ticketCategoryRepo,
                                TicketStat $ticketStat)
    {
        $this->adminRepo = $adminRepo;
        $this->roleRepo = $roleRepo;
        $this->permissionGroupRepo = $permissionGroupRepo;
        $this->ticketCategoryRepo = $ticketCategoryRepo;
        $this->ticketStats = $ticketStat;
    }

    public function index(): View
    {
        return view('admin.users.admins.index');
    }

    public function getList(Request $request)
    {
        if ($request->has('filter_full_name'))
        {
            $this->adminRepo->setFilterName($request->input('filter_full_name'));
        }

        if ($request->has('filter_email'))
        {
            $this->adminRepo->setFilterEmail($request->input('filter_email'));
        }

        if ($request->has('filter_date_start') && $request->has('filter_date_end'))
        {
            $this->ticketStats->setDateFrom($request->input('filter_date_start'));
            $this->ticketStats->setDateTo($request->input('filter_date_end'));
        }

        $users = $this->adminRepo->filter();

        foreach ($users as $user)
        {
            $user->tickets_total = $this->ticketStats->getTotalByUser($user->id);
        }


        return view('admin.users.admins.list', [
            'users' => $users
        ]);
    }

    public function create()
    {
        $formAction = route('AdminUsersAdminStore');
        $roles = $this->roleRepo->filter();
        $ticketCategories = $this->ticketCategoryRepo->filter();

        return view('admin.users.admins.modal_content', [
            'formAction' => $formAction,
            'roles' => $roles,
            'ticketCategories' => $ticketCategories
        ]);
    }

    public function store(AdminsRequest $request)
    {

        $administrator = $this->adminRepo->create($request->all());

        if ($request->has('roles'))
        {

            $administrator->roles()->sync($request->input('roles'));
        }

        if ($request->has('ticket_categories'))
        {
            $administrator->ticketCategories()->sync($request->input('ticket_categories'));
        }
    }

    public function edit(int $adminId)
    {
        $formAction = route('AdminUsersAdminUpdate', ['adminId' => $adminId]);
        $roles = $this->roleRepo->filter();
        $admin = $this->adminRepo->getById($adminId, ['roles', 'ticketCategories']);
        $adminRolesIds = $admin->roles->pluck('id')->toArray();
        $ticketCategories = $this->ticketCategoryRepo->filter();
        $adminTicketCategoriesIds = $admin->ticketCategories->pluck('id')->toArray();



        return view('admin.users.admins.modal_content', [
            'formAction' => $formAction,
            'roles' => $roles,
            'user' => $admin,
            'adminRolesIds' => $adminRolesIds,
            'ticketCategories' => $ticketCategories,
            'adminTicketCategoriesIds' => $adminTicketCategoriesIds
        ]);
    }

    public function update(AdminsRequest $request, int $adminId)
    {
        $request->merge([
            'active' => ($request->has('active')) ? 1 : 0,
        ]);


        $admin = $this->adminRepo->getById($adminId);

        if ($admin->super_user)
        {
            $request->merge([
                'active' =>  1,
            ]);
        }

        $admin->update($request->only(['full_name', 'email', 'active']));

        if ($request->has('password'))
        {
            $admin->update($request->only(['password']));
        }

        if ($request->has('roles'))
        {
            $admin->roles()->sync($request->input('roles'));
        } else {
            $admin->roles()->sync([]);
        }

        if ($request->has('ticket_categories'))
        {
            $admin->ticketCategories()->sync($request->input('ticket_categories'));
        } else {
            $admin->ticketCategories()->sync([]);
        }
    }

    public function delete(int $adminId)
    {
        $this->adminRepo->destroy($adminId);
    }

    // roles

    public function roles(): View
    {
        return view('admin.users.admins.roles.index');
    }

    public function rolesGetList()
    {
        $roles = $this->roleRepo->filter();

        return view('admin.users.admins.roles.list', [
            'roles' => $roles
        ]);
    }

    public function rolesCreate()
    {
        $permsGroups = $this->permissionGroupRepo->filter();
        $formAction = route('AdminUsersAdminRolesStore');
        return view('admin.users.admins.roles.modal_content', [
            'permsGroups' => $permsGroups,
            'formAction' => $formAction
        ]);
    }

    public function rolesStore(AdminsRequest $request)
    {
        $role = $this->roleRepo->create($request->all());

        if ($request->has('perms')) {
            $role->permissions()->sync($request->input('perms'));
        }
    }

    public function rolesEdit(int $roleId)
    {
        $permsGroups = $this->permissionGroupRepo->filter();
        $role = $this->roleRepo->getById($roleId);
        $permsIds = $role->permissions()->pluck('id')->toArray();
        $formAction = route('AdminUsersAdminRolesUpdate', ['roleId' => $roleId]);
        return view('admin.users.admins.roles.modal_content', [
            'permsGroups' => $permsGroups,
            'role' => $role,
            'permsIds' => $permsIds,
            'formAction' => $formAction
        ]);
    }

    public function rolesUpdate(AdminsRequest $request, $roleId)
    {
        $this->roleRepo->update($roleId, $request->only(['name']));
        $role = $this->roleRepo->getById($roleId);

        if ($request->has('perms')) {
            $role->permissions()->sync($request->input('perms'));
        } else {
            $role->permissions()->sync([]);
        }
    }
}
