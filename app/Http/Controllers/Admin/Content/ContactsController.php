<?php

namespace App\Http\Controllers\Admin\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Storage;

class ContactsController extends Controller
{
    public function index()
    {
        $contacts = Storage::get('contacts');

        return view('admin.manage.contacts.index', ['contacts' => $contacts]);
    }

    public function update(Request $request)
    {
        if ($request->has('contacts'))
        {
            Storage::put('contacts', $request->input('contacts'));
        }
    }
}
