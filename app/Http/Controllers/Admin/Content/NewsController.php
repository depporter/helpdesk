<?php

namespace App\Http\Controllers\Admin\Content;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repos
use App\Repos\NewsRepo;

// requests
use App\Http\Requests\Admin\NewsRequest;

class NewsController extends Controller
{

    protected $newsRepo;

    public function __construct(NewsRepo $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }

    public function index(): View
    {
        return view('admin.news.index');
    }

    public function getList(): View
    {
        $news = $this->newsRepo->filter();
        return view('admin.news.list', [
            'news' => $news
        ]);
    }

    public function create(): View
    {
        $formAction = route('AdminNewsStore');
        return view('admin.news.modal_content', [
            'formAction' => $formAction
        ]);
    }

    public function store(NewsRequest $request)
    {
        $request->merge([
            'publish' => ($request->has('publish')) ? 1 : 0
        ]);
        $this->newsRepo->create($request->all());
    }

    public function edit(int $newsId): View
    {
        $news = $this->newsRepo->getById($newsId);
        $formAction = route('AdminNewsUpdate', ['newsId' => $newsId]);
        return view('admin.news.modal_content', [
            'item' => $news,
            'formAction' => $formAction
        ]);
    }

    public function update(NewsRequest $request, int $newsId)
    {
        $request->merge([
            'publish' => ($request->has('publish')) ? 1 : 0
        ]);
        $this->newsRepo->update($newsId, $request->only(['title', 'content', 'publish']));
    }

    public function delete(int $newsId)
    {
        $this->newsRepo->destroy($newsId);
    }
}
