<?php

namespace App\Http\Controllers\Admin\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;

// repos
use App\Repos\PageRepo;

use App\Http\Requests\Admin\PagesRequest;

class PagesController extends Controller
{
    protected $pageRepo;

    public function __construct(PageRepo $pageRepo)
    {
        $this->pageRepo = $pageRepo;
    }

    public function index(): View
    {
        return view('admin.pages.index');
    }

    public function getList(): View
    {
        $pages = $this->pageRepo->filter();
        return view('admin.pages.list', [
            'pages' => $pages
        ]);
    }

    public function create(): View
    {
        $formAction = route('AdminPagesStore');
        return view('admin.pages.modal_content', [
            'formAction' => $formAction
        ]);
    }

    public function store(PagesRequest $request)
    {
        $request->merge([
            'publish' => ($request->has('publish')) ? 1 : 0
        ]);
        $this->pageRepo->create($request->all());
    }

    public function edit(int $pageId): View
    {
        $news = $this->pageRepo->getById($pageId);
        $formAction = route('AdminPagesUpdate', ['newsId' => $pageId]);
        return view('admin.pages.modal_content', [
            'item' => $news,
            'formAction' => $formAction
        ]);
    }

    public function update(PagesRequest $request, int $pageId)
    {
        $request->merge([
            'publish' => ($request->has('publish')) ? 1 : 0
        ]);
        $this->pageRepo->update($pageId, $request->only(['title', 'content', 'publish']));
    }

    public function delete(int $pageId)
    {
        $this->pageRepo->destroy($pageId);
    }
}

