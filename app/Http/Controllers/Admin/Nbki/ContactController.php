<?php

namespace App\Http\Controllers\Admin\Nbki;

use App\Repos\CustomerRepo;
use App\Repos\NbkiContactRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    protected $nbkiContactRepo;
    protected $customerRepo;

    public function __construct(NbkiContactRepo $nbkiContactRepo, CustomerRepo $customerRepo)
    {
        $this->nbkiContactRepo = $nbkiContactRepo;
        $this->customerRepo = $customerRepo;
    }

    public function index()
    {
        $customers = $this->customerRepo->getWhoHashCreditRating();

        return view('admin.nbki.contacts.index', [
            'customers' => $customers
        ]);
    }

    public function getList(Request $request)
    {
        if ($request->has('filter_customer_id') && $request->input('filter_customer_id') != 'all')
        {
            $this->nbkiContactRepo->setFilterCustomer($request->input('filter_customer_id'));
        }

        if ($request->has('filter_first_name'))
        {
            $this->nbkiContactRepo->setFilterFirstName($request->input('filter_first_name'));
        }

        if ($request->has('filter_last_name'))
        {
            $this->nbkiContactRepo->setFilterLastName($request->input('filter_last_name'));
        }

        if ($request->has('filter_doc_series'))
        {
            $this->nbkiContactRepo->setFilterDocSeries($request->input('filter_doc_series'));
        }

        if ($request->has('filter_doc_number'))
        {
            $this->nbkiContactRepo->setFilterDocNumber($request->input('filter_doc_number'));
        }

        $results = $this->nbkiContactRepo->filter();

        return view('admin.nbki.contacts.list', [
            'results' => $results
        ]);
    }

    public function result($id)
    {
        $customer = $this->customerRepo->getAuthUser();
        $result = $this->nbkiContactRepo->getCustomerResult($customer->id, $id);

        $response = json_decode($result->response, true);

        return view('admin.nbki.contacts.result', [
            'result' => $result,
            'response' => $response
        ]);
    }

}
