<?php

namespace App\Http\Controllers\Admin\Nbki;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repos
use App\Repos\NbkiCreditRatingRepo;
use App\Repos\CustomerRepo;

use PDF;

class CreditRatingController extends Controller
{
    protected $nbkiCreditRatingRepo;
    protected $customerRepo;

    public function __construct(NbkiCreditRatingRepo $nbkiCreditRatingRepo, CustomerRepo $customerRepo)
    {
        $this->nbkiCreditRatingRepo = $nbkiCreditRatingRepo;
        $this->customerRepo = $customerRepo;
    }

    public function index()
    {
        $customers = $this->customerRepo->getWhoHashCreditRating();
        return view('admin.nbki.credit_rating.index', [
            'customers' => $customers
        ]);
    }

    public function getList(Request $request)
    {
        if ($request->has('filter_customer_id') && $request->input('filter_customer_id') != 'all')
        {
            $this->nbkiCreditRatingRepo->setFilterCustomer($request->input('filter_customer_id'));
        }

        if ($request->has('filter_first_name'))
        {
            $this->nbkiCreditRatingRepo->setFilterFirstName($request->input('filter_first_name'));
        }

        if ($request->has('filter_last_name'))
        {
            $this->nbkiCreditRatingRepo->setFilterLastName($request->input('filter_last_name'));
        }

        if ($request->has('filter_doc_series'))
        {
            $this->nbkiCreditRatingRepo->setFilterDocSeries($request->input('filter_doc_series'));
        }

        if ($request->has('filter_doc_number'))
        {
            $this->nbkiCreditRatingRepo->setFilterDocNumber($request->input('filter_doc_number'));
        }

        $results = $this->nbkiCreditRatingRepo->filter();

        return view('admin.nbki.credit_rating.list', [
            'results' => $results
        ]);
    }

    public function result($id)
    {
        $customer = $this->customerRepo->getAuthUser();
        $result = $this->nbkiCreditRatingRepo->getCustomerResult($customer->id, $id);

        $response = json_decode($result->response, true);

        return view('admin.nbki.credit_rating.result', [
            'result' => $result,
            'response' => $response
        ]);
    }

    public function pdf($id)
    {
        $customer = $this->customerRepo->getAuthUser();
        $result = $this->nbkiCreditRatingRepo->getCustomerResult($customer->id, $id);

        $response = json_decode($result->response, true);

        $pdf = PDF::loadView('admin.nbki.credit_rating.pdf_export', [
            'result' => $result,
            'response' => $response
        ]);
        return $pdf->download($result->first_name . ' ' . $result->last_name . ' - ' . $result->doc_series . $result->doc_number .  '.pdf');
    }
}
