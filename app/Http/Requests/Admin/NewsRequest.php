<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

use App\Http\Requests\MessagesTrait;

class NewsRequest extends FormRequest
{
    use MessagesTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // создание админа
        if ($this->segment(3) == 'news' && $this->segment(4) == 'store')
        {
            return [
                'title' => 'required',
                'content' => 'required',
            ];
        }

        if ($this->segment(3) == 'news' && $this->segment(5) == 'update')
        {
            return [
                'title' => 'required',
                'content' => 'required',
            ];
        }
    }
}
