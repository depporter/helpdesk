<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\MessagesTrait;
use Auth;

class AdminsRequest extends FormRequest
{
    use MessagesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // создание админа
        if ($this->segment(3) == 'admin' && $this->segment(4) == 'store')
        {
            return [
                'full_name' => 'required|string|max:255',
                'email' => 'required|email|max:255|unique:admins,email',
                'password' => 'required|min:3|max:255',
                'roles.*' => 'integer'
            ];
        }

        if ($this->segment(3) == 'admin' && $this->segment(5) == 'update')
        {
            return [
                'full_name' => 'required|string|max:255',
                'email' => 'required|email|max:255|unique:admins,email,' . $this->segment(4),
                'roles.*' => 'integer'
            ];
        }

        // создание роли
        if ($this->segment(4) == 'roles' && $this->segment(5) == 'store')
        {
            return [
                'name' => 'required|string|max:255',
                'perms.*' => 'integer'
            ];
        }

        // обновление роли
        if ($this->segment(4) == 'roles' && $this->segment(6) == 'update')
        {
            return [
                'name' => 'required|string|max:255',
                'perms.*' => 'integer'
            ];
        }

    }
}
