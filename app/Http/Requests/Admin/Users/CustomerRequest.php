<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\MessagesTrait;
use Auth;

class CustomerRequest extends FormRequest
{
    use MessagesTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // создание клиента
        if ($this->segment(3) == 'customer' && $this->segment(4) == 'store')
        {
            return [
                'full_name' => 'required|string|max:255',
                'email' => 'required|email|max:255|unique:customers,email',
                'phone' => 'string',
                'limit.*' => 'required|integer',
                'password' => 'required',
                //'nbki_credit_rating_price' => 'required_with:allow_nbki_credit_rating|integer|min:1',
                //'nbki_contact_actualisation_price' => 'required_with:allow_nbki_contact_actualisation|integer|min:1'
            ];
        }

        // обновление клиента
        if ($this->segment(3) == 'customer' && $this->segment(5) == 'update')
        {

            return [
                'full_name' => 'required|string|max:255',
                'phone' => 'string',
                'limit.*' => 'required|integer',
                'email' => 'required|email|max:255|unique:customers,email,' . $this->segment(4),
                'nbki_credit_rating_price' => 'required_if:allow_nbki_credit_rating,on',
                'nbki_contact_actualisation_price' => 'required_if:allow_nbki_contact_actualisation,on'
            ];
        }

        // создание группы
        if ($this->segment(3) == 'customer' && $this->segment(4) == 'group' && $this->segment(5) == 'store')
        {
            return [
                'name' => 'required',
            ];
        }

        // обновление группы
        if ($this->segment(3) == 'customer' && $this->segment(4) == 'group' && $this->segment(6) == 'update')
        {
            return [
                'name' => 'required',
            ];
        }

        // создание платежа
        if ($this->segment(3) == 'customer' && $this->segment(5) == 'payments' && $this->segment(6) == 'store')
        {
            return [
                'amount' => 'required|integer',
            ];
        }

    }
}
