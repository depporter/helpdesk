<?php

namespace App\Http\Requests\Admin\Catalogs;

use Illuminate\Foundation\Http\FormRequest;

use App\Http\Requests\MessagesTrait;
use Auth;

class TicketCategoryRequest extends FormRequest
{
    use MessagesTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->segment('3') == 'ticket-categories' && $this->segment('4') == 'store')
        {
            return [
                'name' => 'required',
                'type' => 'required',
                'price' => 'required_if:type,by_price|integer|min:1'
            ];
        }

        if($this->segment('3') == 'ticket-categories' && $this->segment('5') == 'update')
        {
            return [
                'name' => 'required',
                'type' => 'required',
                'price' => 'required_if:type,by_price|integer|min:1'
            ];
        }
    }
}
