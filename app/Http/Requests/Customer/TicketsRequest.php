<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\MessagesTrait;

use Auth;

class TicketsRequest extends FormRequest
{
    use MessagesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('customer')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->segment(2) == 'tickets' && $this->segment(3) == 'store')
        {
            return [
                'ticket_category_id' => 'required|integer',
                'title' => 'required',
                'message' => 'required',
                'image.*' => 'file|mimes:jpeg,jpg,bmp,png,zip,rar,pdf'
            ];
        }

        if ($this->segment(2) == 'tickets' && $this->segment(4) == 'update')
        {
            return [
                'title' => 'required',
            ];
        }

        if ($this->segment(2) == 'tickets' && $this->segment(4) == 'upload')
        {
            return [
                'files.*' => 'file|mimes:jpeg,jpg,bmp,png,zip,rar,pdf'
            ];
        }

        if ($this->segment(2) == 'tickets' && $this->segment(4) == 'message' && $this->segment(5) == 'store')
        {
            return [
                'message' => 'required',
            ];
        }

    }
}
