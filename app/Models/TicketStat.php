<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketStat extends Model
{
    protected $table = 'ticket_stats';
    protected $fillable = [
        'user_id',
        'ticket_id'
    ];

    private $dateFrom;
    private $dateTo;


    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = date('Y-m-d', strtotime($dateFrom)) . ' 00:00:00';
        return $this;
    }

    public function setDateTo($dateTo)
    {
        $this->dateTo = date('Y-m-d', strtotime($dateTo)) . ' 23:59:59';
        return $this;
    }



    public function getTotalByUser($userId)
    {

        $q = $this->where('user_id', $userId);

        if ($this->dateFrom && $this->dateTo)
        {

            $q->whereBetween('created_at', [$this->dateFrom, $this->dateTo]);
        }

        return $q->count();
    }
}
