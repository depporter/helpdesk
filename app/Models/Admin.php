<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Admin extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'admins';
    protected $fillable = [
        'email',
        'full_name',
        'password',
        'active',
        'super_user',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Role::class, 'role_users', 'user_id');
    }

    public function ticketCategories()
    {
        return $this->belongsToMany(\App\Models\TicketCategory::class, 'ticket_category_users', 'user_id', 'category_id');
    }

    public function rolesDevideByComma()
    {
        $rolesArray = $this->roles->pluck('name')->toArray();

        return implode(', ', $rolesArray);
    }

    public function isSuperUser(): string
    {
        return ($this->super_user) ? '<label class="label label-danger">Да</label>' : '<label class="label label-default">Нет</label>';
    }

    public function isActive(): string
    {
        return ($this->active) ? '<label class="label label-success">Да</label>' : '<label class="label label-danger">Нет</label>';
    }
}
