<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopUp extends Model
{
    protected $table = 'top_ups';
    protected $fillable = [
        'admin_id',
        'customer_id',
        'amount',
        'desc'
    ];

    public function admin()
    {
        return $this->hasOne(\App\Models\Admin::class, 'id', 'admin_id');
    }
}
