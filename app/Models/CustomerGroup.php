<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
    protected $table = 'customer_groups';
    protected $fillable = ['name'];

    public function customers()
    {
        return $this->belongsToMany(\App\Models\Customer::class, 'customer_group', 'group_id', 'customer_id');
    }
}
