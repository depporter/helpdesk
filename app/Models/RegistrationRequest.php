<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegistrationRequest extends Model
{
    protected $table = 'registration_requests';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'services',
    ];

    public function formatServices()
    {
        $data = json_decode($this->services);


        $str = [];

        foreach ($data as $val)
        {
            $str[] = config('project.services.' . $val);
        }

        return (is_array($data)) ? implode(", ", $str) : '--';
    }
}
