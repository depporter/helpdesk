<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RuDateTrait;

class Ticket extends Model
{
    use RuDateTrait;

    protected $table = 'tickets';
    protected $fillable = [
        'creator',
        'customer_id',
        'support_id',
        'ticket_category_id',
        'title',
        'customer_unread_messages',
        'user_unread_messages',
        'customer_archived',
        'support_archived',
        'assigned_at',
        'support_archived_at'
    ];

    public function messages()
    {
        return $this->hasMany(\App\Models\TicketMessage::class, 'ticket_id', 'id');
    }

    public function customerFiles()
    {
        return $this->hasMany(\App\Models\TicketAttachment::class, 'ticket_id', 'id')->where('author', 'customer');
    }

    public function category()
    {
        return $this->hasOne(\App\Models\TicketCategory::class, 'id', 'ticket_category_id')->withTrashed();
    }

    public function supportFiles()
    {
        return $this->hasMany(\App\Models\TicketAttachment::class, 'ticket_id', 'id')->where('author', 'support');
    }

    public function customer()
    {
        return $this->hasOne(\App\Models\Customer::class, 'id', 'customer_id')->withTrashed();
    }

    public function support()
    {
        return $this->hasOne(\App\Models\Admin::class, 'id', 'support_id')->withTrashed();
    }

    public function hasAnswers($type = 'customer')
    {
        if ($type == 'customer')
        {
            return ($this->customer_unread_messages > 0) ? '<label class="label label-danger">Да</label>' : '<label class="label label-default">Нет</label>';
        }

        if ($type == 'operator')
        {
            return ($this->user_unread_messages > 0) ? '<label class="label label-danger">Да</label>' : '<label class="label label-default">Нет</label>';
        }

    }

    public function getTicketStatus()
    {
        if (!$this->support_id)
        {
            return '<label class="label label-danger">Не назначен</label>';
        }

        if ($this->support_id && !$this->support_archived)
        {
            return '<label class="label label-warning">В работе</label>';
        }

        if ($this->support_archived)
        {
            return '<label class="label label-success">Завершен</label>';
        }
    }


}
