<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\RuDateTrait;

class News extends Model
{
    use RuDateTrait;

    protected $table = 'news';
    protected $fillable = [
        'title',
        'content',
        'publish'
    ];

    public function isPublish()
    {
        return ($this->publish) ? '<label class="label label-success">Да</label>' : '<label class="label label-danger">Нет</label>';
    }
}
