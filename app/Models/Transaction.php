<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RuDateTrait;

class Transaction extends Model
{
    use RuDateTrait;
    protected $table = 'transactions';
    protected $fillable = [
        'customer_id',
        'creator_id',
        'type',
        'total',
        'comment',
    ];

    public function creator()
    {
        return $this->hasOne(\App\Models\Admin::class, 'id', 'creator_id');
    }

    public function customer()
    {
        return $this->hasOne(\App\Models\Customer::class, 'id', 'customer_id');
    }

    public function formatTotal()
    {
        return number_format($this->total, 0, '.', ' ');
    }

    public function getType()
    {
        switch ($this->type)
        {
            case 'topup':
                return 'Пополнение';
                break;

            case 'expense':
                return 'Трата';
                break;

            case 'correction':
                return 'Корректировка';
                break;
        }
    }
}
