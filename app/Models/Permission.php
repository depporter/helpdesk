<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $fillable = [
        'group_id',
        'name',
        'alias',
        'desc'
    ];

    protected $casts = [
        'group_id' => 'integer',
        'name' => 'string',
        'alias' => 'string',
        'desc' => 'string',
    ];
}
