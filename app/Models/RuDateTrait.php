<?php namespace App\Models;

use Carbon\Carbon;

trait RuDateTrait {
    protected $ruValues = [
        'Jan' => 'Янв',
        'Feb' => 'Фев',
        'Mar' => 'Мар',
        'Apr' => 'Апр',
        'May' => 'Май',
        'Jun' => 'Июн',
        'Jul' => 'Июл',
        'Aug' => 'Авг',
        'Sep' => 'Сен',
        'Oct' => 'Окт',
        'Nov' => 'Ноя',
        'Dec' => 'Дек',
        'January' => 'Январь',
        'February' => 'Февраль',
        'March' => 'Март',
        'April' => 'Апрель',
        'May' => 'Май',
        'June' => 'Июнь',
        'July' => 'Июль',
        'August' => 'Август',
        'September' => 'Сентябрь',
        'October' => 'Октябрь',
        'November' => 'Ноябрь',
        'December' => 'Декабрь',
        'Sun' => 'Вс',
        'Mon' => 'Пн',
        'Tue' => 'Вт',
        'Wed' => 'Ср',
        'Thu' => 'Чт',
        'Fri' => 'Пт',
        'Sat' => 'Сб',
        'Sunday' => 'Воскресенье',
        'Monday' => 'Понедельник',
        'Tuesday' => 'Вторник',
        'Wednesday' => 'Среда',
        'Thursday' => 'Четверг',
        'Friday' => 'Пятница',
        'Saturday' => 'Суббота',

    ];

    public function getRuCreatedAt(string $format = "j M Y г. в H:i"): string
    {


        //return $this->presenterInstance->created_at;
        $date = new Carbon($this->created_at);

        return strtr($date->format($format), $this->ruValues);
    }

    public function getRuAssignedAt(string $format = "j M Y г. в H:i"): string
    {

        if (!$this->assigned_at)
        {
            return '--';
        }

        $date = new Carbon($this->assigned_at);

        return strtr($date->format($format), $this->ruValues);
    }

    public function getRuSupportArchivedAt(string $format = "j M Y г. в H:i"): string
    {
        if (!$this->support_archived_at)
        {
            return '--';
        }

        $date = new Carbon($this->support_archived_at);

        return strtr($date->format($format), $this->ruValues);
    }

    public function getRuUpdatedAt(string $format = "j M Y г. в H:i"): string
    {

        //return $this->presenterInstance->created_at;
        $date = new Carbon($this->created_at);

        return strtr($date->format($format), $this->ruValues);
    }

    public function getRuDate(string $date, string $format = "j M Y")
    {
        $date = new Carbon($date);

        return strtr($date->format($format), $this->ruValues);
    }
}