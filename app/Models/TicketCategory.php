<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketCategory extends Model
{
    use SoftDeletes;

    protected $table = 'ticket_categories';
    protected $fillable = ['name', 'title_hint', 'desc_hint', 'type', 'price'];

    public function users()
    {
        return $this->belongsToMany(\App\Models\Admin::class, 'ticket_category_users', 'category_id', 'user_id');
    }

    public function clients()
    {
        return $this->belongsToMany(\App\Models\Customer::class, 'ticket_category_customers', 'customer_id', 'category_id');
    }


    public function usersDevidedByComma()
    {
        $usersArray = $this->users->pluck('full_name')->toArray();

        return implode(', ', $usersArray);
    }

    public function clientsDevidedByComma()
    {
        $usersArray = $this->clients->pluck('full_name')->toArray();

        return implode(', ', $usersArray);
    }

    public function getType()
    {
        $types = config('project.ticketCategoryTypes');

        return (isset($types[$this->type])) ? $types[$this->type] : '--';
    }

    public function getPrice()
    {
        if ($this->type == 'by_price')
        {
            return $this->price;
        }

        return '--';
    }
}
