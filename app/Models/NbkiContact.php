<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NbkiContact extends Model
{
    protected $table = 'nbki_contacts';

    protected $fillable = [
        'customer_id',
        'request_id',
        'first_name',
        'last_name',
        'middle_name',
        'date_birth',
        'excluded_phones',
        'mode',
        'months',
        'limit',
        'doc_type',
        'doc_number',
        'doc_series',
        'doc_issue_date',
        'response',
        'response_status',
        'cost',
        'type'
    ];

    public function customer()
    {
        return $this->hasOne(\App\Models\Customer::class, 'id', 'customer_id');
    }
}
