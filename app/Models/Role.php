<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = [
        'name'
    ];
    protected $casts = [
        'name' => 'string'
    ];

    public function permissions()
    {
        return $this->belongsToMany(\App\Models\Permission::class, 'role_permissions', 'role_id', 'permission_id');
    }

    public function permsToList()
    {
        $permsNames =  $this->permissions()->pluck('name');

        $list = "<ul class=\"list-group\">";
        foreach ($permsNames as $name)
        {
            $list .= "<li class=\"list-group-item\">$name</li>";
        }
        $list .= "</ul>";

        return $list;
    }
}
