<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketAttachment extends Model
{
    protected $table = 'ticket_attachments';
    protected $fillable = [
        'ticket_id',
        'user_id',
        'author',
        'client_file_name',
        'orig_file_name',
    ];
}
