<?php

namespace App\Models;

use App\Models\DateTrait;
use App\Repos\NbkiContactRepo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\RuDateTrait;

use App\Repos\NbkiCreditRatingRepo;
use App\Models\NbkiCreditRating;

class Customer extends Authenticatable
{
    use SoftDeletes;
    use RuDateTrait;

    protected $table = 'customers';
    protected $fillable = [
        'email',
        'password',
        'full_name',
        'active',
        'company',
        'phone',
        'credit_up_to',
        'allow_work_on_credit',
        'nbki_credit_rating_package_amount',
        'nbki_credit_rating_query_price',
        'nbki_credit_rating_type',
        'actualization_contacts_package_amount',
        'actualization_contacts_query_price',
        'actualization_contacts_type',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getAllowWorkOnCreditAttribute($value)
    {
        return ($value) ? 'Да' : 'Нет';
    }

    public function ticketCategories()
    {
        return $this->belongsToMany(\App\Models\TicketCategory::class, 'ticket_category_customers', 'customer_id', 'category_id')->withPivot('ticket_limit', 'countable')->orderBy('type');
    }

    public function groups()
    {
        return $this->belongsToMany(\App\Models\CustomerGroup::class, 'customer_group', 'customer_id', 'group_id');
    }

    public function isActive()
    {
        return ($this->active) ? '<i style="color:green" class="fa fa-power-off" aria-hidden="true"></i>' : '<i style="color:orangered" class="fa fa-power-off" aria-hidden="true"></i>';
    }

    public function getLimitsList()
    {
        $list = '<ul class="list-group">';

        foreach ($this->ticketCategories as $category)
        {

            if ($category->type == 'limit_monthly')
            {
                $rest = $category->pivot->ticket_limit - $category->pivot->thisMonthCreated;
                $list .= '<li class="list-group-item"><span class="badge bg-red">Лимит: ' . $category->pivot->ticket_limit .' в мес. Ост. '  . $rest . '</span>'. $category->name .'</li>';
            }

            if ($category->type == 'limit_countable')
            {
                $list .= '<li class="list-group-item"><span class="badge bg-green">Осталось: '. $category->pivot->ticket_limit . '</span>'. $category->name .'</li>';
            }

            if ($category->type == 'by_price')
            {
                $list .= '<li class="list-group-item"><span class="badge bg-blue">'. $category->price . ' р.</span>'. $category->name .'</li>';
            }
        }

        if ($this->nbki_credit_rating_type != 'off')
        {
            $type = '';

            if ($this->nbki_credit_rating_type == 'query')
            {
                $type = 'Кредитный рейтинг. <span class="badge bg-orange">Запрос ' . $this->nbki_credit_rating_query_price . ' р.</span>' ;
            }

            if ($this->nbki_credit_rating_type == 'package')
            {
                $model = new NbkiCreditRating();
                $nbki = new NbkiCreditRatingRepo($model);
                $createdQueries = $nbki->queriesTotalCreatedThisMonth($this->id);
                $rest = $this->nbki_credit_rating_package_amount - $createdQueries;
                $type = 'Кредитный рейтинг. <span class="badge bg-orange">Лимит: ' . $this->nbki_credit_rating_package_amount . '. Осталось:' . $rest .  '</span>' ;
            }

            $list .= '<li class="list-group-item list-group-item-success">' . $type . '</li>';
        }

        if ($this->actualization_contacts_type != 'off')
        {
            $type = '';

            if ($this->actualization_contacts_type == 'query')
            {
                $type = 'Кредитный рейтинг. <span class="badge bg-orange">Запрос ' . $this->actualization_contacts_query_price . ' р.</span>' ;
            }

            if ($this->actualization_contacts_type == 'package')
            {
                $model = new NbkiContact();
                $nbki = new NbkiContactRepo($model);
                $createdQueries = $nbki->queriesTotalCreatedThisMonth($this->id);
                $rest = $this->actualization_contacts_package_amount - $createdQueries;
                $type = 'Кредитный рейтинг. <span class="badge bg-orange">Лимит: ' . $this->actualization_contacts_package_amount . '. Осталось:' . $rest .  '</span>' ;
            }

            $list .= '<li class="list-group-item list-group-item-success">' . $type . '</li>';
        }

        $list .= '</ul>';

        return $list;
    }
}
