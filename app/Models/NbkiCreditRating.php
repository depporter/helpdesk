<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NbkiCreditRating extends Model
{
    protected $table = 'nbki_credit_ratings';

    protected $fillable = [
        'customer_id',
        'request_id',
        'type',
        'first_name',
        'last_name',
        'middle_name',
        'date_birth',
        'doc_type',
        'doc_number',
        'doc_series',
        'doc_issue_date',
        'loan_type',
        'loan_amount',
        'loan_duration',
        'response',
        'response_status',
        'response_code',
        'cost'
    ];

    public function customer()
    {
        return $this->hasOne(\App\Models\Customer::class, 'id', 'customer_id');
    }
}
