<?php namespace App\Repos;

use App\Models\TicketCategory;
use Illuminate\Database\Eloquent\Collection;




class TicketCategoryRepo extends Repository
{

    public function __construct(TicketCategory $model)
    {
        $this->model = $model;
    }

    public function filter()
    {
        return $this->model->with('users')->orderBy('type')->get();
    }

}