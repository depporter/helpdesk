<?php namespace App\Repos;

use App\Models\CustomerGroup;

class CustomerGroupRepo extends Repository
{
    private $filterName;
    private $filterEmail;
    private $filterCompany;
    private $filterPhone;
    private $hasFilter = false;

    public function __construct(CustomerGroup $model)
    {
        $this->model = $model;
    }

    public function setFilterName($filterName)
    {
        $this->filterName = $filterName;
        $this->hasFilter = true;
        return $this;
    }


    public function setFilterEmail($filterEmail)
    {
        $this->filterEmail = $filterEmail;
        $this->hasFilter = true;
        return $this;
    }

    public function setFilterCompany($filterCompany)
    {
        $this->filterCompany = $filterCompany;
        $this->hasFilter = true;
        return $this;
    }

    public function setFilterPhone($filterPhone)
    {
        $this->filterPhone = $filterPhone;
        $this->hasFilter = true;
        return $this;
    }

    public function filter()
    {
        return $this->model->withCount('customers')->get();
    }

    public function getCustomers($groupId)
    {
        if ($this->hasFilter)
        {

            $filterName = $this->filterName;
            $filterEmail = $this->filterEmail;
            $filterPhone = $this->filterPhone;
            $filterCompany = $this->filterCompany;
            $group = $this->model->where('id', $groupId)->with(['customers' => function ($q) use ($filterName, $filterEmail, $filterPhone, $filterCompany) {

                if ($filterEmail)
                {

                    $q->where('email', 'like', "%$filterEmail%");
                }

                if ($filterName)
                {
                    $q->where('full_name', 'like', "%$filterName%");
                }

                if ($filterCompany)
                {
                    $q->where('company', 'like', "%$filterCompany%");
                }

                if ($filterPhone)
                {
                    $q->where('phone', 'like', "%$filterPhone%");
                }
            }, 'ticketCategories'])->first();

        } else {
            $group = $this->model->with(['customers'])->where('id', $groupId)->first();
        }


        return $group->customers;
    }

    public function getDefaultGroupId()
    {
        $group =  $this->model->firstOrCreate(['name' => 'Default']);

        return $group->id;
    }

}