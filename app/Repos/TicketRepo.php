<?php namespace App\Repos;

use App\Models\Ticket;
use Illuminate\Database\Eloquent\Collection;


use Carbon\Carbon;
use DB;

class TicketRepo extends Repository
{

    private $supportId;
    private $customerId;
    private $categoryId;
    private $isArchived;

    public function __construct(Ticket $model)
    {
        $this->model = $model;
    }

    public function setSupportId($supportId)
    {
        $this->supportId = $supportId;
        return $this;
    }

    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    public function setIsArchived($isArchived)
    {
        $this->isArchived = $isArchived;
        return $this;
    }


    public function filter()
    {
        $res = $this->model->with(['category', 'customer', 'support'])->orderBy('created_at', 'desc');

        if ($this->categoryId)
        {
            $res->where('ticket_category_id', $this->categoryId);
        }

        if ($this->customerId)
        {
            $res->where('customer_id', $this->customerId);
        }

        if ($this->supportId)
        {
            $res->where('support_id', $this->supportId);
        }

        if ($this->isArchived !== null)
        {
            $res->where('support_archived', $this->isArchived);
        }



        return $res->get();
    }

    public function getNobody()
    {
        return $this->model->with(['category', 'customer'])->where('support_id', null)->orderBy('created_at', 'desc')->get();
    }

    public function getAssigned()
    {
        return $this->model->with(['support', 'customer'])->where('support_id', '!=', null)->where('support_archived', 0)->orderBy('created_at', 'desc')->get();
    }

    public function getOperatorNobody(array $allowedCategories)
    {
        return $this->model->with('category')->where('support_id', null)->whereIn('ticket_category_id', $allowedCategories)->orderBy('created_at', 'desc')->get();
    }

    public function getOperatorAssigned($operatorId)
    {
        return $this->model->with('category')->where('support_id', $operatorId)->where('support_archived', 0)->orderBy('created_at', 'desc')->get();
    }

    public function getOperatorArchived($operatorId)
    {
        return $this->model->with('category')->where('support_id', $operatorId)->where('support_archived', 1)->orderBy('created_at', 'desc')->paginate(50);
    }

    /**
     * В качестве ключа возращает айди категории тикета
     * В качестве значения возвращает кол-во созданных тикетов
     * @param int $customerId
     * @return array
     */
    public function ticketCategoryTotalCreatedThisMonth(int $customerId): array
    {
        $dt = Carbon::now();
        $results = $this->model->select(DB::raw('ticket_category_id, count(*) as total'))->where('created_at', '>=', $dt->startOfMonth())->where('customer_id', $customerId)->groupBy('ticket_category_id')->get();

        $categoryTicketsTotal = [];

        foreach ($results as $item)
        {
            $categoryTicketsTotal[$item->ticket_category_id] = $item->total;
        }

        return  $categoryTicketsTotal;
    }

    public function canCustomerCreate(int $customerId, int $ticketCategoryId, $categoryLimit)
    {
        $ticketsByCategory = $this->ticketCategoryTotalCreatedThisMonth($customerId);


        if (isset($ticketsByCategory[$ticketCategoryId]) && $ticketsByCategory[$ticketCategoryId] >= $categoryLimit)
        {
           // dd('limit');
            return false;
        }

        return true;
    }

    public function getCustomerActual(int $customerId)
    {
        return $this->model->with('category')->where('customer_id', $customerId)->where('customer_archived', 0)->where('support_archived', 0)->orderBy('created_at', 'desc')->get();
    }

    public function getCustomerDone(int $customerId)
    {
        return $this->model->with('category')->where('customer_id', $customerId)->where('customer_archived', 0)->where('support_archived', 1)->orderBy('created_at', 'desc')->get();
    }

    public function getCustomerArchive(int $customerId)
    {
        return $this->model->with('category')->where('customer_id', $customerId)->where('customer_archived', 1)->orderBy('created_at', 'desc')->paginate(50);
    }

    public function getCustomersWithAnswers(int $customerId)
    {
        return $this->model->with('category')->where('customer_id', $customerId)->where('customer_unread_messages', '>', 0)->orderBy('created_at', 'desc')->get();
    }

    public function getForArchiving()
    {

        return $this->model
            ->with('supportFiles')
            ->where('system_archived', 0)
            ->where('customer_archived', 1)
            ->where('customer_archived', '<=', Carbon::now()->subDays(14)->toDateTimeString())->get();
    }

}