<?php namespace App\Repos;

use App\Models\TicketAttachment;
use Illuminate\Http\UploadedFile;

class TicketAttachmentRepo extends Repository
{
    public function __construct(TicketAttachment $model)
    {
        $this->model = $model;
    }

    public function attach(int $ticketId, int $userId, string $userType, UploadedFile $file)
    {
        $uploadPath = storage_path() . '/app/public/tickets_media';

        if ($file->isValid()) {
            $clientName = $file->getClientOriginalName();
            $origName = $ticketId . '_' . uniqid() . '.' . $file->guessClientExtension();

            $file->move($uploadPath, $origName);

            $this->model->create([
                'ticket_id' => $ticketId,
                'user_id' => $userId,
                'author' => $userType,
                'client_file_name' => $clientName,
                'orig_file_name' => $origName
            ]);

        }
    }

    public function delete(int $ticketId, int $fileId)
    {
        $file = $this->getFirstByWhere(['ticket_id' => $ticketId, 'id' => $fileId]);

        if ($file)
        {
            $filePath = storage_path() . '/app/public/tickets_media/' . $file->orig_file_name;
            if (file_exists($filePath))
            {
                @unlink($filePath);
            }

            $file->delete();
        }
    }

    public function getSupportFiles(int $ticketId)
    {
        return $this->model->where('ticket_id', $ticketId)->where('author', 'support')->get();


    }

}