<?php namespace App\Repos;

use App\Models\PermissionGroup;

class PermissionGroupRepo extends Repository
{
    public function __construct(PermissionGroup $model)
    {
        $this->model = $model;
    }

    public function filter()
    {
        return $this->model->with('permissions')->orderBy('name')->get();
    }

}