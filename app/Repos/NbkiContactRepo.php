<?php namespace App\Repos;

use App\Models\NbkiContact;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
class NbkiContactRepo extends Repository
{
    private $filterCustomer;
    private $filterFirstName;
    private $filterLastName;
    private $filterDocSeries;
    private $filterDocNumber;

    public function __construct(NbkiContact $model)
    {
        $this->model = $model;
    }

    public function setFilterCustomer($filterCustomer)
    {
        $this->filterCustomer = $filterCustomer;
        return $this;
    }

    public function setFilterFirstName($filterFirstName)
    {
        $this->filterFirstName = $filterFirstName;
        return $this;
    }

    public function setFilterLastName($filterLastName)
    {
        $this->filterLastName = $filterLastName;
        return $this;
    }

    public function setFilterDocSeries($filterDocSeries)
    {
        $this->filterDocSeries = $filterDocSeries;
        return $this;
    }

    public function setFilterDocNumber($filterDocNumber)
    {
        $this->filterDocNumber = $filterDocNumber;
        return $this;
    }

    public function filter()
    {
        $q = $this->model->with('customer')->orderBy('created_at', 'desc');

        if ($this->filterCustomer)
        {
            $q->where('customer_id', $this->filterCustomer);
        }

        if ($this->filterFirstName)
        {
            $q->where('first_name', 'like', "%$this->filterFirstName%" );
        }

        if ($this->filterLastName)
        {
            $q->where('last_name', 'like', "%$this->filterLastName%" );
        }

        if ($this->filterDocSeries)
        {
            $q->where('doc_series', $this->filterDocSeries );
        }

        if ($this->filterDocNumber)
        {
            $q->where('doc_number', $this->filterDocNumber);
        }

        return $q->paginate(50);
    }

    public function getCustomerResult($customerId, $id)
    {
        return $this->model->where('customer_id', $customerId)->where('id', $id)->first();
    }

    public function queriesTotalCreatedThisMonth($customerId)
    {
        $dt = Carbon::now();
        $results = $this->model
            ->where('created_at', '>=',  $dt->startOfMonth())
            ->where('customer_id', $customerId)
            ->where('type', 'package')->count();

        return $results;
    }

}