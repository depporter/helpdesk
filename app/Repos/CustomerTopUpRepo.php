<?php namespace App\Repos;

use App\Models\TopUp;

class CustomerTopUpRepo extends Repository
{
    public function __construct(TopUp $model)
    {
        $this->model = $model;
    }

    public function getCustomerTransactions($customerId)
    {
        return $this->model->where(['customer_id' => $customerId])->orderBy('created_at', 'desc')->get();
    }

}