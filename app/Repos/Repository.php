<?php namespace App\Repos;


abstract class Repository
{

    /**
     * The Model instance.
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;



    /**
     * Destroy a model.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        $this->getById($id)->delete();
    }

    /**
     * Get Model by id.
     *
     * @param  int $id
     * @return App\Models\Model
     */
    public function getById(int $id, array $relations = [])
    {
        if (count($relations))
        {
            return $this->model->with($relations)->findOrFail($id);
        }
        return $this->model->findOrFail($id);
    }

    public function getByIds(array $ids)
    {
        return $this->model->whereIn('id', $ids)->get();
    }

    /**
     * Create model
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function all($relations = false)
    {
        if (is_array($relations))
        {
            return $this->model->with($relations)->get();
        }
        return $this->model->all();
    }

    public function update($id, $data)
    {
        return $this->getById($id)->update($data);
    }

    public function updateByWhere(array $where, array $updateData)
    {
        return $this->model->where($where)->update($updateData);
    }

    public function destroyByWhere(array $where)
    {
        return $this->model->where($where)->delete();
    }

    public function getFirstByIdAndWhere($id, array $where, $relations = [])
    {
        $res = $this->model->where('id', $id)->where($where);

        if (count($relations))
        {
            $res->with($relations);
        }
        return $res->first();
    }

    public function getFirstByWhere(array $where)
    {
        return $this->model->where($where)->first();
    }

    public function updateByIdAndWhere($id, array $where, array $data)
    {
        $this->model->where('id', $id)->where($where)->update($data);
    }

    public function destroyByIdAndWhere($id, array $where)
    {
        $this->model->where('id', $id)->where($where)->delete();
    }




}