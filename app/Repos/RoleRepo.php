<?php namespace App\Repos;

use App\Models\Role;

class RoleRepo extends Repository
{
    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function filter()
    {
        return $this->model->orderBy('name')->get();
    }

}