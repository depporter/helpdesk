<?php namespace App\Repos;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Collection;
use Auth;



class CustomerRepo extends Repository
{

    private $filterName;
    private $filterEmail;
    private $filterCompany;
    private $filterPhone;

    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    public function setFilterName($filterName)
    {
        $this->filterName = $filterName;
        return $this;
    }


    public function setFilterEmail($filterEmail)
    {
        $this->filterEmail = $filterEmail;
        return $this;
    }

    public function setFilterCompany($filterCompany)
    {
        $this->filterCompany = $filterCompany;
        return $this;
    }

    public function setFilterPhone($filterPhone)
    {
        $this->filterPhone = $filterPhone;
        return $this;
    }


    public function filter()
    {
        $res = $this->model->with(['ticketCategories'])->orderBy('active', 'desc')->orderBy('created_at', 'desc');

        if ($this->filterEmail)
        {
            $res->where('email', 'like', "%$this->filterEmail%");
        }

        if ($this->filterName)
        {
            $res->where('full_name', 'like', "%$this->filterName%");
        }

        if ($this->filterCompany)
        {
            $res->where('company', 'like', "%$this->filterCompany%");
        }

        if ($this->filterPhone)
        {
            $res->where('phone', 'like', "%$this->filterPhone%");
        }

        return $res->paginate(50);
    }

    public function getFullName(): string
    {
        return (Auth::guard('customer')->check()) ? Auth::guard('customer')->user()->full_name : '';
    }

    public function getAvatar(): string
    {
        return (Auth::guard('customer')->check() && Auth::guard('customer')->user()->avatar) ? Auth::guard('customer')->user()->avatar : asset('assets/app/customer/images/user-placeholder-160x160.gif');
    }

    public function getBalance(): string
    {
        return (Auth::guard('customer')->check()) ? Auth::guard('customer')->user()->balance : 0;
    }

    public function getId()
    {
        return (Auth::guard('customer')->check()) ? Auth::guard('customer')->id() : null;
    }

    public function getAuthUser()
    {
        return Auth::guard('customer')->user();
    }

    public function checkTicketCategoryAssigned(Customer $customer, $ticketCategoryId)
    {
        $ticketCategoriesIds = $customer->ticketCategories->pluck('id')->toArray();

        return in_array($ticketCategoryId, $ticketCategoriesIds);
    }


    public function getTicketCategoryLimit(Customer $customer, int $categoryId)
    {
        // проверим сначала есть ли такая категория у кастомера
        if (!$this->checkTicketCategoryAssigned($customer, $categoryId))
        {
            return 0;
        }



        foreach ($customer->ticketCategories as $category)
        {
            if ($category->id == $categoryId)
            {
                return $category;
            }
        }

        return 0;
    }

    public function getWhoHashCreditRating()
    {
        return $this->model->where('allow_work_on_credit', 1)->get();
    }

}