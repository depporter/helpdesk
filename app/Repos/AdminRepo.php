<?php namespace App\Repos;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Collection;
use Auth;



class AdminRepo extends Repository
{
    public $user;
    private $filterName;
    private $filterEmail;

    public function __construct(Admin $model)
    {
        $this->model = $model;
        $this->user = Auth::guard('admin')->user();
    }

    public function setFilterName($filterName)
    {
        $this->filterName = $filterName;
        return $this;
    }

    public function setFilterEmail($filterEmail)
    {
        $this->filterEmail = $filterEmail;
        return $this;
    }


    public function filter()
    {
        $res = $this->model->with('roles')->orderBy('super_user', 'desc')->orderBy('active', 'desc');

        if ($this->filterName)
        {
            $res->where('full_name', 'like', "%$this->filterName%");
        }

        if ($this->filterEmail)
        {
            $res->where('email', 'like', "%$this->filterEmail%");
        }

        return $res->paginate(50);

    }

    public function getFullName(): string
    {
        return (Auth::guard('admin')->check()) ? Auth::guard('admin')->user()->full_name : '';
    }

    public function getAvatar(): string
    {
        return (Auth::guard('admin')->check() && Auth::guard('admin')->user()->avatar) ? Auth::guard('admin')->user()->avatar : asset('assets/app/admin/images/user-placeholder-160x160.gif');
    }

    public function getAuthUserId(): int
    {
        return Auth::guard('admin')->id();
    }

    public function getAuthUser()
    {
        return Auth::guard('admin')->user();
    }

    public function permissions(int $userId): array
    {
        $user = $this->getById($userId, ['roles', 'roles.permissions']);

        return $this->extractPerms($user);
    }

    // извлечение пермишенов из инстанса юзера
    private function extractPerms($user)
    {
        $perms = [];


        foreach ($user->roles as $role)
        {
            foreach ($role->permissions as $permission)
            {
                $perms[] = $permission->alias;
            }
        }

        return array_unique($perms);
    }

    public function can(string $permission): bool
    {
        if ($this->user->super_user)
        {
            return true;
        }

        $perms =  $this->permissionsByUser($this->user);

        return in_array($permission, $perms);
    }

    public function usersCan(string $permission)
    {
        $users = $this->all();

        foreach ($users as $key => $user)
        {
            $perms = $this->extractPerms($user);

           if(!in_array($permission, $perms))
           {
               unset($users[$key]);
           }
        }

        return $users;
    }

    // получение пермишено из инстанса юзера
    public function permissionsByUser(Admin $user)
    {
        return $this->extractPerms($user);
    }

    public function getAllowedTicketCategoriesIds()
    {
        $user = $this->getById($this->getAuthUserId(), ['ticketCategories']);

        return $user->ticketCategories->pluck('id')->toArray();
    }

    public function isSuperUser()
    {
        return (boolean) $this->user->super_user;
    }

    public function getSuperUsers()
    {
        return $this->model->where('super_user', 1)->get();
    }
}