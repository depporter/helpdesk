<?php namespace App\Repos;


use App\Models\RegistrationRequest;
use Illuminate\Database\Eloquent\Collection;


class RegRequestRepo extends Repository
{

    public function __construct(RegistrationRequest $model)
    {
        $this->model = $model;
    }

}