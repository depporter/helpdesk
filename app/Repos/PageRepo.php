<?php namespace App\Repos;


use App\Models\Page;
use Illuminate\Database\Eloquent\Collection;


class PageRepo extends Repository
{
    private $filterPublish;

    public function __construct(Page $model)
    {
        $this->model = $model;
    }

    public function setFilterPublish($filterPublish)
    {
        $this->filterPublish = $filterPublish;
        return $this;
    }


    public function filter()
    {
        $res = $this->model->orderBy('created_at', 'desc');
        if ($this->filterPublish !== null)
        {
            $res->where('publish', $this->filterPublish);
        }

        return $res->get();
    }

}