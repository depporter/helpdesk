<?php namespace App\Repos;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;




class TransactionRepo extends Repository
{

    private $filterCustomerId;
    private $filterCreatorId;
    private $filterNeedPaginate = true;

    public function __construct(Transaction $model)
    {
        $this->model = $model;
    }

    public function setFilterCustomerId($filterCustomerId)
    {
        $this->filterCustomerId = $filterCustomerId;
        return $this;
    }

    public function setFilterCreatorId($filterCreatorId)
    {
        $this->filterCreatorId = $filterCreatorId;
        return $this;
    }

    public function setFilterNeedPaginate(bool $filterNeedPaginate)
    {
        $this->filterNeedPaginate = $filterNeedPaginate;
        return $this;
    }

    public function filter(): Collection
    {
        $res = $this->model->with('customer', 'creator')->orderBy('created_at', 'desc');

        if ($this->filterCreatorId)
        {
            $res->where('creator_id', $this->filterCreatorId);
        }

        if ($this->filterCustomerId)
        {
            $res->where('customer_id', $this->filterCustomerId);
        }

        if ($this->filterNeedPaginate)
        {
            return $res->paginate(50);
        }

        return $res->get();


    }



}