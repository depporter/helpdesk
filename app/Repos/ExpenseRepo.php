<?php namespace App\Repos;

use App\Models\Expense;

class ExpenseRepo extends Repository
{
    public function __construct(Expense $model)
    {
        $this->model = $model;
    }

    public function getCustomerTransactions($customerId)
    {
        return $this->model->where(['customer_id' => $customerId])->orderBy('created_at', 'desc')->get();
    }

}