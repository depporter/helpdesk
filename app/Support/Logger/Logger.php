<?php

namespace App\Support\Logger;

use App\Models\ActivityLog;
use Auth;

class Logger
{

    private $model;
    protected $type;

    public function __construct()
    {
        $this->model = new ActivityLog;
    }

    public function autoLog(string $model, string $action, array $modelData): ActivityLog
    {
        $log = $this->model->create([
            'user_id' => Auth::guard('admin')->id(),
            'type' => 'auto',
            'model' => $model,
            'model_id' => $modelData['id'],
            'action' => $action,
            'log_data' => json_encode($modelData)
        ]);

        return $log;

    }




}