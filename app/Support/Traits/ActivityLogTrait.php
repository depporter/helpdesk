<?php namespace App\Support\Traits;

use App\Support\Logger\Logger;

trait ActivityLogTrait
{
    public function created($model)
    {
        $logger = new Logger();
        return $logger->autoLog(get_class($model), 'create', $model->toArray());
    }

    public  function updated($model)
    {

        $logger = new Logger();
        return $logger->autoLog(get_class($model), 'update', $model->toArray());

    }

    public function deleted($model)
    {
        $logger = new Logger();
        return $logger->autoLog(get_class($model), 'delete', $model->toArray());
    }
}