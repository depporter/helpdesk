<?php namespace App\Support\Traits;


trait CommonTrait
{
    public function checkModelExists(string $model): bool
    {

        $classPath = app_path() . '/Models/' . ucfirst($model) . '.php';
        $className = "App\Models\\" . ucfirst($model);

        if (!file_exists($classPath))
        {
            return false;
        }

        try {
            $inst = new $className;

            if ($className != get_class($inst))
            {
                return false;
            }

            return true;
        }
        catch (Exception $e)
        {
            return false;
        }

    }

    public function generatePersonalCode()
    {
        return str_pad(rand(1000, 999999), 6, '0', STR_PAD_LEFT);
    }

    public function apiResponse($data, int $code = 200)
    {
        $response = [
            'code' => $code,
            'response' => $data
        ];

        return response()->json($response);
    }




}