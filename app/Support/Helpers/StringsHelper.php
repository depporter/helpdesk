<?php namespace App\Support\Helpers;

class StringsHelper
{


    /**
     * Разбивает строку в массив по символу переноса (\n)
     * Возвращает уникальный массив строк
     * @param string $string
     * @return array
     */
    public function stringsToArrayByEol(string $string): array
    {
        return array_unique(array_values(array_filter(explode("\n", str_replace("\r", "", $string)))));
    }

    /**
     * И так понятно, что это такое.
     * @param String $value
     * @return string
     */
    public function translitToFriendlyUrl(string $value): string
    {
        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g",
            "Д" => "d", "Е" => "e", "Ж" => "j", "З" => "z", "И" => "i",
            "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            " -" => "", "," => "", " " => "-", "." => "", "/" => "_",
            "-" => ""
        );

        $value = strtr($value, $tr);

        if (preg_match('/[^A-Za-z0-9_\-]/', $value)) {

            $value = preg_replace('/[^A-Za-z0-9_\-]/', '', $value);
        }
        return strtolower($value);
    }

    public function queryStringToArray(string $queryString, bool $excludePagination = true): array
    {
        parse_str($queryString, $array);

        if ($excludePagination) {
            if (key_exists('page', $array)) {
                unset($array['page']);
            }
        }

        return $array;
    }

    public function ruPlural(int $n, string $one, string $two, string $five): string
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $one : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $two : $five);
    }

    public function replaceComma(string $value): float
    {
        return floatval(str_replace(' ', '', str_replace(',', '.', $value)));
    }


}