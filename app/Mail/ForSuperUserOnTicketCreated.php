<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


use App\Models\Admin;
use App\Models\Ticket;
use App\Models\TicketCategory;

class ForSuperUserOnTicketCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $ticket;
    public $admin;
    public $ticketCategory;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Admin $admin, Ticket $ticket, TicketCategory $ticketCategory)
    {
        $this->admin = $admin;
        $this->ticket = $ticket;
        $this->ticketCategory = $ticketCategory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новый тикет #' . $this->ticket->id . ' категория:' . $this->ticketCategory->name)->view('emails.tickets.for_super_user_on_ticket_created');
    }
}
