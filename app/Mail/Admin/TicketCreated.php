<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Admin;
use App\Models\Ticket;

class TicketCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $admin;
    public $ticket;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Admin $admin, Ticket $ticket)
    {
        $this->admin = $admin;
        $this->ticket = $ticket;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Создан новый тикет #' . $this->ticket->id)->view('emails.tickets.ticket_created');
    }
}
