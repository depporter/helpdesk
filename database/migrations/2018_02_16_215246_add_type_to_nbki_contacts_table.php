<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToNbkiContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nbki_contacts', function (Blueprint $table) {
            $table->enum('type', ['package', 'query'])->nullable()->after('request_id');
            $table->text('excluded_phones')->nullable()->after('date_birth');
            $table->integer('mode')->default(0)->after('date_birth');
            $table->integer('months')->default(0)->after('date_birth');
            $table->tinyInteger('limit')->default(0)->after('date_birth');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nbki_contacts', function (Blueprint $table) {
            //
        });
    }
}
