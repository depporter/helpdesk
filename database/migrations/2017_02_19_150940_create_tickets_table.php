<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('creator', ['customer', 'support']);
            $table->unsignedInteger('customer_id')->nullale();
            $table->unsignedInteger('support_id')->nullable();
            $table->unsignedInteger('ticket_category_id');
            $table->string('title');
            $table->unsignedSmallInteger('customer_unread_messages')->default(0);
            $table->unsignedSmallInteger('user_unread_messages')->default(0);
            $table->boolean('customer_archived')->default(0);
            $table->boolean('support_archived')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->index('customer_id');
            $table->index('support_id');
            $table->index('ticket_category_id');
            $table->index('customer_archived');
            $table->index('support_archived');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
