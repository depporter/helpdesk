<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketCategoryCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_category_customers', function (Blueprint $table) {
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('category_id');
            $table->unsignedSmallInteger('ticket_limit');
            $table->primary(['customer_id', 'category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_category_customers');
    }
}
