<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNbkiFieldsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->enum('nbki_credit_rating_type', ['off', 'query', 'package'])->after('balance');
            $table->unsignedInteger('nbki_credit_rating_query_price')->default('0')->after('balance');
            $table->unsignedInteger('nbki_credit_rating_package_amount')->default('0')->after('balance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            //
        });
    }
}
