<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFieldsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->boolean('allow_nbki_credit_rating')->after('phone')->default(0);
            $table->decimal('nbki_credit_rating_price')->after('phone')->default(0);
            $table->boolean('allow_nbki_contact_actualisation')->after('phone')->default(0);
            $table->decimal('nbki_contact_actualisation_price')->after('phone')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            //
        });
    }
}
