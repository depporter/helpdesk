<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNbkiContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nbki_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('request_id')->nullable();
            $table->string('first_name', 256)->nullable();
            $table->string('last_name', 256)->nullable();
            $table->string('middle_name', 256)->nullable();
            $table->date('date_birth')->nullable();
            $table->unsignedTinyInteger('doc_type');
            $table->string('doc_number', 6)->nullable();
            $table->string('doc_series', 4)->nullable();
            $table->date('doc_issue_date')->nullable();
            $table->text('response')->nullable();
            $table->decimal('cost')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nbki_contacts');
    }
}
