<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatingContactsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->enum('actualization_contacts_type', ['off', 'query', 'package'])->after('nbki_credit_rating_type');
            $table->unsignedInteger('actualization_contacts_query_price')->default('0')->after('nbki_credit_rating_type');
            $table->unsignedInteger('actualization_contacts_package_amount')->default('0')->after('nbki_credit_rating_type');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            //
        });
    }
}
