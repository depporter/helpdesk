$.App.modules.manageTickets = {
    handleFilterForm: function (form) {
        var url = form.attr('action') + '?' + form.serialize();
        var table = $('#ticketList');
        $.App.ui.elemMask("#ticketList");

        $.get( url, function( data ) {
            table.find('tbody').html(data);
        }).always(function() {
            $.App.ui.elemUnmask("#ticketList");
        });
    },

    handleTicketMessageForm: function (form) {

        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.box-footer');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function (data) {
                $("#chat-box").append(data);
            },
            error: function (data) {

                if (data.status == 422)
                {
                    $.App.ui.showMessage('Ошибка', 'Напишите сообщение', 'error');
                }


                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
                form.find('input').val('');
            }
        });
    },

    handleTicketDeleteBtn: function (link) {
        var url = link.attr('href');
        var elemMask = link.closest('li');

        swal({
                title: "Удаление файла",
                text: "Вы уверены, что хотите удалить файл?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Да удалить!",
                cancelButtonText: "Не удалять!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.App.ui.elemMask(elemMask);
                    $.get(url, function (data) {

                        link.closest('li').fadeOut();


                    }).always(function () {
                        $.App.ui.elemUnmask(elemMask);
                    });
                } else {
                    $.App.ui.elemUnmask(elemMask);
                }
            });
    }
}