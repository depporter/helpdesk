$.App.modules.usersCustomers = {

    handleCustomerForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.modal').find('.modal-body');
        var modal = form.closest('.modal');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function () {
                modal.modal('hide');
                $.App.core.loadContentInTable();
            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'adminCustomerForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },

    handleCustomerGroupForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.modal').find('.modal-body');
        var modal = form.closest('.modal');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function () {
                modal.modal('hide');
                $.App.core.loadContentInTable();
            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'adminCustomerForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },

    handleCustomerTransactionForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.modal').find('.modal-body');
        var modal = form.closest('.modal');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function () {
                modal.modal('hide');
                $.App.core.loadContentInTable();
            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'adminCustomerTransactionForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },

    handleCustomerFilterForm: function (form) {
        var url = form.attr('action') + '?' + form.serialize();
        var table = $('#customersListTable');
        $.App.ui.elemMask("#customersListTable");

        $.get( url, function( data ) {
            table.find('tbody').html(data);
        }).always(function() {
            $.App.ui.elemUnmask("#customersListTable");
        });
    },

    handleSearchBrandForm: function (form) {

        var url = form.attr('action');
        var formData = form.serialize();

        
        blockElem = $("#brandsSearchBlock");

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.ui.elemMask(blockElem);
            },
            success: function (data) {
                $("#brandsSearchResults").html(data);
                $("#adminCustomerSearchBrandForm .brand-name").focus();

            },
            error: function (data) {

                if (data.status == 422)
                {
                    $.App.validation.fail(data.responseJSON, 'adminCustomerSearchBrandForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });

    },

    handleCustomerPaymentForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.modal').find('.modal-body');
        var modal = form.closest('.modal');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function () {
                modal.modal('hide');
                $.App.core.loadContentInTable();
            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'adminCustomerPaymentForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },
    
    handleAddBrand: function (link) {
        var table = $("#customerBrandsTable");
        var brandId = link.data('brand-id');
        var brandName = link.data('brand-name');
        var deleteUrl = link.data('brand-delete-url');
        var url = link.attr('href');

        var tpl = '<tr style="display: none" class="new-item"><td class="text-center">'+ brandId +'</td><td>'+ brandName +'</td><td class="text-center"><a href="'+deleteUrl+'" class="handle-click" data-app-module="usersCustomers" data-app-method="handleDeleteBrand" data-brand-id="'+brandId+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>'

        $.ajax({
            method: 'post',
            url: url,
            data: {'brandId' : brandId},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {

                link.closest('li').fadeOut();

            },
            success: function () {
                table.find('tbody').append(tpl);
                table.find('.new-item').fadeIn();
                $.App.core.loadContentInTable();
            },
            error: function (data) {



            },
            complete: function () {

            }
        });
    },




}

$(document.body).on('change', '#allow_nbki_credit_rating', function () {
    if(this.checked) {
        $("#nbki_credit_rating_price").fadeIn();
        $("#nbki_credit_rating_price").find('input').val('0');
        $("#nbki_credit_rating_price").find('input').focus();
        $("#nbki_credit_rating_price").find('input').select();
    } else {
        $("#nbki_credit_rating_price").fadeOut();
        $("#nbki_credit_rating_price").find('input').val('0');
    }

});

$(document.body).on('change', '#allow_nbki_contact_actualisation', function () {
    if(this.checked) {
        $("#nbki_contact_actualisation_price").fadeIn();
        $("#nbki_contact_actualisation_price").find('input').val('0');
        $("#nbki_contact_actualisation_price").find('input').focus();
        $("#nbki_contact_actualisation_price").find('input').select();
    } else {
        $("#nbki_contact_actualisation_price").fadeOut();
        $("#nbki_contact_actualisation_price").find('input').val('0');
    }

});

$(document.body).on('click', '#adminCustomerForm .show-password', function (e) {
    e.preventDefault();
    var input = $(this).closest('.form-group').find('input');

    if (input.attr('type') == 'text')
    {
        input.attr('type', 'password');
    } else {
        input.attr('type', 'text');
    }

});




