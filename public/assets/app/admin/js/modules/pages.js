
$.App.modules.pages = {

    handlePagesForm: function (form) {

        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.modal').find('.modal-body');
        var modal = form.closest('.modal');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function () {
                modal.modal('hide');
                $.App.core.loadContentInTable();
            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'pagesForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    }
}