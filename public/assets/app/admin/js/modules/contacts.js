
$.App.modules.contacts = {
    handleContactsForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.box-body');


        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function () {

                $.App.core.loadContentInTable();
            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'contactsForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    }
}



$(document.body).on('submit', '#contactsFilterForm', function (e) {
    e.preventDefault();

    var url = $(this).attr('action') + '?' + $(this).serialize();
    var table = $('#contactTable');
    $.App.ui.elemMask(table);

    $.get( url, function( data ) {
        table.find('tbody').html(data);
    }).always(function() {
        $.App.ui.elemUnmask(table);
    });
});


$(document.body).on('click', '.contactsResult', function (e) {

    e.preventDefault();
    var url = $(this).attr('href');
    loadResults(url);
});

var loadResults = function (url) {



    var blockElem = $("#contactsResultModal .modal-body");

    $.ajax({
        method: 'get',
        url: url,

        beforeSend: function () {
            $("#contactsResultModal .modal-body").html('');
            $("#contactsResultModal").modal('show');
            $.App.ui.elemMask(blockElem);
        },
        success: function (data) {

            $("#contactsResultModal .modal-body").html(data);


        },
        error: function (data) {

            $.App.ui.elemUnmask(blockElem);

        },
        complete: function () {
            $.App.ui.elemUnmask(blockElem);
            $('#contactsResultModal').modal('handleUpdate');
        }
    });
}