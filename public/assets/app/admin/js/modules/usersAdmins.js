$.App.modules.usersAdmins = {

    handleAdminForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.modal').find('.modal-body');
        var modal = form.closest('.modal');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function () {
                modal.modal('hide');
                $.App.core.loadContentInTable();
            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'usersAdminForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },

    handleRoleForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.modal').find('.modal-body');
        var modal = form.closest('.modal');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function () {
                modal.modal('hide');
                $.App.core.loadContentInTable();
            },
            error: function (data) {

                if (data.status == 422)
                {
                    $.App.validation.fail(data.responseJSON, 'adminRoleForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },

    adminUserFilterForm: function (form) {
        var url = form.attr('action') + '?' + form.serialize();
        var table = $('#usersListTable');
        $.App.ui.elemMask("#usersListTable");

        $.get( url, function( data ) {
            table.find('tbody').html(data);
        }).always(function() {
            $.App.ui.elemUnmask("#usersListTable");
        });
    },
}