$.App.modules.auth = {
    handleLoginForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask('.login-box-body');
            },
            success: function () {
                if (form.data('success-url').length)
                {
                    location.replace(form.data('success-url'));
                }
            },
            error: function (data) {
                if (data.status == 403)
                {
                    $.App.ui.showMessage('Ошибка', 'Логин или пароль не верные!', 'error');
                }

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'loginForm');
                }

                $.App.ui.elemUnmask('.login-box-body');

            },
            complete: function () {
                $.App.ui.elemUnmask('.login-box-body');
            }
        });
    }



};