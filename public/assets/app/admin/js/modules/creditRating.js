
$(document.body).on('submit', '#creditRatingFilterForm', function (e) {
    e.preventDefault();

    var url = $(this).attr('action') + '?' + $(this).serialize();
    var table = $('#creditRatingTable');
    $.App.ui.elemMask(table);

    $.get( url, function( data ) {
        table.find('tbody').html(data);
    }).always(function() {
        $.App.ui.elemUnmask(table);
    });
});


$(document.body).on('click', '.creditRatingResult', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    loadResults(url);
});

var loadResults = function (url) {



    var blockElem = $("#creditRatingResultModal .modal-body");

    $.ajax({
        method: 'get',
        url: url,

        beforeSend: function () {
            $("#creditRatingResultModal .modal-body").html('');
            $("#creditRatingResultModal").modal('show');
            $.App.ui.elemMask(blockElem);
        },
        success: function (data) {

            $("#creditRatingResultModal .modal-body").html(data);


        },
        error: function (data) {

            $.App.ui.elemUnmask(blockElem);

        },
        complete: function () {
            $.App.ui.elemUnmask(blockElem);
            $('#creditRatingResultModal').modal('handleUpdate');
        }
    });
}