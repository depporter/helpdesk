if (typeof jQuery === "undefined") {
    throw new Error("App requires jQuery");
}



$.App = {};

$.App.modules = {};

$.App.core = {
    loadModalContent: function (url, modal, modalTitle) {



        var modal = $(modal).modal('show');
        modal.find('.modal-title').html(modalTitle);

        $.App.ui.elemMask(modal.find('.modal-content'));
        $.get(url, function (data) {
            modal.find('.modal-body').html(data);
            if ($(".dp").length)
            {
                $.App.ui.initDp();
            }

            if ($("#content").length)
            {
                editor = CKEDITOR.replace('content', {
                    toolbarGroups: [
                        {"name":"basicstyles","groups":["basicstyles"]},
                        {"name":"links","groups":["links"]},
                        {"name":"paragraph","groups":["list","blocks"]},
                        {"name":"styles","groups":["styles"]},
                        {"name":"insert","groups":["insert"]},



                    ],
                    // Remove the redundant buttons from toolbar groups defined above.
                    removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,HorizontalRule,Table',
                    height: 300

                });
            }

        }).always(function () {
            $.App.ui.elemUnmask(modal.find('.modal-content'));


        });

    },

    loadContentInTable: function (url, table) {

        if (url && table)
        {
            $.App.ui.elemMask(table);
            $.get(url, function (data) {
                table.find('tbody').html(data);
                $.App.ui.scrollToTop();

            }).always(function () {
                $.App.ui.elemUnmask(table);
            });

            return;
        }

        $(".table-has-content").each(function (index) {

            if ($(this).data('content-url').length) {
                var table = $(this);
                var url = $(this).data('content-url');
                var self = $(this);
                $.App.ui.elemMask(self);
                $.get(url, function (data) {
                    table.find('tbody').html(data);

                }).always(function () {
                    $.App.ui.elemUnmask(self);
                });
            }
        });


    },

    loadContentInBlock: function () {

        $(".block-has-content").each(function (index) {

            if ($(this).data('url').length) {
                var block = $(this);
                var url = $(this).data('url');
                var self = $(this);
                $.App.ui.elemMask(self);
                $.get(url, function (data) {
                    block.html(data);

                }).always(function () {
                    $.App.ui.elemUnmask(self);
                });
            }
        });
    },

    runMethod: function (object) {

        if (object.data('app-module') && object.data('app-method')) {

            var moduleName = object.data('app-module');
            var moduleFunc = object.data('app-method');


            var fn = $.App.modules[moduleName][moduleFunc];

            if (typeof fn === "function") {
                return fn(object);


            } else {
                $.App.ui.showMessage('Ошибка', 'Обработчик для ' + funcName + ' не найден', 'error')
                console.log('Handler ' + funcName + ' not exists');

            }

        }
    }

    
};

$.App.plugins = {
    select2: function () {
        $(".select2").select2();
    }
};

$.App.ui = {

    elemMask: function (elem) {

        if (typeof elem == 'object')
        {
            elem.addClass('busy');
        } else {
            $(elem).addClass('busy');
        }

    },

    elemUnmask: function (elem) {

        if (typeof elem == 'object')
        {
            elem.removeClass('busy');
        } else {
            $(elem).removeClass('busy');
        }

    },

    showMessage: function (header, message, type) {
        if (!type) {
            type = 'success';
        }
        swal(header, message, type);
    },

    scrollToTop: function () {
        $("html, body").animate({ scrollTop: 0 }, "fast");
    },

    initDp: function () {
        console.log('okk');
        $('.dp').datepicker({
            autoclose: true,
            format: "dd.mm.yyyy",
            language: 'ru',
            orientation: 'top left',
            zIndexOffset: 5000
        });
    }


};

$.App.map = {

    vars: {
        lat: 42.876680,
        lng: 74.588096,
        latLng: null,
        markersArray: [],
        map: null,
        mapOptions: {
            zoom: 13,
            center: null,
            mapTypeId: null,
            scrollwheel: false,
        }
    },

    init:function () {

        if($(".has-map .lat").val())
        {
            $.App.map.vars.lat = $(".has-map .lat").val();
        }

        if ($(".has-map .lng").val())
        {
            $.App.map.vars.lng = $(".has-map .lng").val();
        }

        $.App.map.vars.latLng = new google.maps.LatLng($.App.map.vars.lat, $.App.map.vars.lng);
        $.App.map.vars.mapOptions.center = $.App.map.vars.latLng;
        $.App.map.vars.mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;

        $.App.map.vars.map = new google.maps.Map(document.getElementById('map'), $.App.map.vars.mapOptions);

        $.App.map.setMarker($.App.map.vars.latLng);


        google.maps.event.addListener($.App.map.vars.map, "click", function(event)
        {
            // place a marker
            $.App.map.setMarker(event.latLng);
            $.App.map.vars.lat = event.latLng.lat();
            $.App.map.vars.lng = event.latLng.lng();



        });


    },
    
    setMarker: function (latLng) {
        $.App.map.deleteOverlays();
        var marker = new google.maps.Marker({
            position: latLng,
            map: $.App.map.vars.map,

        });

        $(".has-map .lat").val(latLng.lat());
        $(".has-map .lng").val(latLng.lng());

        $.App.map.vars.map.panTo(latLng);

        // add marker in markers array
        $.App.map.vars.markersArray.push(marker);
    },
    
    deleteOverlays: function () {
        if ($.App.map.vars.markersArray) {
            for (i in $.App.map.vars.markersArray) {
                $.App.map.vars.markersArray[i].setMap(null);
            }
            $.App.map.vars.markersArray.length = 0;
        }
    }

}

$.App.validation = {
    fail: function (errors, formId) {
        for (field in errors) {
            var origField = field;
            field = field.replace('.', "_");


            if (field.indexOf('image') !== -1) {
                field = 'image[]';
            }

            var formGroup = $("#" + formId + " *[name='" + field + "']").closest('.form-group');
            formGroup.addClass('has-error');

            for (message in errors[origField]) {

                formGroup.find('.help-block').html(errors[origField][message]);
            }
        }
    },

    resetErrors: function () {
        $('.form-group').find('.help-block').html('');
        $('.form-group').removeClass('has-error');
    }
};


$(document.body).on('submit', 'form', function (e) {
    e.preventDefault();
    return $.App.core.runMethod($(this));

});

$(document.body).on('click', '.handle-click', function (e) {
    e.preventDefault();
    return $.App.core.runMethod($(this));
});

$(document.body).on('click', '.modal-show', function (e) {
    e.preventDefault();

    var url = $(this).data('modal-content-url');
    var modalTitle = $(this).data('modal-title');
    var modal = $(this).data('modal');


    return $.App.core.loadModalContent(url, modal, modalTitle);

});

$(document.body).on('click', '.delete-item', function (e) {
    e.preventDefault();

    var url = $(this).attr('href');
    var elemMask = $(this).closest('td');



    swal({
            title: "Удаление данных",
            text: "Вы уверены что хотите удалить данные?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, удалить!",
            cancelButtonText: "Нет не удалять",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.App.ui.elemMask(elemMask);
                $.get(url, function (data) {

                    $.App.core.loadContentInTable();

                }).always(function () {
                    $.App.ui.elemUnmask(elemMask);
                });
            } else {
                $.App.ui.elemUnmask(elemMask);
            }
        });


});

$(document).ready(function () {

    if ($('.table-has-content').length) {
        return $.App.core.loadContentInTable();
    }

    if ($('.block-has-content').length) {
        return $.App.core.loadContentInBlock();
    }

    if ($("#contacts").length)
    {
        editor = CKEDITOR.replace('contacts', {
            toolbarGroups: [
                {"name":"basicstyles","groups":["basicstyles"]},
                {"name":"links","groups":["links"]},
                {"name":"paragraph","groups":["list","blocks"]},
                {"name":"styles","groups":["styles"]},
                {"name":"insert","groups":["insert"]},



            ],
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,HorizontalRule,Table',
            height: 300

        });
    }


    $.App.ui.initDp();

});

$(document.body).on('click', '.show-filter', function (e) {
    e.preventDefault();
    $(".filter").toggle('fast');
    $.App.ui.initDp();
});

$(document.body).on('click', '.pagination li a', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var table = $(this).closest('table');
    return $.App.core.loadContentInTable(url, table);

});




var initMap = function () {

    $.App.map.init();


}