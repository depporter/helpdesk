if (typeof jQuery === "undefined") {
    throw new Error("App requires jQuery");
}

$.App = {};

$.App.modules = {

};

$.App.core = {
    loadModalContent: function (url, modal, modalTitle) {


        var modal = $(modal).modal('show');
        modal.find('.modal-title').html(modalTitle);

        $.App.ui.elemMask(modal.find('.modal-content'));
        $.get(url, function (data) {
            modal.find('.modal-body').html(data);
            if ($(".dp").length)
            {
                $.App.ui.initDp();
            }

        }).always(function () {
            $.App.ui.elemUnmask(modal.find('.modal-content'));

        });

    },

    loadContentInTable: function (url, table) {

        if (url && table)
        {
            $.App.ui.elemMask(table);
            $.get(url, function (data) {
                table.find('tbody').html(data);
                $.App.ui.scrollToTop();

            }).always(function () {
                $.App.ui.elemUnmask(table);
            });

            return;
        }

        $(".table-has-content").each(function (index) {

            if ($(this).data('content-url').length) {
                var table = $(this);
                var url = $(this).data('content-url');
                var self = $(this);
                $.App.ui.elemMask(self);
                $.get(url, function (data) {
                    table.find('tbody').html(data);

                }).always(function () {
                    $.App.ui.elemUnmask(self);
                });
            }
        });


    },

    loadContentInBlock: function () {

        $(".block-has-content").each(function (index) {

            if ($(this).data('url').length) {
                var block = $(this);
                var url = $(this).data('url');
                var self = $(this);
                $.App.ui.elemMask(self);
                $.get(url, function (data) {
                    block.html(data);

                }).always(function () {
                    $.App.ui.elemUnmask(self);
                });
            }
        });
    },

    runMethod: function (object) {

        if (object.data('app-module').length && object.data('app-method').length) {

            var moduleName = object.data('app-module');
            var moduleFunc = object.data('app-method');


            var fn = $.App.modules[moduleName][moduleFunc];

            if (typeof fn === "function") {
                return fn(object);


            } else {
                $.App.ui.showMessage('Ошибка', 'Обработчик для ' + funcName + ' не найден', 'error')
                console.log('Handler ' + funcName + ' not exists');

            }

        } else {
            $.App.ui.showMessage('Ошибка', 'Определите модуль и метод', 'error')
        }
    }


};



$.App.ui = {

    elemMask: function (elem) {
        $(elem).addClass('busy');
    },

    elemUnmask: function (elem) {
        $(elem).removeClass('busy');
    },
    
    showMessage: function (header, message, type) {
        if (!type)
        {
            type = 'success';
        }
        swal(header, message, type);
    },

    scrollToTop: function () {
        $("html, body").animate({ scrollTop: 0 }, "fast");
    },
    

};

$.App.validation = {
    fail: function (errors, formId) {
        for (field in errors)
        {
            var origField = field;
            field = field.replace('.',"_");


            if(field.indexOf('image') !== -1)
            {
                field = 'image[]';
            }

            var formGroup = $("#" + formId + " *[name='" + field + "']").closest('.form-group');
            formGroup.addClass('has-error');

            for (message in errors[origField])
            {

                formGroup.find('.help-block').html(errors[origField][message]);
            }
        }
    },
    
    resetErrors: function () {
        $('.form-group').find('.help-block').html('');
        $('.form-group').removeClass('has-error');
    }
};


$(document.body).on('submit', 'form', function (e) {


    if ($(this).data('app-module') && $(this).data('app-method'))
    {
        e.preventDefault();
        var moduleName = $(this).data('app-module');
        var moduleFunc = $(this).data('app-method');


        var fn = $.App.modules[moduleName][moduleFunc];

        if (typeof fn === "function")
        {
            return fn($(this));


        } else {
            $.App.ui.showMessage('Ошибка', 'Обработчик для ' + funcName + ' не найден', 'error')
            console.log('Handler ' + funcName + ' not exists');
            console.log(typeof fn);
        }

    }
});

$(document.body).on('click', '.modal-show', function (e) {
    e.preventDefault();

    var url = $(this).data('modal-content-url');
    var modalTitle = $(this).data('modal-title');
    var modal = $(this).data('modal');


    return $.App.core.loadModalContent(url, modal, modalTitle);

});


$(document.body).on('click', '.pagination li a', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var table = $(this).closest('table');
    return $.App.core.loadContentInTable(url, table);

});

$(document.body).on('click', '.handle-click', function (e) {
    e.preventDefault();
    return $.App.core.runMethod($(this));
});

$(document.body).on('click', '.show-filter', function (e) {
    e.preventDefault();
    $(".filter").toggle('fast');
});

$(document).ready(function () {

    if ($('.table-has-content').length) {
        return $.App.core.loadContentInTable();
    }

    if ($('.block-has-content').length) {
        return $.App.core.loadContentInBlock();
    }



});