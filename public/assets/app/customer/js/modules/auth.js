$.App.modules.auth = {
    handleLoginForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask('.login-box-body');
            },
            success: function () {
                if (form.data('success-url').length)
                {
                    location.replace(form.data('success-url'));
                }
            },
            error: function (data) {
                if (data.status == 403)
                {
                    $.App.ui.showMessage('Ошибка', 'Логин или пароль не верные!', 'error');
                }

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'loginForm');
                }

                $.App.ui.elemUnmask('.login-box-body');

            },
            complete: function () {
                $.App.ui.elemUnmask('.login-box-body');
            }
        });
    },

    handleRegisterForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask('.register-box-body');
            },
            success: function () {

                swal({
                        title: "Вы зарегистрированны!",
                        text: "На указанный при регистрации e-mail мы отправили письмо с дальнешими инструкциями. Пожалуйста, проверьте почту",
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "ОК!",
                        closeOnConfirm: true
                    },
                    function(){

                        if (form.data('success-url').length)
                        {
                            location.replace(form.data('success-url'));
                        }
                    });

            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'registerForm');
                }

                $.App.ui.elemUnmask('.register-box-body');

            },
            complete: function () {
                $.App.ui.elemUnmask('.register-box-body');
            }
        });
    },

    handleRemindForm: function (form) {

        var url = form.attr('action');
        var formData = form.serialize();

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask('.login-box-body');
            },
            success: function () {
                $.App.ui.showMessage('Восстановление пароля', 'На указанный e-mail отправлено письмо с инструкцией по восстановлению пароля!', 'success');
            },
            error: function (data) {
                if (data.status == 403)
                {
                    $.App.ui.showMessage('Ошибка', 'Пользователя с указанным e-mail не существует!', 'error');
                }

                if (data.status == 406)
                {
                    $.App.ui.showMessage('Ошибка', 'Указанный E-mail использует для авторизации соц сеть! Авторизуйтесь на сайте используя соц сеть.', 'error');
                }

                if (data.status == 422)
                {
                    $.App.validation.fail(data.responseJSON, 'remindForm');
                }


                $.App.ui.elemUnmask('.login-box-body');

            },
            complete: function () {
                $.App.ui.elemUnmask('.login-box-body');
            }
        });
    },

    handleNewPasswordForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask('.login-box-body');
            },
            success: function () {
                if (form.data('success-url').length)
                {
                    location.replace(form.data('success-url'));
                }
            },
            error: function (data) {

                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'newPasswordForm');
                }

                $.App.ui.elemUnmask('.login-box-body');

            },
            complete: function () {
                $.App.ui.elemUnmask('.login-box-body');
            }
        });
    },

    handleRequestForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var elemMask = '#requestModal .modal-body';

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(elemMask);
            },
            success: function () {
                swal({
                        title: "Заявка отправлена",
                        text: "Мы свяжемся с Вами по указанному номеру для уточнения деталей",
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "ОК!",
                        closeOnConfirm: true
                    },
                    function(){
                        $("#requestModal").modal('hide');
                        $('body').hide();
                        location.replace('/');
                    });
            },
            error: function (data) {
                if (data.status == 422)
                {

                    $.App.validation.fail(data.responseJSON, 'requestForm');
                }

                $.App.ui.elemUnmask(elemMask);
            },
            complete: function () {
                $.App.ui.elemUnmask(elemMask);
            }
        });
    }

};