

$(document.body).on('click', '#createCreditRatingRequest', function (e) {
    e.preventDefault();

    $("#creditRatingModal").modal('show');
    var url = $(this).attr('href');

    $.App.ui.elemMask($("#creditRatingModal .modal-body"));

    $.get( url, function( data ) {
        $("#creditRatingModal .modal-body").html(data);
    }).always(function() {
        $.App.ui.elemUnmask($("#creditRatingModal .modal-body"));
    });

});

$(document.body).on('submit', '#creditRatingForm', function (e) {
    e.preventDefault();

    var url = $(this).attr('action');
    var formData = $(this).serialize();
    var blockElem = $('#creditRatingModal .modal-body');

    $.ajax({
        method: 'post',
        url: url,
        data: formData,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        beforeSend: function () {
            $.App.validation.resetErrors();
            $.App.ui.elemMask(blockElem);
        },
        success: function (data) {


            $("#creditRatingForm").find('input').val('');
            $("#creditRatingModal").modal('hide');

            $.App.core.loadContentInTable();

            $('#creditRatingModal').on('hidden.bs.modal', function (e) {
                loadResults(data.result_url);
            })

        },
        error: function (data) {

            if (data.status == 422)
            {
                $.App.validation.fail(data.responseJSON, 'creditRatingForm');
            }

            if (data.status == 409)
            {
                $.App.ui.showMessage('Ошибка', data.responseJSON.message, 'error');
            }


            $.App.ui.elemUnmask(blockElem);

        },
        complete: function () {
            $.App.ui.elemUnmask(blockElem);

        }
    });
});

$(document.body).on('submit', '#creditRatingFilterForm', function (e) {
    e.preventDefault();

    var url = $(this).attr('action') + '?' + $(this).serialize();
    var table = $('#creditRatingTable');
    $.App.ui.elemMask(table);

    $.get( url, function( data ) {
        table.find('tbody').html(data);
    }).always(function() {
        $.App.ui.elemUnmask(table);
    });
});



$(document.body).on('click', '.creditRatingResult', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    loadResults(url);
});

$(document.body).on('keyup', '#doc_series', function (e) {
    console.log($(this).val().length);

    if ($(this).val().length == 4)
    {
        $("#doc_number").focus();
    }
});

var loadResults = function (url) {



    var blockElem = $("#creditRatingResultModal .modal-body");

    $.ajax({
        method: 'get',
        url: url,

        beforeSend: function () {
            $("#creditRatingResultModal .modal-body").html('');
            $("#creditRatingResultModal").modal('show');
            $.App.ui.elemMask(blockElem);
        },
        success: function (data) {

            $("#creditRatingResultModal .modal-body").html(data);


        },
        error: function (data) {

            $.App.ui.elemUnmask(blockElem);

        },
        complete: function () {
            $.App.ui.elemUnmask(blockElem);
            $('#creditRatingResultModal').modal('handleUpdate');
        }
    });
}