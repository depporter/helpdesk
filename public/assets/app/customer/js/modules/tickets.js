$.App.modules.tickets = {

    handleTicketCreateForm: function (form) {
        var url = form.attr('action');
        var formData = new FormData(form[0]);
        var blockElem = form.closest('.box-body');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            dataType: 'json',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function (data) {
                location.replace(data.url);
            },
            error: function (data) {



                if (data.status == 422)
                {
                    $.App.validation.fail(data.responseJSON, 'ticketCreateForm');
                }

                if (data.status == 402)
                {
                    $.App.ui.showMessage('Тикет не создан', data.responseJSON, 'error');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },

    handleTicketUpdateForm: function (form) {
        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.modal').find('.modal-body');
        var modal = form.closest('.modal');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,

            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function (data) {
                $.App.core.loadContentInTable();
                modal.modal('hide');
            },
            error: function (data) {

                if (data.status == 422)
                {
                    $.App.validation.fail(data.responseJSON, 'ticketUpdateForm');
                }

                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },

    handleUploadBtn: function () {
        $("#fileSelector").click();
    },

    handleUploadForm: function (form) {
        var url = form.attr('action');
        var formData = new FormData(form[0]);


        $.ajax({
            method: "POST",
            url: url,
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend : function() {
                $("#fileUploadBtn").fadeOut(function () {
                    $("#spinner").fadeIn();
                });
            },
            success: function (data) {
                $("#spinner").fadeOut(function () {
                    $("#fileUploadBtn").fadeIn();
                });
                $("#fileSelector").val('');
                $.App.modules.tickets.getCustomerFiles();
            },

            error: function (data) {
                if (data.status == '422')
                {
                    $.App.ui.showMessage('Ошибка', 'К загрузке допускаются только .jpeg, .jpg, .bmp, .png, .zip, .rar, .pdf', 'error');

                }

                $("#spinner").fadeOut(function () {
                    $("#fileUploadBtn").fadeIn();
                });
                $("#fileSelector").val('');
            },

            complete: function() {
                $("#spinner").fadeOut(function () {
                    $("#fileUploadBtn").fadeIn();
                });
                $("#fileSelector").val('');
            }
        });
    },
    
    getCustomerFiles: function () {
        var url = $(".customer-files").data('url');
        var blockElem = $(".customer-files");
        $.ajax({
            method: 'get',
            url: url,

            beforeSend: function () {
                $.App.ui.elemMask(blockElem);
            },
            success: function (data) {
                blockElem.html(data);
            },

            error: function (data) {

                $.App.ui.elemUnmask(blockElem);
            },

            complete: function () {
                $.App.ui.elemUnmask(blockElem);
            }
        });
    },

    handleTicketMessageForm: function (form) {

        var url = form.attr('action');
        var formData = form.serialize();
        var blockElem = form.closest('.box-footer');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                $.App.validation.resetErrors();
                $.App.ui.elemMask(blockElem);
            },
            success: function (data) {
                $("#chat-box").append(data);
            },
            error: function (data) {

                if (data.status == 422)
                {
                    $.App.ui.showMessage('Ошибка', 'Напишите сообщение', 'error');
                }


                $.App.ui.elemUnmask(blockElem);

            },
            complete: function () {
                $.App.ui.elemUnmask(blockElem);
                form.find('input').val('');
            }
        });
    },

    handleArchiveBtn: function (link) {

        var url = link.attr('href');
        var elemMask = link.closest('td');
        var redirectUrl = link.data('redirect-after');

        swal({
                title: "Перемещение в архив",
                text: "Данный тикет будет перемещен в архив?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Хорошо!",
                cancelButtonText: "Не перемещать!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.App.ui.elemMask(elemMask);
                    $.get(url, function (data) {
                        location.replace(redirectUrl);

                    }).always(function () {
                        $.App.ui.elemUnmask(elemMask);
                    });
                } else {
                    $.App.ui.elemUnmask(elemMask);
                }
            });
    },

    handleArchiveBtnFromList: function (link) {
        var url = link.attr('href');
        var elemMask = link.closest('td');


        swal({
                title: "Перемещение в архив",
                text: "Данный тикет будет перемещен в архив?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Хорошо!",
                cancelButtonText: "Не перемещать!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.App.ui.elemMask(elemMask);
                    $.get(url, function (data) {
                        $.App.core.loadContentInTable();

                    }).always(function () {
                        $.App.ui.elemUnmask(elemMask);
                    });
                } else {
                    $.App.ui.elemUnmask(elemMask);
                }
            });
    }
}


$(document.body).on('change', '#fileSelector', function (e) {
    $('#uploadForm').submit();
});

$(document.body).on('change', '#ticket_category_id', function (e) {


    if ($("option:selected", this).data('title-hint'))
    {
        $("#title").attr('placeholder', $("option:selected", this).data('title-hint'));
    } else {
        $("#title").attr('placeholder', '');
    }

    if ($("option:selected", this).data('desc-hint'))
    {
        $("#message").attr('placeholder', $("option:selected", this).data('desc-hint'));
    } else {
        $("#message").attr('placeholder', '');
    }
});