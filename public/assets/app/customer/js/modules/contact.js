
$(document.body).on('click', '#createContactRequest', function (e) {
    e.preventDefault();

    $("#contactModal").modal('show');
    var url = $(this).attr('href');

    $.App.ui.elemMask($("#contactModal .modal-body"));

    $.get( url, function( data ) {
        $("#contactModal .modal-body").html(data);
    }).always(function() {
        $.App.ui.elemUnmask($("#contactModal .modal-body"));

        if ($(".datemask").length)
        {
            $('.datemask').inputmask('dd.mm.yyyy', { 'placeholder': 'дд.мм.гггг' })
        }
    });

});

$(document.body).on('submit', '#contactForm', function (e) {
    e.preventDefault();

    var url = $(this).attr('action');
    var formData = $(this).serialize();
    var blockElem = $('#contactModal .modal-body');

    $.ajax({
        method: 'post',
        url: url,
        data: formData,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        beforeSend: function () {
            $.App.validation.resetErrors();
            $.App.ui.elemMask(blockElem);
        },
        success: function (data) {

            $("#contactForm").find('input').val('');
            $("#contactModal").modal('hide');

            $.App.core.loadContentInTable();

            $('#contactModal').on('hidden.bs.modal', function (e) {
                loadResults(data.result_url);
            })

        },
        error: function (data) {

            if (data.status == 422)
            {
                $.App.validation.fail(data.responseJSON, 'contactForm');
            }

            if (data.status == 409)
            {
                $.App.ui.showMessage('Ошибка', data.responseJSON.message, 'error');
            }


            $.App.ui.elemUnmask(blockElem);

        },
        complete: function () {
            $.App.ui.elemUnmask(blockElem);

        }
    });
});

$(document.body).on('submit', '#contactFilterForm', function (e) {
    e.preventDefault();

    var url = $(this).attr('action') + '?' + $(this).serialize();
    var table = $('#contactTable');
    $.App.ui.elemMask(table);

    $.get( url, function( data ) {
        table.find('tbody').html(data);
    }).always(function() {
        $.App.ui.elemUnmask(table);
    });
});


$(document.body).on('click', '.contactResult', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    loadResults(url);
});

$(document.body).on('keyup', '#doc_series', function (e) {
    console.log($(this).val().length);

    if ($(this).val().length == 4)
    {
        $("#doc_number").focus();
    }
});

var loadResults = function (url) {



    var blockElem = $("#contactResultModal .modal-body");

    $.ajax({
        method: 'get',
        url: url,

        beforeSend: function () {
            $("#contactResultModal .modal-body").html('');
            $("#contactResultModal").modal('show');
            $.App.ui.elemMask(blockElem);
        },
        success: function (data) {

            $("#contactResultModal .modal-body").html(data);




        },
        error: function (data) {

            $.App.ui.elemUnmask(blockElem);

        },
        complete: function () {
            $.App.ui.elemUnmask(blockElem);
            $('#contactResultModal').modal('handleUpdate');
        }
    });
}