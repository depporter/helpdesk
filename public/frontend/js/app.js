
$(".has-sber").change(function() {
    if(this.checked) {
        $("#card-info").fadeIn();
    } else {
        $("#card-info").fadeOut();
    }
});

$(document.body).on('submit', '#regForm', function (e) {
    e.preventDefault();

    $("#services_inputs").html('');

    $.each($(".services .service"), function () {
        if ($(this).hasClass('btn-success'))
        {
            $("#services_inputs").append('<input type="hidden" name="service[]" value="'+ $(this).data('service') +'">');
        }
    })

    var formId = $(this).attr('id');
    var blockElem = $(this);
    var url = $(this).attr('action');
    var data =  new FormData($(this)[0]);
    var self = $(this);
    $.ajax({
        method: 'post',
        url: url,
        dataType: 'json',
        data: data,
        async: true,
        cache: false,
        contentType: false,
        processData: false,

        beforeSend: function () {
            mask(blockElem);
            resetErrors();
        },
        success: function (data) {

            swal({
                title: 'Данные приняты',
                text: "Мы свяжемся с вами в ближайшее время",
                type: 'success',


            }).then(function () {
                location.reload();
            });

        },
        error: function (data) {
            if (data.status == 422) {

                validationFail(data.responseJSON, formId);
            }

        },
        complete: function () {
            unmask(blockElem);
        }
    });
});

var mask =  function (elem) {
    if (typeof elem == 'object')
    {
        elem.addClass('busy');
    } else {
        $(elem).addClass('busy');
    }
};

var unmask = function (elem) {
    if (typeof elem == 'object')
    {
        elem.removeClass('busy');
    } else {
        $(elem).removeClass('busy');
    }
};

var validationFail = function (errors, formId) {

    for (inp in errors)
    {

        var formGroup = $("#" + formId + " *[id='" + inp + "']").closest('.form-group');
        formGroup.addClass('has-error');

        for (message in errors[inp])
        {
            formGroup.find('.help-block').append(' ' + errors[inp][message]);
        }

    }
};

var resetErrors = function () {
    $('.form-group').find('.help-block').html('');
    $('.form-group').removeClass('has-error');
};


$(document.body).on('click', '.service', function (e) {

    e.preventDefault();
    var tpl = '✓';
    if ($(this).hasClass('btn-success'))
    {
        ;
        $(this).removeClass('btn-success');
        $(this).addClass('btn-info');
        $(this).find('span').html('');
    } else {

        $(this).removeClass('btn-info');
        $(this).addClass('btn-success');
        $(this).find('span').html(tpl);
    }

});