<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('test', 'TestController@index')->name('test');
Route::get('/', 'Frontend\HomeController@index')->name('frontend.home');
Route::get('rr', 'Frontend\HomeController@index')->name('frontend.home');
Route::post('register', 'Frontend\HomeController@register')->name('frontend.home.register');

// admin auth
Route::get('admins/auth/login', 'Admin\AuthController@getLogin')->name('AdminLogin');
Route::post('admins/auth/login', 'Admin\AuthController@postLogin')->name('AdminPostLogin');
Route::get('admins/auth/logout', 'Admin\AuthController@getLogout')->name('AdminLogout');

Route::group(['prefix' => 'admins', 'middleware' => 'adminMiddleware:admin'], function () {
    Route::get('/', 'Admin\HomeController@index')->name('AdminHome');

    Route::group(['prefix' => 'users/admin', 'middleware' => 'adminPermission:manage_admins'], function () {
        Route::get('/', 'Admin\Users\AdminController@index')->name('AdminUsersAdmin');
        Route::get('list', 'Admin\Users\AdminController@getList')->name('AdminUsersAdminList');
        Route::get('create', 'Admin\Users\AdminController@create')->name('AdminUsersAdminCreate');
        Route::post('store', 'Admin\Users\AdminController@store')->name('AdminUsersAdminStore');
        Route::get('{adminId}/edit', 'Admin\Users\AdminController@edit')->name('AdminUsersAdminEdit');
        Route::post('{adminId}/update', 'Admin\Users\AdminController@update')->name('AdminUsersAdminUpdate');
        Route::get('{adminId}/delete', 'Admin\Users\AdminController@delete')->name('AdminUsersAdminDelete');


        Route::get('roles', 'Admin\Users\AdminController@roles')->name('AdminUsersAdminRoles');
        Route::get('roles/get-list', 'Admin\Users\AdminController@rolesGetList')->name('AdminUsersAdminRolesList');
        Route::get('roles/create', 'Admin\Users\AdminController@rolesCreate')->name('AdminUsersAdminRolesCreate');
        Route::post('roles/store', 'Admin\Users\AdminController@rolesStore')->name('AdminUsersAdminRolesStore');
        Route::get('roles/{roleId}/edit', 'Admin\Users\AdminController@rolesEdit')->name('AdminUsersAdminRolesEdit');
        Route::post('roles/{roleId}/update', 'Admin\Users\AdminController@rolesUpdate')->name('AdminUsersAdminRolesUpdate');
    });

    Route::group(['prefix' => 'users/customer', 'middleware' => 'adminPermission:manage_customers'], function () {
        Route::get('/', 'Admin\Users\CustomerController@index')->name('AdminUsersCustomer');
        Route::get('get-list', 'Admin\Users\CustomerController@getList')->name('AdminUsersCustomerList');
        Route::get('create', 'Admin\Users\CustomerController@create')->name('AdminUsersCustomerCreate');
        Route::post('store', 'Admin\Users\CustomerController@store')->name('AdminUsersCustomerStore');
        Route::get('{customerId}/edit', 'Admin\Users\CustomerController@edit')->name('AdminUsersCustomerEdit');
        Route::post('{customerId}/update', 'Admin\Users\CustomerController@update')->name('AdminUsersCustomerUpdate');
        Route::get('{customerId}/delete', 'Admin\Users\CustomerController@delete')->name('AdminUsersCustomerDelete');
        Route::get('{customerId}/payments', 'Admin\Users\CustomerController@payments')->name('AdminUsersCustomerPayments');
        Route::get('{customerId}/payments/list', 'Admin\Users\CustomerController@paymentsList')->name('AdminUsersCustomerPaymentsList');
        Route::get('{customerId}/payments/create', 'Admin\Users\CustomerController@paymentsCreate')->name('AdminUsersCustomerPaymentsCreate');
        Route::post('{customerId}/payments/store', 'Admin\Users\CustomerController@paymentsStore')->name('AdminUsersCustomerPaymentsStore');
        Route::get('{customerId}/payments/expenses', 'Admin\Users\CustomerController@expenses')->name('AdminUsersCustomerPaymentsExpenses');

        Route::get('group/create', 'Admin\Users\CustomerController@groupCreate')->name('AdminUsersCustomerGroupCreate');
        Route::post('group/store', 'Admin\Users\CustomerController@groupStore')->name('AdminUsersCustomerGroupStore');
        Route::get('group/{groupId}/edit', 'Admin\Users\CustomerController@groupEdit')->name('AdminUsersCustomerGroupEdit');
        Route::post('group/{groupId}/update', 'Admin\Users\CustomerController@groupUpdate')->name('AdminUsersCustomerGroupUpdate');
        Route::get('group/{groupId}/customers', 'Admin\Users\CustomerController@groupCustomers')->name('AdminUsersCustomerGroupCustomers');
        Route::get('group/{groupId}/customers/list', 'Admin\Users\CustomerController@groupCustomersList')->name('AdminUsersCustomerGroupCustomersList');
        Route::get('reg/list', 'Admin\Users\CustomerController@regList')->name('AdminUsersCustomerRegList');
        Route::get('reg/{id}/delete', 'Admin\Users\CustomerController@deleteReg')->name('AdminUsersCustomerRegDelete');
    });

    Route::group(['prefix' => 'catalogs/ticket-categories', 'middleware' => 'adminPermission:catalogs_tickets_categories'], function () {
        Route::get('/', 'Admin\Catalogs\TicketCategoryController@index')->name('AdminCatalogsTicketCategory');
        Route::get('create', 'Admin\Catalogs\TicketCategoryController@create')->name('AdminCatalogsTicketCategoryCreate');
        Route::get('get-list', 'Admin\Catalogs\TicketCategoryController@getList')->name('AdminCatalogsTicketCategoryList');
        Route::post('store', 'Admin\Catalogs\TicketCategoryController@store')->name('AdminCatalogsTicketCategoryStore');
        Route::get('{categoryId}/edit', 'Admin\Catalogs\TicketCategoryController@edit')->name('AdminCatalogsTicketCategoryEdit');
        Route::post('{categoryId}/update', 'Admin\Catalogs\TicketCategoryController@update')->name('AdminCatalogsTicketCategoryUpdate');
        Route::get('{categoryId}/delete', 'Admin\Catalogs\TicketCategoryController@delete')->name('AdminCatalogsTicketCategoryDelete');
    });

    Route::group(['prefix' => 'tickets', 'middleware' => 'adminPermission:module_tickets'], function () {
        Route::get('nobody', 'Admin\Modules\Tickets\TicketController@nobody')->name('AdminTicketNobody');
        Route::get('my', 'Admin\Modules\Tickets\TicketController@my')->name('AdminTicketMy');
        Route::get('archive', 'Admin\Modules\Tickets\TicketController@archive')->name('AdminTicketArchive');
        Route::get('get-list/{type}', 'Admin\Modules\Tickets\TicketController@getList')->name('AdminTicketList');
        Route::get('{ticketId}/take', 'Admin\Modules\Tickets\TicketController@take')->name('AdminTicketTake');
        Route::get('{ticketId}/show', 'Admin\Modules\Tickets\TicketController@show')->name('AdminTicketShow');
        Route::post('{ticketId}/upload', 'Admin\Modules\Tickets\TicketController@upload')->name('AdminTicketUpload');
        Route::post('{ticketId}/message/store', 'Admin\Modules\Tickets\TicketController@messageStore')->name('AdminTicketMessageStore');
        Route::get('{ticketId}/files/{type}', 'Admin\Modules\Tickets\TicketController@getFiles')->name('AdminTicketFiles');
        Route::get('{ticketId}/files/{fileId}/delete', 'Admin\Modules\Tickets\TicketController@deleteFile')->name('AdminTicketFilesDelete');
        Route::get('{ticketId}/action/{type}', 'Admin\Modules\Tickets\TicketController@action')->name('AdminTicketAction');

    });

    Route::group(['prefix' => 'content/news', 'middleware' => 'adminPermission:manage_news'], function () {
        Route::get('/', 'Admin\Content\NewsController@index')->name('AdminNews');
        Route::get('get-list', 'Admin\Content\NewsController@getList')->name('AdminNewsList');
        Route::get('create', 'Admin\Content\NewsController@create')->name('AdminNewsCreate');
        Route::post('store', 'Admin\Content\NewsController@store')->name('AdminNewsStore');
        Route::get('{newsId}/edit', 'Admin\Content\NewsController@edit')->name('AdminNewsEdit');
        Route::post('{newsId}/update', 'Admin\Content\NewsController@update')->name('AdminNewsUpdate');
        Route::get('{newsId}/delete', 'Admin\Content\NewsController@delete')->name('AdminNewsDelete');
    });

    Route::group(['prefix' => 'content/pages', 'middleware' => 'adminPermission:static_pages'], function () {
        Route::get('/', 'Admin\Content\PagesController@index')->name('AdminPages');
        Route::get('get-list', 'Admin\Content\PagesController@getList')->name('AdminPagesList');
        Route::get('create', 'Admin\Content\PagesController@create')->name('AdminPagesCreate');
        Route::post('store', 'Admin\Content\PagesController@store')->name('AdminPagesStore');
        Route::get('{pageId}/edit', 'Admin\Content\PagesController@edit')->name('AdminPagesEdit');
        Route::post('{pageId}/update', 'Admin\Content\PagesController@update')->name('AdminPagesUpdate');
        Route::get('{pageId}/delete', 'Admin\Content\PagesController@delete')->name('AdminPagesDelete');
    });

    Route::group(['prefix' => 'manage/tickets', 'middleware' => 'adminSuperUser'], function () {
        Route::get('/', 'Admin\Modules\Tickets\ManageController@index')->name('AdminManageTickets');
        Route::get('get-list', 'Admin\Modules\Tickets\ManageController@getFilterList')->name('AdminManageTicketsFilter');
        Route::get('{ticketId}/show', 'Admin\Modules\Tickets\ManageController@showTicket')->name('AdminManageTicketsShow');
    });

    Route::group(['prefix' => 'manage/contacts', 'middleware' => 'adminSuperUser'], function () {
        Route::get('/', 'Admin\Content\ContactsController@index')->name('AdminContacts');
        Route::post('update', 'Admin\Content\ContactsController@update')->name('AdminContactsUpdate');

    });

    Route::group(['prefix' => 'manage/archives', 'middleware' => 'adminSuperUser'], function () {
        Route::get('/', 'Admin\Modules\Tickets\ManageController@archives')->name('AdminArchives');
        Route::get('{archiveName}/download', 'Admin\Modules\Tickets\ManageController@archivesDownload')->name('AdminArchivesDownload');
    });

    Route::group(['prefix' => 'nbki/credit-rating', 'middleware' => 'adminPermission:nbki_credit_rating'], function () {
        Route::get('/', 'Admin\Nbki\CreditRatingController@index')->name('AdminNbkiCreditRating');
        Route::get('get-list', 'Admin\Nbki\CreditRatingController@getList')->name('AdminNbkiCreditRatingList');
        Route::get('{id}/result', 'Admin\Nbki\CreditRatingController@result')->name('AdminNbkiCreditRatingResult');
        Route::get('{id}/pdf', 'Admin\Nbki\CreditRatingController@pdf')->name('AdminNbkiCreditRatingPdf');
    });

    Route::group(['prefix' => 'nbki/contact', 'middleware' => 'adminPermission:nbki_credit_rating'], function () {
        Route::get('/', 'Admin\Nbki\ContactController@index')->name('AdminNbkiContact');
        Route::get('get-list', 'Admin\Nbki\ContactController@getList')->name('AdminNbkiContactList');
        Route::get('{id}/result', 'Admin\Nbki\ContactController@result')->name('AdminNbkiContactResult');
        Route::get('{id}/pdf', 'Admin\Nbki\ContactController@pdf')->name('AdminNbkiContactPdf');
    });

});


// customer auth routes
Route::get('customers/auth/login', 'Customer\AuthController@getLogin')->name('CustomerLogin');
Route::get('customers/auth/logout', 'Customer\AuthController@getLogout')->name('CustomerLogout');
Route::post('customers/auth/login', 'Customer\AuthController@postLogin')->name('CustomerPostLogin');

Route::group(['prefix' => 'customers', 'middleware' => 'customerMiddleware:customer'], function () {
    Route::get('/', 'Customer\HomeController@index')->name('CustomerHome');
    Route::get('get-news/{newsId}', 'Customer\HomeController@getNews')->name('CustomerHomeGetNews');
    Route::get('get-page/{pageId}', 'Customer\HomeController@getPage')->name('CustomerHomeGetPage');

    Route::group(['prefix' => 'tickets'], function () {
        Route::get('actual', 'Customer\TicketController@actual')->name('CustomerTicketActual');
        Route::get('archive', 'Customer\TicketController@archive')->name('CustomerTicketArchive');
        Route::get('done', 'Customer\TicketController@done')->name('CustomerTicketDone');
        Route::get('list/{type}', 'Customer\TicketController@getList')->name('CustomerTicketList');
        Route::get('create', 'Customer\TicketController@create')->name('CustomerTicketCreate');
        Route::post('store', 'Customer\TicketController@store')->name('CustomerTicketStore');
        Route::get('{ticketId}/show', 'Customer\TicketController@show')->name('CustomerTicketShow');
        Route::post('{ticketId}/upload', 'Customer\TicketController@upload')->name('CustomerTicketUpload');
        Route::post('{ticketId}/message/store', 'Customer\TicketController@messageStore')->name('CustomerTicketMessageStore');
        Route::get('{ticketId}/files/{type}', 'Customer\TicketController@getFiles')->name('CustomerTicketGetFiles');
        Route::get('{ticketId}/action/{type}', 'Customer\TicketController@action')->name('CustomerTicketAction');
        Route::get('{ticketId}/edit', 'Customer\TicketController@edit')->name('CustomerTicketEdit');
        Route::post('{ticketId}/update', 'Customer\TicketController@update')->name('CustomerTicketUpdate');
        Route::get('{ticketId}/download', 'Customer\TicketController@download')->name('CustomerTicketDownload');

    });

    Route::group(['prefix' => 'nbki/credit-rating'], function () {
        Route::get('/', 'Customer\Nbki\CreditRatingController@index')->name('CustomerNbkiCreditRating');
        Route::get('create', 'Customer\Nbki\CreditRatingController@create')->name('CustomerNbkiCreditRatingCreate');
        Route::get('get-list', 'Customer\Nbki\CreditRatingController@getList')->name('CustomerNbkiCreditRatingList');
        Route::post('store', 'Customer\Nbki\CreditRatingController@store')->name('CustomerNbkiCreditRatingStore');
        Route::get('{id}/result', 'Customer\Nbki\CreditRatingController@result')->name('CustomerNbkiCreditRatingResult');
        Route::get('{id}/pdf', 'Customer\Nbki\CreditRatingController@pdf')->name('CustomerNbkiCreditRatingPdf');
    });

    Route::group(['prefix' => 'nbki/contact'], function () {
        Route::get('/', 'Customer\Nbki\ContactController@index')->name('CustomerNbkiContact');
        Route::get('create', 'Customer\Nbki\ContactController@create')->name('CustomerNbkiContactCreate');
        Route::get('get-list', 'Customer\Nbki\ContactController@getList')->name('CustomerNbkiContactList');
        Route::post('store', 'Customer\Nbki\ContactController@store')->name('CustomerNbkiContactStore');
        Route::get('{id}/result', 'Customer\Nbki\ContactController@result')->name('CustomerNbkiContactResult');
        Route::get('{id}/pdf', 'Customer\Nbki\ContactController@pdf')->name('CustomerNbkiContactPdf');
    });

    Route::group(['prefix' => 'payments'], function () {
        Route::get('/', 'Customer\PaymentsController@index')->name('CustomerPayments');
    });

});

